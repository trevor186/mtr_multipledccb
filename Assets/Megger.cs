﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Megger : MonoBehaviour
{
    public Animator meggerAnimator;
    public void CollideAction(){
        meggerAnimator.SetBool("Megger_send_1kv",true);
        if(IsInvoking())
            CancelInvoke();
        Invoke("ResetAnimation",3f);
    }
    public void ResetAnimation(){
        meggerAnimator.SetBool("Megger_send_1kv",false);
    }

}
