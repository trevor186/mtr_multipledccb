﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthingTester : MonoBehaviour
{
    public GameObject tester;
    public GameObject earthingLock;
    public GameObject earthingClip;
    public List<Transform> testerPosition = new List<Transform>();
    public List<Transform> lockPosition = new List<Transform>();
    public GameObject extensionRod;
    public List<GameObject> buttonLightList = new List<GameObject>();
    private bool buttonPressed;
    public bool checkedTester;
    public BoxCollider testerCollider;
    public bool applyExtention;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // if(applyExtention){
        //     if(true){
        //         extendRod(true);
        //     }
        // }
    }

    public void SetTester(bool value){
        // tester.transform.localPosition =testerPosition[value? 0:1].localPosition;
        tester.SetActive(value);
        earthingLock.SetActive(false);
        earthingClip.SetActive(false);
        // longRod.SetActive(value);
    }
    public void SetLock(bool value){
        tester.SetActive(false);
        earthingLock.SetActive(value);
        if(MultiDCCBSceneHandlingManager.instance.currentSceneIndex >2){
            earthingClip.SetActive(value);
        }

    }
    public void extendRod(bool value){
        tester.transform.localPosition =testerPosition[value? 0:1].localPosition;
        earthingLock.transform.localPosition = lockPosition[value? 0:1].localPosition;
        extensionRod.SetActive(value);

    }
    public void OnTesterButtonPressed(){
        buttonLightList[0].SetActive(true);
        buttonLightList[1].SetActive(false);
        buttonPressed = true;
    }
    public void OnTesterButtonReleased(){
        if(buttonPressed){
            buttonLightList[0].SetActive(false);
            buttonLightList[1].SetActive(true);
            checkedTester = true;
            MultiDCCBSceneHandlingManager.instance.SceneActionCallResult("CheckTester");
        }
    }
}
