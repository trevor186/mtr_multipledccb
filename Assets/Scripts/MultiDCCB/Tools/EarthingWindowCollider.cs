﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthingWindowCollider : MonoBehaviour
{
   public void OnTriggerEnter(Collider other){
       if(other.tag =="Hand"){
           if(MultiDCCBSceneEventManager.instance.current25kProcedure == MultiDCCBSceneEventManager.Cabinate25kProcedure.APPLY_TO_CIRCUIT){
                MultiDCCBSceneEventManager.instance.ActionCallResult("TouchedWindow");
           }
       }
   }
}
