﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthingContactCollider : MonoBehaviour
{
    // Start is called before the first frame update
    public string cabinateCode;
    public bool earthLockStart;
    public GameObject attachedClip;
    public void OnTriggerEnter(Collider other){
        if(other.tag =="EarthingRodClip"){
            if(earthLockStart){
                attachedClip.SetActive(true);
                MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(cabinateCode+ "_AttachClip");
            }
        }
    }
}
