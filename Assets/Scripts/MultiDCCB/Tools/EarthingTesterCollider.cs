﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthingTesterCollider : MonoBehaviour
{
    // Start is called before the first frame update\
public string cabinateCode;
public bool earthTestStart;
public bool earthLockStart;
public GameObject attachedLock;
    public void OnTriggerEnter(Collider other){
       if(other.tag =="EarthingTester"){
           EarthingTester tester = other.GetComponentInParent<EarthingTester>();
           if(earthTestStart){
                if(tester.checkedTester){
                    MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(cabinateCode+"_ApplyCircuit");
                    tester.checkedTester = false;
                }else{
                    MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(cabinateCode+"_ErrorCircuit");
                    IncorrectStepHandleManager.instance.CreateSmokeError(this.transform);
                }
           }
       }
        if(other.tag =="EarthingRodHead"){
            Debug.Log("??????????????????????????  other name   "  + other.name);
            EarthingTester tester = other.GetComponentInParent<EarthingTester>();
            if(attachedLock == null) return;
            if(earthLockStart && tester != null){
                tester.earthingLock.SetActive(false);
                attachedLock.SetActive(true);
                MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(cabinateCode+"_AttachLock");
            }
        }
    }
}
