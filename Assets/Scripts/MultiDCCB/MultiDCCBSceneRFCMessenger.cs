using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using UnityEngine.SceneManagement;
public class MultiDCCBSceneRFCMessenger : TNBehaviour
{
    // public MultiDCCBSceneEventManager sceneEventManager;
    #region Singleton

    public static MultiDCCBSceneRFCMessenger instance = null;
    // Start is called before the first frame update
    protected override void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start(){

    }
        #endregion

	public void SendAll(string rfcName)
	{
		tno.Send(rfcName, Target.All);
	}
	public void SendAll(string rfcName, object obj1)
	{
		tno.Send(rfcName, Target.All, obj1);
	}
	public void SendOthers(string rfcName)
	{
		tno.Send(rfcName, Target.Others);
	}
	public void SendOthers(string rfcName, object obj1)
	{
		tno.Send(rfcName, Target.Others, obj1);
	}
    public void SetCollideEventRFC(string eventName,int id,bool value){
        Debug.Log("test ??  ");
        tno.Send("SetCollideEvent",Target.All,eventName,id,value);
    }

    [RFC] void SetCollideEvent(string eventName, int id,bool value){
        Debug.Log("Set " + value + "  on collide event name  " + eventName  + " for ID :  " + id);
            CollideEventObjectManager.instance.AddToColliderStackList(id,eventName,value);
        if (id == AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID()){
            CollideEventObjectManager.instance.SetColliderObjectActive(eventName,id,value);
        }
    }
    public void ReadStepInstructionsRFC(bool value,string extraInfo = null, bool soloTest = false){
        tno.Send("ReadStepInstructions",Target.All,value,extraInfo,soloTest);
    }
    [RFC] void ReadStepInstructions(bool value,string extraInfo = null, bool soloTest = false){
        MultiDCCBSceneHandlingManager.instance.SceneReadStepInstructions(value,extraInfo,soloTest);
    }
    public void NextStepRFC(){
        Debug.Log("test");
        tno.Send("NextStep",Target.All);
    }
    [RFC] void NextStep(){
        // StartCoroutine(sceneEventManager.ReadStepInstructions(true));
        MultiDCCBSceneHandlingManager.instance.SceneReadStepInstructions(true);
    
    }
    public void SetSnapEventRFC(string eventName){
        tno.Send("SetSnapEvent",Target.All,eventName);
    }
    [RFC] void SetSnapEvent(string eventName){
        // MultiDCCBSceneEventManager.instance.currentCabinate.SetSnapEvent(eventName);
        Debug.Log("Open??? " +eventName);
        MultiplayerSnapEventObjectManager.instance.ShowSnapEvent(eventName,true);
    }
    public void PlayAnimationTriggerRFC(string animation, string animatorName){
        tno.Send("PlayAnimationTrigger",Target.All,animation,animatorName);
    }

    [RFC] void PlayAnimationTrigger(string animation,string animatorName ){
        AnimationManager.instance.RunAnimationTrigger(animation,animatorName);
    }
    public void PlayAnimationBoolRFC(string animation, string animatorName,bool value){
        tno.Send("PlayAnimationBool",Target.All,animation,animatorName,value);
    }

    [RFC] void PlayAnimationBool(string animation,string animatorName,bool value ){
        AnimationManager.instance.RunAnimationBool(animation,animatorName,value);
    }
    // public void SetCabinateSnapEventRFC(string eventName,int id){
    //     tno.Send("SetCabinateSnapEvent",Target.All,eventName,id);
    // }
    // [RFC] void SetCabinateSnapEvent(string eventName, int id){
    //         // if (idList[ii] == AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID())
    //         //     CollideEventObjectManager.instance.SetColliderObjectActive(eventName,idList[ii],value);
    //     if (id == AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID()){
    //         MultiDCCBSceneEventManager.instance.currentCabinate.SetSnapEvent(eventName);
    //     }
    // }
    public void SnapObjectCreateRFC(string path, Vector3 pos, Quaternion rot)
    {
        Debug.Log("SnapObjectCreateRFC: " + path);
        tno.Send("SnapObjectCreate", Target.Others, path, pos, rot);
    }

    [RFC] void SnapObjectCreate(string path, Vector3 pos, Quaternion rot)
	{
		Debug.Log("SnapObjectCreate");

        GameObject clone;
        if (ScenarioEventManager.instance.GetSwitchRoom().gameObject.activeInHierarchy)
		    clone = GameObject.Instantiate(Resources.Load<GameObject>(path), pos, rot, ScenarioEventManager.instance.GetSwitchRoom().transform);
        else
            clone = GameObject.Instantiate(Resources.Load<GameObject>(path), pos, rot, ScenarioEventManager.instance.GetTrackside().transform);
        // clone.GetComponent<BoxCollider>().enabled = false;
    }
    // [RFC] void NextStepWithString(string obj1,string obj2 = null){
    //     sceneEventManager.PointerEvent(obj1,obj2);
    // }
    public void ShowCautionNoticeRFC()
    {
        tno.Send("ShowRightHandObject", Target.All, "CautionNotice");        
    }
    public void ShowDangerNoticeRFC()
    {
        tno.Send("ShowRightHandObject", Target.All, "DangerNotice");        
    }
    public void ShowRotatorHandleRFC()
    {
        tno.Send("ShowRightHandObject", Target.All, "RotatorHandle");
    }
    public void ShowRightHandObjectRFC(string value){
        tno.Send("ShowRightHandObject",Target.All,value);
    }
    public void RemoveRightHandObjectRFC(){
        tno.Send("RemoveRightHandObject",Target.All);
    }
    [RFC] void ShowRightHandObject(string targetItem)
    {
        MTRPlayerController playerController = MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).playerController;
        int idx = -1;
        switch (targetItem)
        {
            case "CautionNotice": idx = 0; break;
            case "DangerNotice":  idx = 1; break;
            case "RotatorHandle": idx = 2; break;
            case "QRScanner" : idx = 3; break;
            case "SafetyDocument" : idx = 4; break;
            case "Red_flash_prefab" :idx = 5; break;
        }
        if (idx != -1 && !playerController.phone.isPhoneUsing)
        {
            if (playerController.currentRightHandObject == playerController.rightHandObjectList[idx])
            {
                RemoveRightHandObject();
            }
            else
            {
                RemoveRightHandObject();
                playerController.rightHandObjectList[idx].SetActive(true);
                playerController.currentRightHandObject = playerController.rightHandObjectList[idx];
            }

            if((playerController.currentRightHandObject ==playerController.rightHandObjectList[0]) ||
             (playerController.currentRightHandObject ==playerController.rightHandObjectList[1])||
             (playerController.currentRightHandObject ==playerController.rightHandObjectList[5])){
                 MultiplayerSnapEventObjectManager.instance.EnableSnapHighlight(true);
             }else{
                MultiplayerSnapEventObjectManager.instance.EnableSnapHighlight(false);
             }
        }
    }
    [RFC] void RemoveRightHandObject()
    {
        MTRPlayerController playerController =MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).playerController;
        if (playerController.currentRightHandObject != null)
        {
            playerController.currentRightHandObject.SetActive(false);
            playerController.currentRightHandObject = null;
            MultiplayerSnapEventObjectManager.instance.EnableSnapHighlight(false);
        }
    }
    public void ShowLeftHandObjectRFC(string value){
        tno.Send("ShowLeftHandObject",Target.All,value);
    }
    public void RemoveLeftHandObjectRFC(){
        tno.Send("RemoveLeftHandObject",Target.All);
    }
    [RFC] void ShowLeftHandObject(string targetItem)
    {
        MTRPlayerController playerController =MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).playerController;

        int idx = -1;
        switch (targetItem)
        {
            case "EarthingTester": idx = 0; break;
            case "EarthingClipAndLock":  idx = 1; break;
            // case "RotatorHandle": idx = 2; break;
        }
        if (idx != -1 && !playerController.phone.isPhoneUsing)
        {
            if (playerController.currentLeftHandObject == playerController.leftHandObjectList[idx])
            {
                RemoveLeftHandObject();
            }
            else
            {
                RemoveLeftHandObject();
                playerController.leftHandObjectList[idx].SetActive(true);
                playerController.currentLeftHandObject = playerController.leftHandObjectList[idx];
                //Extra step
                switch(idx){
                    case(0):
                        playerController.leftHandObjectList[idx].GetComponentInChildren<EarthingTester>().SetTester(true);
                        // if((string) TNManager.GetPlayerData("SceneParameter").value == "Scene2A"){
                        //     playerController.leftHandObjectList[idx].GetComponentInChildren<EarthingTester>().extendRod(true);
                        // }
                        break;
                    case(1):
                        playerController.leftHandObjectList[idx].GetComponentInChildren<EarthingTester>().SetLock(true);
                        break;
                    default: 
                        break;
                }
                
            }
        }
    }
    [RFC] void RemoveLeftHandObject()
    {
        MTRPlayerController playerController =MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).playerController;
        if (playerController.currentLeftHandObject != null)
        {
            if(playerController.currentLeftHandObject = playerController.leftHandObjectList[1]){
                playerController.leftHandObjectList[1].GetComponentInChildren<EarthingTester>().SetLock(false);
            }
            playerController.currentLeftHandObject.SetActive(false);
            playerController.currentLeftHandObject = null;
        }
    }
    // public void CabinateSwitchChangeProcedure(){

    // }
    public void RotatorHandleAngleRFC(float angle,string cabinateCode,string holeName){
        tno.Send("RotatorHandleAngle",Target.Others,angle,cabinateCode,holeName);
    }
    [RFC] void RotatorHandleAngle(float angle,string cabinateCode,string holeName)
    {
        // Debug.Log("Receive RotatorAngle" + angle);
        // ScenarioEventManager.instance.selectedGear.trolleyController.SetValue(z);
        // ScenarioEventManager.instance.SetRotatorAngle(angle);
        MultiDCCBSceneEventManager.instance.SetHandleRotate(angle,cabinateCode,holeName);
    }    
    public void ReloadSceneRFC(string sceneName){
        tno.Send("ReloadScene",Target.All,sceneName);
    }
    [RFC] void ReloadScene(string sceneName){
        SceneManager.LoadScene("MTR_MultiDCCB_Trevor");
    }

    public void RemoveHandleRFC(string code,string hole){
        tno.Send("RemoveHandle",Target.All,code,hole);
    }
    [RFC] void RemoveHandle(string code,string hole){
        MultiDCCBSceneEventManager.instance.RemoveHandle(code,hole);
    }
    public void CloseHandleSnapRFC(string code, string hole){
        tno.Send("CloseHandleSnap",Target.All,code,hole);
    }
    [RFC] void CloseHandleSnap(string code,string hole){
        MultiDCCBSceneEventManager.instance.switch25kVCabinateList[0].CloseHandleSnap(code,hole);
        MultiDCCBSceneEventManager.instance.switch25kVCabinateList[1].CloseHandleSnap(code,hole);
    }
    public void EnterSceneRFC(int channelIndex,string sceneName,int sceneIndex,string sceneParam){
		// Debug.Log("?index "  + sceneIndex + " sceneame???  " + sceneName);
		// channelID = sceneIndex;
		// TNManager.JoinChannel(sceneIndex, sceneName, false, 255, null);
		Debug.Log("Test?");
		tno.Send("EnterScene",Target.All,channelIndex,sceneName,sceneIndex,sceneParam);
	}
    [RFC] void EnterScene(int channelIndex,string sceneName,int sceneIndex,string sceneParam){
		Debug.Log("?index "  + sceneIndex + " sceneame???  " + sceneName);
		AMVRNetworkManager.instance.channelID = channelIndex;
		TNManager.JoinChannel(channelIndex, sceneName, false, 255, null);
		MultiDCCBSceneHandlingManager.instance.SetCurrentScene(sceneIndex);
        AMVRNetworkManager.instance.SetSceneParam(sceneParam);

	}

    public void CheckSnapObjectRFC(string name){
        tno.Send("CheckSnapEvent",Target.All,name);
    }
    [RFC] void CheckSnapEvent(string name){
        Debug.Log("CALLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
        MultiplayerSnapEventObjectManager.instance.ForceSnap(name);
    }
    public void CallFingeringRFC(string value){
        tno.Send("CallFingering",Target.Others,value);
    }
    [RFC] void CallFingering(string value){
        CollideEventObjectManager.instance.RFCCallFingering(value);
    }
    public void PlayAudioRFC(string value){
        tno.Send("PlayAudio",Target.All,value);
    }
    [RFC] void PlayAudio(string value){
        AudioManager.instance.PlayAudioClip(value);
    }
    public void DoTeleportRFC(){
        tno.Send("DoTeleport",Target.All);
    }
    [RFC] void DoTeleport(){
        TeleportManager.instance.Teleport();
    }
    public void HandleColliderErrorEventRFC(string colliderName){
        tno.Send("HandleColliderErrorEvent",Target.All,colliderName);
    }
    [RFC] void HandleColliderErrorEvent(string colliderName){
        IncorrectStepHandleManager.instance.CreateSmokeError(colliderName);
    }
}
