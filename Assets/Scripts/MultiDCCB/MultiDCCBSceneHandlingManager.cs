﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiDCCBSceneHandlingManager : MonoBehaviour
{
    #region Singleton
    public static MultiDCCBSceneHandlingManager instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this){
            Destroy(gameObject);  
        }

    }
    #endregion
    public int currentSceneIndex;
    public MTRPlayer player1Controller;
    public MTRPlayer player2Controller;
    public PlayerVRController rightHandController;
    public PlayerVRController leftHandController;
    public bool soloPlay;
    // public MultiDCCBSceneEventManager scene1EventManager;
    // public MultiDCCBScene2EventManager scene2EventManager;
    
    public void SceneActionCallResult(string action,string extraInfo = null){
        switch(currentSceneIndex){
            case(1):
                MultiDCCBSceneEventManager.instance.ActionCallResult(action);
                break;
            case(2):
                MultiDCCBScene2EventManager.instance.ActionCallResult(action);
                break;
            default: 
                break;
        }
    }
    public void SceneReadStepInstructions(bool result,string extraInfo = null,bool soloTest = false){
        Debug.Log("Test read scene");
        switch(currentSceneIndex){
            case(1):
                StartCoroutine(MultiDCCBSceneEventManager.instance.ReadStepInstructions(result,extraInfo,soloTest));
                break;
            case(2):
                StartCoroutine(MultiDCCBScene2EventManager.instance.ReadStepInstructions(result,extraInfo,soloTest));
                break;
            default: 
                break;
        }
    }
    public MTRPlayer GetMTRPlayer(int id =1){
        if (id ==1)
            return player1Controller;
        else if( id ==2)
            return player2Controller;
        else
            return null;
    }
    public void SetCurrentScene(int index){
        currentSceneIndex = index;
    }
    public void SceneSkipStep(){
        switch(currentSceneIndex){
            case(1):
                MultiDCCBSceneEventManager.instance.SkipStep();
                break;
            case(2):
                MultiDCCBScene2EventManager.instance.SkipStep();
                break;
        }
    }
    public void UIPanelActionCall(string action){
        MTRPlayer player = AMVRNetworkManager.instance.GetCurrentMTRPlayer();
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        if(player.IsPlayerOne()){
            switch(action){
                case"Phone" : 

                    SceneActionCallResult(action);
                    break;
                case"Briefing":
                    // player.OpenDocRFC();
                    rfcMessenger.ShowRightHandObjectRFC("SafetyDocument");
                    break;
                case"Report":
                    // player.OpenReportRFC();
                    // playerController.OpenReportRFC();
                    break;           
                case"CautionNotice":
                
                    // player.ShowCautionNoticeRFC();
                    rfcMessenger.ShowRightHandObjectRFC("CautionNotice");
                    break;            
                case"DangerNotice":                
                    // player.ShowDangerNoticeRFC();
                    rfcMessenger.ShowRightHandObjectRFC("DangerNotice");
                    break;            
                case"Handle":
                    rfcMessenger.ShowRightHandObjectRFC("RotatorHandle");
                    break;
                case"Tester":
                    // player.SendAll("ActivateMeggerTester",!switchRoomScene.MeggerTesterTool.activeSelf);
                    // player.PlayerActiveTesterPensRFC();
                    rfcMessenger.ShowLeftHandObjectRFC("EarthingTester");

                    break;
                case "Gloves":
                    // RFCMessenger.instance.NextStepRFC("Gloves");
                    // player.PlayerChangeGlovesRFC();
                    rfcMessenger.ShowRightHandObjectRFC("Red_flash_prefab");
                    break;
                case "Earthing":
                    rfcMessenger.ShowLeftHandObjectRFC("EarthingClipAndLock");
                    break;
            }
        }
    }
    public void HandleError(){}
}
