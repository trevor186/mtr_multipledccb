﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;




public class GlobalConfigData
{
    public int isServer = 0;        //  0: client, 1: server
    public int isServerWithClient = 0;      // 0: normal server, 1: server with client
    public int networkId = -1;      // -1: undefined, 0: instructor, >=1: player
    public int useSteamVR = 0;
    public string serverIP = "";
    public int isCave = 0;          //  0: normal, 1: cave
    public string displayID = "";
}


public class GlobalConfig : MonoBehaviour
{
    #region Singleton
    public static GlobalConfig instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        Debug.Log("check awake" + this.name);
        AwakeInit();
    }
    #endregion

    private const string _configFileName = "MTR_Multiplayer_Config.json";
    public GlobalConfigData data;
    public bool isEditorMustBeServer = false;
    public bool isEditorBeClientAsWell = false;
    public bool isEditorBePlayer2 = false;
    // public bool isSoloPlay = false;
    // Start is called before the first frame update
    void AwakeInit()
    {
        Debug.Log("???");
        _LoadConfig();
        DontDestroyOnLoad(gameObject);
        // isEditorMustBeServer = true;
    }

    private void _LoadConfig()
    {
        Debug.Log("test load config");
        string filePath = Path.Combine(Application.streamingAssetsPath, _configFileName);

        if(File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath); 
            GlobalConfigData loadedData = JsonUtility.FromJson<GlobalConfigData>(dataAsJson);
            data = loadedData;

#if UNITY_EDITOR
            
            if (isEditorMustBeServer)
            {
                data.isServer = 1;          //UNITY_EDITOR must be a server
                data.networkId = 1;         //UNITY_EDITOR must be player 1
                data.useSteamVR = 0;        //UNITY_EDITOR don't use vr if server
            }
            if (isEditorBeClientAsWell)
            {
                data.isServerWithClient = 1;
                data.networkId = 1;         //UNITY_EDITOR must be player 1
                data.useSteamVR = 1;        //UNITY_EDITOR must use steam vr
            }
            if (isEditorBePlayer2)
            {
                data.networkId = 2;
                data.isServer = 0;
            }
#endif

            //raymond temp setting
            // data.isServer = 0;
            // Debug.Log("data.isServer" + data.isServer);

            // if (GetNetworkID() > 0)
            //     PhotonNetwork.LocalPlayer.NickName = "Trainee" + GetNetworkID();
            // else
            //     PhotonNetwork.LocalPlayer.NickName = "Instructor";
        }
        else
        {
            Debug.LogError("Cannot load GlobalConfigData, using default value now");
            _SaveConfig();
        }
    }

    private void _SaveConfig()
    {
        string dataAsJson = JsonUtility.ToJson (data);

        string filePath = Path.Combine(Application.streamingAssetsPath, _configFileName);
        //string filePath = Application.dataPath + gameDataFileName;
        File.WriteAllText (filePath, dataAsJson);
    }

    public bool IsTrainee()
    {
// #if UNITY_EDITOR
//         //raymond temp setting
//         if (isEditorBeClientAsWell)
//             return true;
// #endif
        if (data.isServer == 1 && data.isServerWithClient == 1)
            return true;

        return data.isServer == 0;
    }

    public bool IsServer()
    {
        return data.isServer == 1;
    }

    public bool IsServerWithClient()
    {
        return (data.isServer == 1 && data.isServerWithClient == 1);
    }

    public bool isCaveView()
    {
        return data.isCave == 1;
    }

    public int GetNetworkID()
    {
        return data.networkId;
    }

    public int getDisplayID(int index)
    {
        if (index - 1 < data.displayID.Length)
        {
            return int.Parse(data.displayID.Substring(index - 1,1));
        }

        return index;
    }
}
