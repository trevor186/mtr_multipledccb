﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;

public class RFCMessenger : TNBehaviour
{
    public ScenarioEventManager scenarioEventManager;
    // public ControllerManager leftHandControllerManager;
    // public ControllerManager rightHandControllerManager;
     #region Singleton
    public static RFCMessenger instance = null;
    // public TNetPlayer player;
    protected override void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start(){
    }
    #endregion
    // Start is called before the first frame update
	public void SendAll(string rfcName)
	{
		tno.Send(rfcName, Target.All);
	}
	public void SendAll(string rfcName, object obj1)
	{
		tno.Send(rfcName, Target.All, obj1);
	}
	public void SendOthers(string rfcName)
	{
		tno.Send(rfcName, Target.Others);
	}
	public void SendOthers(string rfcName, object obj1)
	{
		tno.Send(rfcName, Target.Others, obj1);
	}

    public void NextStepRFC(){
        tno.Send("NextStep",Target.All);
    }
    public void NextStepRFC(string obj1,string obj2 = null ){
        tno.Send("NextStepWithString",Target.All,obj1,obj2);
    }
    [RFC] void ReceiveDocAction(string name){
        AMVRNetworkManager.instance.GetCurrentPlayer().GiveDocRFC(name);
    }
    public void ReceiveDocActionRFC(string name){
        tno.Send("ReceiveDocAction",Target.All,name);
    }
    public void ReadStepInstructionsRFC(){
        Debug.Log("test call");
        tno.Send("ReadStepInstructions",Target.All);
    }
    public void InitSetupRFC(int obj1,bool obj2, bool obj3, bool obj4){
        Debug.Log("send obj1" + obj1);
        tno.Send("InitSetup",Target.All,obj1,obj2,obj3,obj4);
    }
    // public void OpenLeftHandToolboxRFC(bool obj1){
    //     tno.Send("OpenLeftHandToolbox",Target.All,obj1);
    // }
    // public void SnappedNoticeRFC(string obj1,string obj2){
    //     tno.Send("SnappedNotice",Target.All,obj1,obj2);
    // }
    // public void SnappedHandleRFC(string obj1,string obj2){
    //     tno.Send("SnappedHandle",Target.All,obj1,obj2);
    // }
    // public void 
    [RFC] void NextStep(){
        scenarioEventManager.StepCounterSet();
    }
    [RFC] void NextStepWithString(string obj1,string obj2 = null){
        scenarioEventManager.PointerEvent(obj1,obj2);
    }

    // [RFC] void SendGearNumber(int obj1){
    //     scenarioEventManager.cabinateNumber = obj1; 
    // }
    // [RFC] void ActionCall(string obj1,GameObject obj2 = null){
    //     Debug.Log("Cehck?" + obj1 + " obje?" + obj2.name );
    //     scenarioEventManager.ActionCallResult(obj1,obj2);

    // }
    [RFC] void ReadStepInstructions(){
        scenarioEventManager.ReadStepInstructions();

    }
    [RFC] void InitSetup(int obj1,bool obj2, bool obj3, bool obj4){
        Debug.Log("Get obj1 "+ obj1);
        scenarioEventManager.InitSetup(obj1,obj2,obj3,obj4);
    }

    // [RFC] void OpenLeftHandToolbox(bool obj1){
    //     player.toolsBox.SetActive(obj1);
    // }
    // [RFC] void SnappedNotice(string obj1, string obj2){
    //     scenarioEventManager.PointerEvent(obj1, obj2);
    // }
    // [RFC] void SnappedHandle(string obj1, string obj2){
    //     scenarioEventManager.PointerEvent(obj1, obj2);
    // }

    [RFC] void BackToWaitingRoom()
    {
        scenarioEventManager.BackToWaitingRoom();
    }

    [RFC] void TrolleyZ(float z)
    {
        // Debug.Log("Receive TrolleyZ" + z);
        // ScenarioEventManager.instance.selectedGear.trolleyController.SetValue(z);
        ScenarioEventManager.instance.SetTrolleyValue(z);
    }

    [RFC] void RotatorAngle(float angle)
    {
        // Debug.Log("Receive RotatorAngle" + angle);
        // ScenarioEventManager.instance.selectedGear.trolleyController.SetValue(z);
        ScenarioEventManager.instance.SetRotatorAngle(angle);
    }    

    [RFC] void PasteSafetyDoc()
    {
        ScenarioEventManager.instance.PasteSafetyDoc();        
    }

    public void PlayAudioRFC(string name){
        tno.Send("PlayAudio",Target.All,name);
    }
    [RFC] void PlayAudio(string name){
        Debug.Log("name");
        AudioManager.instance.PlayAudioClip(name);
    }

}
