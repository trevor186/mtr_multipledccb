﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using VRTK;

public class VRTK_ExtendInput : MonoBehaviour
{
    #region Singleton
    public static VRTK_ExtendInput instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        AwakeInit();
    }
    #endregion


    private const float maxTRForceFilter = 150.0f;
    private const float openTRForce = 40.0f;

    public SteamVR_ControllerManager controllerManager;
    public VRTK.VRTK_ControllerEvents leftController;
    public VRTK.VRTK_ControllerEvents rightController;
    public SteamVR_TrackedObject leftTrackedObect;
    public SteamVR_TrackedObject rightTrackedObect;
    public SteamVR_TrackedObject headTrackedObect;
    public SteamVR_TrackedObject otherTrackedObject;
    private SteamVR_Controller.Device rightDevice
    {
        get
        {
            int index = (int)rightTrackedObect.index;
            if (index != -1)
            {
                return SteamVR_Controller.Input(index);
            }
            return null;
        }
    }

    public Action actOnPressLeftController;
    public Action actOnPressUpLeftController;
    public Action actOnPressDownLeftController;

    public Action actOnPressRightController;
    public Action<bool> actOnPressUpRightController;
    public Action<bool> actOnPressDownRightController;

    public Action actOnPressDownRightMenu;

    public Action<bool> actOnAccelTrigger;

    public Action actOnUserMountHeadset;
    public Action actOnUserUnmountHeadset;

    public Action<Transform> actOnHeadTransfromUpdate;
    public Action<Transform> actOnRightHandTransfromUpdate;

    [HideInInspector]
    public bool isUserPresent;

    public bool isRightControllerTriggerPressed { get { return rightController.triggerPressed; } }
    public bool isAccelTriggered;

    void AwakeInit()
    {
        var system = OpenVR.System;
        VRControllerState_t controllerState = new VRControllerState_t();
        if (system != null && system.GetControllerState(0, ref controllerState, (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(VRControllerState_t))))
        {
            isUserPresent = (controllerState.ulButtonPressed & (1UL << ((int)EVRButtonId.k_EButton_ProximitySensor))) > 0L;
        }
        SteamVR_Events.System(EVREventType.VREvent_ButtonPress).Listen(OnButtonPress);
        SteamVR_Events.System(EVREventType.VREvent_ButtonUnpress).Listen(OnButtonUnpress);

        // leftTrackedObect = leftController.GetComponent<SteamVR_TrackedObject>();
        // rightTrackedObect = rightController.GetComponent<SteamVR_TrackedObject>();

        leftController.TriggerClicked += LeftController_TriggerClicked;
        leftController.TriggerUnclicked += LeftController_TriggerUnclicked;
        rightController.TriggerClicked += RightController_TriggerClicked;
        rightController.TriggerUnclicked += RightController_TriggerUnclicked;

        rightController.ButtonTwoPressed += RightController_MenuButtonClicked;
    }

    private void RightController_MenuButtonClicked(object sender, ControllerInteractionEventArgs e)
    {
        // Debug.Log("RightController_MenuButtonClicked");

        if (actOnPressDownRightMenu != null)
        {
            actOnPressDownRightMenu();
        }
    }

    private void LeftController_TriggerClicked(object sender, ControllerInteractionEventArgs e)
    {
        // Debug.Log("LeftController_TriggerClicked");

        if (actOnPressDownLeftController != null)
        {
            actOnPressDownLeftController();
        }
    }
    private void LeftController_TriggerUnclicked(object sender, ControllerInteractionEventArgs e)
    {
        if (actOnPressUpLeftController!= null)
        {
            actOnPressUpLeftController();
        }
    }
    private void RightController_TriggerClicked(object sender, ControllerInteractionEventArgs e)
    {
        // Debug.Log("RightController_TriggerClicked");

        if (actOnPressDownRightController != null)
        {
            actOnPressDownRightController(false);
        }
    }

    private void RightController_TriggerUnclicked(object sender, ControllerInteractionEventArgs e)
    {
        if (actOnPressUpRightController != null)
        {
            actOnPressUpRightController(false);
        }
    }
    private void Update()
    {
        if (actOnPressLeftController != null && leftController.triggerPressed)
        {
            actOnPressLeftController();
        }
        if (actOnPressRightController != null && rightController.triggerPressed)
        {
            actOnPressRightController();
        }

        if (rightDevice != null && rightDevice.velocity.sqrMagnitude > openTRForce && rightDevice.velocity.sqrMagnitude < maxTRForceFilter)
        {
            // if (Main.GetInstance().propsController.isPropsByPassed)
            {
                isAccelTriggered = true;
                if (actOnAccelTrigger != null)
                {
                    actOnAccelTrigger(false);
                }
            }
        }
        if (actOnHeadTransfromUpdate != null)
        {
            actOnHeadTransfromUpdate(headTrackedObect.transform);
        }
        if (actOnRightHandTransfromUpdate != null)
        {
            actOnRightHandTransfromUpdate(rightTrackedObect.transform);
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (actOnPressDownRightController != null)
            {
                actOnPressDownRightController(false);
            }
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            if (actOnPressUpRightController != null)
            {
                actOnPressUpRightController(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (actOnPressDownRightMenu != null)
            {
                actOnPressDownRightMenu();
            }
        }
#endif
    }

    void OnButtonPress(VREvent_t args)
    {
        VREvent_t ev = (VREvent_t)args;

        if (ev.data.controller.button == 31)
        {
            isUserPresent = true;
            if(actOnUserMountHeadset != null)
                actOnUserMountHeadset();
            Debug.Log("Mounted headset!");

        }
    }

    void OnButtonUnpress(VREvent_t args)
    {
        VREvent_t ev = (VREvent_t)args;

        if (ev.data.controller.button == 31)
        {
            isUserPresent = false;
            if(actOnUserUnmountHeadset != null)
                actOnUserUnmountHeadset();
            Debug.Log("Unmounted headset!");

        }
    }
    public void AddOnPressDownRightControllerEvent(Action<bool> actOnPressDownRightController)
    {
        this.actOnPressDownRightController = actOnPressDownRightController;
    }
    public void AddOnPressUpRightControllerEvent(Action<bool> actOnPressUpRightController)
    {
        this.actOnPressUpRightController = actOnPressUpRightController;
    }
    public void AddOnAccelTriggerEvent(Action<bool> actOnAccelTrigger)
    {
        this.actOnAccelTrigger = actOnAccelTrigger;
    }
    public void OnCloseScene()
    {
        isAccelTriggered = false;
    }
    private void OnDestroy()
    {
        if (leftController != null)
        {
            leftController.TriggerClicked -= LeftController_TriggerClicked;
            leftController.TriggerUnclicked -= LeftController_TriggerUnclicked;
        }

        if (rightController != null)
        {
            rightController.TriggerClicked -= RightController_TriggerClicked;
            rightController.TriggerUnclicked -= RightController_TriggerUnclicked;

            rightController.ButtonTwoPressed -= RightController_MenuButtonClicked;
        }
        
        actOnPressUpRightController = null;
        actOnPressDownRightController = null;
        actOnAccelTrigger = null;
    }
}