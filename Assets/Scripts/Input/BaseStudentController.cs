﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStudentController : MonoBehaviour
{
    public Transform leftHand;
    public Transform rightHand;
    public Transform rightHandRig;

    public Transform head;
    [HideInInspector]
    public Transform calibratedHand;


    public GameObject bodyPrefab;
    public Transform bodyTransform;

    [SerializeField]
    private Transform headTrackedObject;
    private Transform leftHandTrackObject;
    private Transform rightHandTrackObject;

    public Transform soundTransfrom;

    public bool isLocalPlayer;
    // {
    //     get 
    //     { 
    //         //return Main.GetInstance().localStudentController == this; 
    //         return 
    //     }
    // }

    public Camera playerCamera;


    protected virtual void Awake()
    {
        Init();
    }
    protected virtual void Start()
    {
    }
    public void Init()
    {
        // sprayControl = GameObject.Instantiate(sprayControlPrefab).GetComponent<SprayControl>();
        // truncheonControl = GameObject.Instantiate(truncheonControlPrefab).GetComponent<TruncheonControl>();
        // sprayControl.Init(this);
        // truncheonControl.Init(this);
        // HideOC();
        // HideTR();
        // bodyTransform = GameObject.Instantiate(bodyPrefab).transform;
        // bodyTransform.SetParent(head);
        // bodyTransform.localRotation = Quaternion.identity;
        // bodyTransform.localPosition = Vector3.zero;
        // calibratedHand = rightHandRig;
    }
    public void SetLocalPlayerData()
    {
        //Debug.Log("SetLocalPlayerData");

    }
    public void SetTrackTarget(Transform headTrackedObject, Transform rightHandTrackObject, Transform leftHandTrackObject)
    {
        this.headTrackedObject = headTrackedObject;
        this.leftHandTrackObject = leftHandTrackObject;
        this.rightHandTrackObject = rightHandTrackObject;
        gameObject.SetActive(true);
    }
    public void Close()
    {
        headTrackedObject = null;
        rightHandTrackObject = null;
        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (headTrackedObject != null && head != null)
            head.SetPositionAndRotation(headTrackedObject.position, headTrackedObject.rotation);
        if (leftHandTrackObject != null && leftHand != null)
            leftHand.SetPositionAndRotation(leftHandTrackObject.position, leftHandTrackObject.rotation);
        if (rightHandTrackObject != null && rightHand != null)
            rightHand.SetPositionAndRotation(rightHandTrackObject.position, rightHandTrackObject.rotation);
        
        // if (headTrackedObject != null && playerCamera != null)
        //     playerCamera.transform.rotation = headTrackedObject.rotation;
    }
}
