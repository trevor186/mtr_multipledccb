using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ScenarioEventManager : MonoBehaviour
{
    const float EXPLOSIONTIME = 2.4f; 
    public enum Procedure{
        WAITINGROOM,
        SAFETY_BRIEFING,
        SELECT_CABINATE,
        PUT_NOTICE1,
        CALLING_PSC,
        WAIT_FOR_PSC,
        FINGERING_PROCEDURES,
        DCCB_CRTL_LOCAL,
        PUT_NOTICE2,
        MANOEUVRING_HANDLE,
        OPEN_FRONT_COVER,
        CONTROL_CABLE_PLUG,
        HSCB_TROLLEY,
        BUSBAR_DISCONNECT,
        PUT_NOTICE3,
        CALLING_TC,
        CALLING_MOTOR_PSC,
        CONFIRM_GO_TRACKSIDE,
        GO_TRACKSIDE,
        SELECT_ISOLATOR,
        CABLE_START,
        CABLE_END,
        UNLOCK_MANUAL_ISOLATOR,  // Manual Start Step = 17
        OPEN_MANUAL_ISOLATOR,
        MANUAL_ISOLATER_FINGERING_PROCEDURE,
        LOCK_MANUAL_ISOLATOR,    //Manual Last Step = 20
        MOTOR_TURN_TO_LOCAL,     // Motor Start STep = 21
        MOTOR_POWER_SUPPLY_OFF,
        MOTOR_CONTROL_UNLOCK,
        MOTOR_HANDLE_UNLOCK,
        MOTOR_OPEN_DOOR,
        MOTOR_TURN_TO_MANUAL, 
        MOTOR_FINGERING_PROCEDURE,
        MOTOR_CLOSE_DOOR,
        MOTOR_HANDLE_LOCK,
        MOTOR_CONTROL_LOCK,
        MOTOR_PADLOCK_LOCK,  // Motor Last Step = 30
        PUT_NOTICE4,
        CALL_TRACKSIDE_PSC,
        CONFIRM_GO_DCCB,
        GO_DCCB,
        REMOVE_BACK_COVER,
        SELFTEST_TESTER,
        PUT_ON_GLOVES,
        USE_TESTER,
        SECOND_SELFTEST,
        CONNECT_EARTHING,
        CALLING_PSC2,
        GIVE_SAFETY_DOCUMENT,
        REPORT
        
    }
    public GameObject mainCanvas;
    public GameObject waitingRoom;
    // private GameObject switchRoom;
    SwitchRoomScene switchRoomScene;
    public GameObject switchRoom_prefab;
    public SwitchRoomScene GetSwitchRoom()
    {
        return switchRoomScene;
    }
    private TrackScene trackScene;
    public GameObject trackScene_prefab;
    public TrackScene GetTrackside()
    {
        return trackScene;
    }
    public static ScenarioEventManager instance;
    public Procedure currentProcedure;
    // public List<GameObject> animationObjects = new List<GameObject>();
    // Animator switchGearAnimator;
    Animator manualIsolatorAnimator;
    public List<AudioClip> audioList = new List<AudioClip>();
    public AudioSource audioSource;
    // public List<GameObject> safetyBarrierList = new List<GameObject>();
    // public List<SwitchGear> switchCabinateList = new List<SwitchGear>();
    // public List<ManuelIsolator> manuelIsolatorList = new List<ManuelIsolator>();
    int cabinateNumber;
    SwitchGear selectedGear;
    ManualIsolator selectedManualIsolator;
    MotorIsolator selectedMotorIsolator;
    public TNetPlayer playerController;
    // public List<GameObject> objectHolder = new List<GameObject>();
    private bool _isManualGameMode = true;
    private bool _isVoiceOver = true;
    private bool _isSoloPlay = true;
    private bool _isDebugMode = true;
    private bool _InExplosion = false;
    // public GameObject explosionEffect;
    // public GameObject TracksideSign;
    // public GameObject SwitchGearSign;
    public GameObject startPage;
    private bool _connectedEarthingStart = false;
    bool _isGameStarted;
    private bool _waitForPSCCall;
    private bool _DCCB_PSCCalled;
    public bool isGameStarted { get { return _isGameStarted; } }

    public PlayingPage playingPage;
    [SerializeField] private bool _player1GlovesStatus;
    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    void Start(){
        mainCanvas.SetActive(true);

        InitScene();
    }
    void Update(){

       #if UNITY_EDITOR
       if(Input.GetKeyDown(KeyCode.Q))
        {
            RFCMessenger.instance.NextStepRFC();
        }
        if(Input.GetKeyDown(KeyCode.T)){
            HapticsManager.instance.PlayVibration();
        }
        #endif
        if(_isGameStarted && (Input.GetKeyDown(KeyCode.Return)||Input.GetKeyDown(KeyCode.KeypadEnter)))
        {
            RFCMessenger.instance.NextStepRFC(("Instructor"));
        }

        // if(Input.GetKeyDown(KeyCode.C))
        // {
        //     Debug.Log("PlayerCount" + AMVRNetworkManager.instance.GetPlayerCount());
        // }

        // if (Input.GetKeyDown(KeyCode.S))
        // {
        //     AMVRNetworkManager.instance.GetCurrentPlayer().briefDoc.SetFinishList();
        // }

        // if(_PlayingAudioToSkip){
        //     if(!audioManager.isPlaying){
        //         _PlayingAudioToSkip = false;
        //         StepCounterSet();
        //     }
        // }
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (GlobalConfig.instance.GetNetworkID() == 1)
            {
                for (int i=0 ; i<(int)Procedure.GIVE_SAFETY_DOCUMENT-1 ; i++)
                {
                    RFCMessenger.instance.NextStepRFC();                    
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (GlobalConfig.instance.GetNetworkID() == 1)
            {
                for (int i=0 ; i<(int)Procedure.MOTOR_HANDLE_UNLOCK-1 ; i++)
                {
                    RFCMessenger.instance.NextStepRFC();                    
                }
            }
        }        

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (GlobalConfig.instance.GetNetworkID() == 1)
            {
                for (int i=0 ; i<(int)Procedure.MOTOR_PADLOCK_LOCK-1 ; i++)
                {
                    RFCMessenger.instance.NextStepRFC();                    
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (GlobalConfig.instance.GetNetworkID() == 1)
            {
                for (int i=0 ; i<(int)Procedure.REMOVE_BACK_COVER-1 ; i++)
                {
                    RFCMessenger.instance.NextStepRFC();                    
                }
            }
        }            
#endif

    }
    public void InitScene()
    {
        waitingRoom.SetActive(true);

        if (switchRoomScene != null)
        {
            Destroy(switchRoomScene.gameObject);
            switchRoomScene = null;
        }
        if (trackScene != null)
        {
            Destroy(trackScene.gameObject);
            trackScene = null;
        }

        // switchRoom.SetActive(false);
        // trackSide.SetActive(false);

        //need to init / reset all the playing status here...
        _isGameStarted = false;
        audioSource.Stop();
        currentProcedure= Procedure.WAITINGROOM;
        if(!_isVoiceOver){
            audioSource.mute = true;
        }
    }
    public void GameStart(bool manual, bool voiceOver, bool player2)
    {
        startPage.SetActive(false);

        Debug.Log("GameStart");
        Debug.Log("_isManualGameMode" + _isManualGameMode);
        Debug.Log("_isVoiceOver" + _isVoiceOver);
        Debug.Log("_isSoloPlay" + _isSoloPlay);
        
        cabinateNumber = Random.Range(0,3);
// #if UNITY_EDITOR
//         cabinateNumber = 0;     //hardcode test
// #endif

        RFCMessenger.instance.InitSetupRFC(cabinateNumber,manual, voiceOver, player2);

    }

    public bool IsManualGameMode()
    {
        return _isManualGameMode;
    }
    public bool IsVoiceOver()
    {
        return _isVoiceOver;
    }
    public bool IsSoloPlay()
    {
        return _isSoloPlay;
    }            

    public void InitSetup(int number,bool obj2, bool obj3, bool obj4)
    {
        AudioManager.instance.PlayAudioClip("Com_Enter");

        _isManualGameMode = obj2;
        _isVoiceOver = obj3;
        _isSoloPlay = !obj4;
        // _P1MotorPadlock = false;
        // _P2MotorPadlock = false;
        //Renew the scene now...
        waitingRoom.SetActive(false);

        switchRoomScene = Instantiate(switchRoom_prefab).GetComponent<SwitchRoomScene>();
        switchRoomScene.singlePlayNPC.SetActive(_isSoloPlay);
        
        trackScene = Instantiate(trackScene_prefab).GetComponent<TrackScene>();
        trackScene.gameObject.SetActive(false);

        switchRoomScene.MeggerTesterTool.SetActive(false);
        Debug.Log("Show?" + number);

        selectedGear = switchRoomScene.switchCabinateList[number];
        selectedGear.colliderObjects[0].GetComponent<CollideEvent>().setActionValue("CorrectCabinate");
        selectedGear.colliderObjects[6].GetComponent<CollideEvent>().setActionValue("CorrectCabinate");
        // AMVRNetworkManager.instance.GetCurrentPlayer().testerPens.Add(selectedGear.testerBlackClip);

        if(_isManualGameMode){
            trackScene.manualIsolatorID.SetActive(true);
            foreach(ManualIsolator obj in trackScene.manualIsolatorList){
                obj.gameObject.SetActive(true);
            }
            selectedManualIsolator = trackScene.manualIsolatorList[number];
            selectedManualIsolator.colliderObjects[0].GetComponent<CollideEvent>().setActionValue("ManualFingeringProcedure");
            manualIsolatorAnimator = selectedManualIsolator.animator;

        }else{
            foreach(MotorIsolator obj in trackScene.motorIsolatorList){
                obj.gameObject.SetActive(true);
            }
            selectedMotorIsolator = trackScene.motorIsolatorList[number];
            selectedMotorIsolator.colliderObjects[0].GetComponent<CollideEvent>().setActionValue("MotorFingeringProcedure");

            // selectedMotorIsolator.colliderObjects[6].GetComponent<CollideEvent>().setActionValue("PadlockUnlock_" + GlobalConfig.instance.GetNetworkID());

        }


        switchRoomScene.MeggerTesterTool.transform.parent = selectedGear.meggerTesterTransform;
        switchRoomScene.MeggerTesterTool.transform.localPosition = Vector3.zero;


        AMVRNetworkManager.instance.GetCurrentPlayer().BriefDocInitRFC(_isManualGameMode, number);
        AMVRNetworkManager.instance.GetCurrentPlayer().TesterPenInitRFC();
        AMVRNetworkManager.instance.GetCurrentPlayer().MeggertesterInitRFC();
        _isGameStarted = true;

        ReportManager.instance.InitReportType();
        _DCCB_PSCCalled = false;
        _waitForPSCCall = false;
        _connectedEarthingStart = false;
        currentProcedure= Procedure.SAFETY_BRIEFING;
        ReadStepInstructions();
    }

    void GameEnd()
    {
        Debug.Log("gameENd");
        _isGameStarted = false;
        // audioManager.Stop();

        AMVRNetworkManager.instance.GetCurrentPlayer().OpenReportRFC();
        if (AMVRNetworkManager.instance.GetCurrentPlayer().IsPlayerOne())
            ReportManager.instance.ShowAllReports();
    }
    
    public void PointerEvent(string obj1,string obj2 =null){
        // if(obj1 =="True"){
        //     ActionCallResult(true);
        // }else
        // {
        //     ActionCallResult(false);q
        // }

        // actionProcedure = (Procedure)System.Enum.Parse(typeof(Procedure),obj1);
        if(obj1.StartsWith("UI")){
            UIPanelActionCall(obj1.Substring(3));
        }else
            ActionCallResult(obj1,obj2);
    }
    public void StepCounterSet(){ // auto go to next step if no special action
        // stepCounter += 1;
        currentProcedure += 1;
        Debug.Log("Show " + currentProcedure);

        if(currentProcedure == Procedure.GO_TRACKSIDE){
            switchRoomScene.trackSideSign.SetActive(false);                    
            switchRoomScene.gameObject.SetActive(false);
            trackScene.gameObject.SetActive(true);
            switchRoomScene.gameObject.transform.localPosition = new Vector3(0,0,3);
            for(int i = 0; i< 13;i++){
                int ii = i;
                ReportManager.instance.UpdateReportStep(ii,true);
            }
        }else if(currentProcedure == Procedure.GO_DCCB){
            trackScene.switchGearSign.SetActive(false);                    
            switchRoomScene.gameObject.SetActive(true);
            switchRoomScene.ChangeIsoCameraToPart3();
            trackScene.gameObject.SetActive(false);
            for(int i = 13; i< 18;i++){
                int ii = i;
                ReportManager.instance.UpdateReportStep(ii,true);
            }
        }
        // ReadStepInstructions();
        RFCMessenger.instance.ReadStepInstructionsRFC();
    }
    public void UIPanelActionCall(string action){
        TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
        if(player.IsPlayerOne()){
            switch(action){
                case"Phone" : 
                    RFCMessenger.instance.NextStepRFC("Phone");
                    break;
                case"Briefing":
                    player.OpenDocRFC();
                    break;
                case"Report":
                    player.OpenReportRFC();
                    playerController.OpenReportRFC();
                    break;           
                case"CautionNotice":
                    player.ShowCautionNoticeRFC();
                    break;            
                case"DangerNotice":                
                    player.ShowDangerNoticeRFC();
                    break;            
                case"Handle":
                    player.ShowRotatorHandleRFC();
                    break;
                case"MeggerTester":
                    player.SendAll("ActivateMeggerTester",!switchRoomScene.MeggerTesterTool.activeSelf);
                    player.PlayerActiveTesterPensRFC();
                    break;
                case "Gloves":
                    RFCMessenger.instance.NextStepRFC("Gloves");
                    player.PlayerChangeGlovesRFC();
                    ActionCallResult(action);
                    break;
                case "Earthing":
                    player.PlayerActiveEarthingSetRFC(!playerController.EarthingClips[1].activeSelf);
                    break;
            }
        }
    }
   


    public void ReadStepInstructions(){
        switch(currentProcedure){
            case(Procedure.SAFETY_BRIEFING): //SafetyBriefing
                    Debug.Log("Tell the players to open briefing files in UI panel");
                    playingPage.ShowEnterSign(true);
                    audioSource.clip = audioList[0];
                    // audioManager.Play();
                    PlayAudio();

                    break;
            case(Procedure.SELECT_CABINATE):
                    playingPage.ShowEnterSign(false);

                    ReportManager.instance.UpdateReportStep(1,true);
                    audioSource.clip = audioList[1];
                    // selectedGear.triggerObjects[0].SetActive(true);
                    // audioManager.Play();
                    PlayAudio();
                    Debug.Log("SelectCabinaet");
                    foreach(SwitchGear gear in switchRoomScene.switchCabinateList){
                        gear.colliderObjects[0].SetActive(true);
                    }
                    break;             
            case(Procedure.PUT_NOTICE1): //SelectCabinet
                    Debug.Log("notice");
                    // Invoke("TriggerAnimation",1f);
                    selectedGear.colliderObjects[7].SetActive(true);
                    selectedGear.colliderObjects[13].SetActive(true);

                    foreach(SwitchGear gear in switchRoomScene.switchCabinateList){
                        gear.colliderObjects[0].SetActive(false);
                    }
                    break;                   

            case(Procedure.CALLING_PSC):  // Calling psc
                    Debug.Log("call psc ");
                    audioSource.clip = audioList[2];
                    // audioManager.Play();
                    PlayAudio();
                    // selectedGear.colliderObjects[7].GetComponent<BoxCollider>().enabled = false;
                    // selectedGear.colliderObjects[13].GetComponent<BoxCollider>().enabled = false;

                    selectedGear.colliderObjects[2].SetActive(true);
                    selectedGear.colliderObjects[3].SetActive(true);
                    break;            
            case(Procedure.FINGERING_PROCEDURES): //CallingPSC
                    audioSource.clip = audioList[3];

                    // audioManager.Play();
                    PlayAudio();

                    break;             
            case(Procedure.WAIT_FOR_PSC):
                    playingPage.ShowEnterSign(true);
                    break;
            case(Procedure.DCCB_CRTL_LOCAL): //FingeringProcedures
                    break;
            case(Procedure.MANOEUVRING_HANDLE): //ChangeDCCBButton
                    // selectedGear.colliderObjects[2].SetActive(false);
                    break;             
            case(Procedure.OPEN_FRONT_COVER): //ManoeuvringHandle
                    // selectedGear.colliderObjects[3].SetActive(false);
                    selectedGear.colliderObjects[4].SetActive(true);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                    break;             
            case(Procedure.CONTROL_CABLE_PLUG): //CallingTC
                    selectedGear.colliderObjects[4].SetActive(false);
                    selectedGear.colliderObjects[5].SetActive(true);
                    break;             
            case(Procedure.HSCB_TROLLEY): //PutOnSecondNotice
                    selectedGear.colliderObjects[5].SetActive(false);
                    selectedGear.SetReadyToPull(true);
                    break;   
            case(Procedure.BUSBAR_DISCONNECT):
            case(Procedure.PUT_NOTICE3): // Put notice 2 
                    selectedGear.colliderObjects[12].SetActive(true);
                    selectedGear.colliderObjects[9].SetActive(true);
                    selectedGear.colliderObjects[14].SetActive(true);
                    break;
            case(Procedure.CALLING_TC):
                    audioSource.clip = audioList[4];
                    PlayAudio();
                    selectedGear.colliderObjects[9].GetComponent<BoxCollider>().enabled = false;
                    break;
            case(Procedure.CALLING_MOTOR_PSC): 
                    break;
            case(Procedure.CONFIRM_GO_TRACKSIDE):
                    playingPage.ShowEnterSign(true);
                    break;
            case(Procedure.GO_TRACKSIDE): //ToTrackSide
                    playingPage.ShowEnterSign(false);
                    switchRoomScene.trackSideSign.SetActive(true);
                    break;
            case(Procedure.SELECT_ISOLATOR):
                    audioSource.clip = audioList[5];
                    PlayAudio();
                    foreach(ManualIsolator obj in trackScene.manualIsolatorList){
                        obj.colliderObjects[0].SetActive(true);
                    }
                    if(!_isManualGameMode){
                        foreach(MotorIsolator obj in trackScene.motorIsolatorList){
                            obj.colliderObjects[0].SetActive(true);
                        }
                        selectedMotorIsolator.animatorList[2].SetTrigger("Motor_Isolator_Off");  
                    }
            break;
            case(Procedure.CABLE_START):
                foreach(ManualIsolator obj in trackScene.manualIsolatorList){
                    obj.colliderObjects[0].SetActive(false);
                }
                foreach(MotorIsolator obj in trackScene.motorIsolatorList){
                    obj.colliderObjects[0].SetActive(false);
                }
                if(_isManualGameMode){
                    selectedManualIsolator.colliderObjects[1].SetActive(true);
                    selectedManualIsolator.colliderObjects[3].SetActive(true);
                }
                else{
                    selectedMotorIsolator.colliderObjects[1].SetActive(true);
                    selectedMotorIsolator.colliderObjects[3].SetActive(true);
                }
                break;
            case(Procedure.CABLE_END):
                if(_isManualGameMode){
                    selectedManualIsolator.colliderObjects[2].SetActive(true);
                    selectedManualIsolator.colliderObjects[1].SetActive(false);
                }else{
                    selectedMotorIsolator.colliderObjects[2].SetActive(true);
                    selectedMotorIsolator.colliderObjects[1].SetActive(false);
                }
                break;
            case(Procedure.UNLOCK_MANUAL_ISOLATOR):
                selectedManualIsolator.colliderObjects[1].SetActive(false);
                selectedManualIsolator.colliderObjects[2].SetActive(false);
                // selectedManualIsolator.colliderObjects[3].SetActive(true);

                break;               
            case(Procedure.OPEN_MANUAL_ISOLATOR):
                selectedManualIsolator.colliderObjects[1].SetActive(false);
                selectedManualIsolator.colliderObjects[2].SetActive(false);
                selectedManualIsolator.colliderObjects[3].SetActive(false);
                selectedManualIsolator.colliderObjects[5].SetActive(true);
                // selectedManualIsolator.colliderObjects[1].GetComponent<CollideEvent>().setActionValue("ManualFingeringProcedure");

                break;
            case(Procedure.MANUAL_ISOLATER_FINGERING_PROCEDURE):
                selectedManualIsolator.colliderObjects[5].SetActive(false);
                selectedManualIsolator.colliderObjects[1].SetActive(true);
                break;
            case(Procedure.CALL_TRACKSIDE_PSC):
                audioSource.clip = audioList[6];                    
                PlayAudio();
                if(_isManualGameMode){
                    selectedManualIsolator.colliderObjects[1].SetActive(false);
                }else{
                    selectedMotorIsolator.colliderObjects[0].SetActive(false);
                }
                break;
        
            case(Procedure.LOCK_MANUAL_ISOLATOR):
                audioSource.clip = audioList[7];
                // audioManager.Play();
                PlayAudio();
                selectedManualIsolator.colliderObjects[4].SetActive(true);
                selectedManualIsolator.colliderObjects[1].SetActive(false);

                break;
            case(Procedure.MOTOR_TURN_TO_LOCAL):
                selectedMotorIsolator.colliderObjects[1].SetActive(false);
                selectedMotorIsolator.colliderObjects[2].SetActive(false);
                // selectedMotorIsolator.colliderObjects[3].SetActive(true);
            break;
            case(Procedure.MOTOR_POWER_SUPPLY_OFF):
                selectedMotorIsolator.colliderObjects[4].SetActive(true);
                selectedMotorIsolator.colliderObjects[1].SetActive(false);
                selectedMotorIsolator.colliderObjects[2].SetActive(false);
                selectedMotorIsolator.colliderObjects[3].SetActive(false);
            break;
            case(Procedure.MOTOR_CONTROL_UNLOCK):
                selectedMotorIsolator.colliderObjects[5].SetActive(true);
                selectedMotorIsolator.colliderObjects[4].SetActive(false);

            break;
            case(Procedure.MOTOR_HANDLE_UNLOCK):
                selectedMotorIsolator.colliderObjects[6].SetActive(true);
                selectedMotorIsolator.colliderObjects[5].SetActive(false);
            break;
            case(Procedure.MOTOR_OPEN_DOOR):
                selectedMotorIsolator.colliderObjects[7].SetActive(true);
                selectedMotorIsolator.colliderObjects[6].SetActive(false);

            break;
            case(Procedure.MOTOR_TURN_TO_MANUAL):
                selectedMotorIsolator.colliderObjects[8].SetActive(true);
                selectedMotorIsolator.colliderObjects[7].SetActive(false);

            break;
            case(Procedure.MOTOR_FINGERING_PROCEDURE):
                selectedMotorIsolator.colliderObjects[1].SetActive(true);
                selectedMotorIsolator.colliderObjects[8].SetActive(false);
                break;
            case(Procedure.MOTOR_CLOSE_DOOR):
                audioSource.clip = audioList[7];
                selectedMotorIsolator.animatorList[3].SetTrigger("Reset_To_Close_Door"); 
                selectedMotorIsolator.animatorList[5].SetTrigger("Handle_Open"); 

                PlayAudio();
                selectedMotorIsolator.colliderObjects[9].SetActive(true);
                selectedMotorIsolator.colliderObjects[1].SetActive(false);
            break;
            case(Procedure.MOTOR_HANDLE_LOCK):
                selectedMotorIsolator.colliderObjects[13].SetActive(true);
                selectedMotorIsolator.colliderObjects[9].SetActive(false);
            break;
            case(Procedure.MOTOR_CONTROL_LOCK):
                selectedMotorIsolator.colliderObjects[10].SetActive(true);
                selectedMotorIsolator.colliderObjects[13].SetActive(false);
            break;
            case(Procedure.MOTOR_PADLOCK_LOCK):
                selectedMotorIsolator.colliderObjects[11].SetActive(true);
                selectedMotorIsolator.colliderObjects[10].SetActive(false);
            break;

            case(Procedure.PUT_NOTICE4):
                if(_isManualGameMode){
                    selectedManualIsolator.colliderObjects[6].SetActive(true);
                    selectedManualIsolator.colliderObjects[4].SetActive(false);
                    selectedManualIsolator.colliderObjects[3].SetActive(false);
                }else{
                    Debug.Log("Hang notice");
                    selectedMotorIsolator.colliderObjects[11].SetActive(false);
                    selectedMotorIsolator.colliderObjects[12].SetActive(true);

                }

                break;
            case(Procedure.CONFIRM_GO_DCCB):
                playingPage.ShowEnterSign(true);
                break;
            case(Procedure.GO_DCCB):
                playingPage.ShowEnterSign(false);
                trackScene.switchGearSign.SetActive(true);
                break;
            case(Procedure.REMOVE_BACK_COVER):
                audioSource.clip = audioList[8];
                selectedGear.animatorList[5].SetTrigger("Reset_Back_Door"); 
                PlayAudio();
                foreach(SwitchGear gear in switchRoomScene.switchCabinateList){
                    gear.colliderObjects[6].SetActive(true);
                }
                break;
            case(Procedure.SELFTEST_TESTER):
            case(Procedure.PUT_ON_GLOVES):
            case(Procedure.USE_TESTER):
                foreach(SwitchGear gear in switchRoomScene.switchCabinateList){
                    gear.colliderObjects[6].SetActive(false);
                }
                selectedGear.colliderObjects[10].SetActive(true);
                switchRoomScene.roomEarthingStartCollider.enabled = true;
                selectedGear.colliderObjects[11].SetActive(true);
                break;
            case(Procedure.SECOND_SELFTEST): 
                Debug.Log("Second test");  
                break;
            case(Procedure.CONNECT_EARTHING):
                Debug.Log("Start Earthing");  
                break;
            case(Procedure.CALLING_PSC2):
                selectedGear.colliderObjects[11].SetActive(false);
                audioSource.clip = audioList[9];
                // audioManager.Play();
                PlayAudio();

                break;
            case(Procedure.GIVE_SAFETY_DOCUMENT):
                audioSource.clip = audioList[10];                    
                PlayAudio();
                if (GlobalConfig.instance.GetNetworkID() == 1)
                {
                    AMVRNetworkManager.instance.GetCurrentPlayer().SendAll("ShowSafetyDocument");
                }
                if (GlobalConfig.instance.GetNetworkID() == 2)
                {
                    AMVRNetworkManager.instance.GetCurrentPlayer().SendAll("PrepareReceiveSafetyDocument");
                }
                break;
            case(Procedure.REPORT):

                break;
            default:break;
        }
    }


    public void ActionCallResult(string action,string extraInfo =null ){
        switch(currentProcedure){
            case(Procedure.SAFETY_BRIEFING): //SafetyBriefing
            Debug.Log(">>" + action);
                    if(action == "Instructor"){
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "DebugModeStart" && _isDebugMode){
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    break;
            case(Procedure.SELECT_CABINATE):
                        if(action == "CorrectCabinate"){
                            currentProcedure +=1;
                            selectedGear.safetyBarrier.gameObject.SetActive(true);
                            if(!ReportManager.instance.checkList.ContainsKey("SelectWrongCainate")){
                                ReportManager.instance.UpdateReportStep(2,true);
                            }
                            ReadStepInstructions();
                        }else if(action == "Cabinate"){
                            ReportManager.instance.UpdateRemark(2, "Wrong Cabinet");
                            Debug.Log("Wrong Gear report and select again" );
                        }
                    break;                
            case(Procedure.PUT_NOTICE1): //SelectCabinet
                    if(action == "PutUpNotice"){
                        if(!extraInfo.Contains("Danger")){
                            ReportManager.instance.UpdateRemark(3,"Wrong Notice");
                        }
                        if(selectedGear.colliderObjects[7].GetComponent<VRTK_SnapDropZone>().GetCurrentSnappedObject() != null &&
                            selectedGear.colliderObjects[13].GetComponent<VRTK_SnapDropZone>().GetCurrentSnappedObject() != null){
                            ReportManager.instance.UpdateReportStep(3,true);
                            currentProcedure +=1;
                            ReadStepInstructions();
                        }
                    }
                    break;      
            case(Procedure.CALLING_PSC):  //PutNotice
            case(Procedure.FINGERING_PROCEDURES): //CallingPSC                        
            case(Procedure.DCCB_CRTL_LOCAL):
            case(Procedure.PUT_NOTICE2):
            case(Procedure.WAIT_FOR_PSC):
            case(Procedure.MANOEUVRING_HANDLE):
                    if(action == "Phone" && !_DCCB_PSCCalled ){ // Call PSC 
                        if(!ReportManager.instance.CheckStep(4)){
                            AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                            if(playerController.IsPlayerOne()){
                                playerController.UsePhoneRFC(0);
                            }
                            _waitForPSCCall = true;
                            _DCCB_PSCCalled = true;
                            currentProcedure =Procedure.WAIT_FOR_PSC;
                            ReadStepInstructions();
                        }
                            // ReportManager.instance.UpdateReport("CalledPSC",true);
                    }else if(action == "FingeringProcedures"){
                            // audioManager.clip = audioList[4];
                            Debug.Log("SetTrigger: Finger_Procedure");
                            selectedGear.colliderObjects[1].SetActive(false);
                            selectedGear.FingerProcedureObjects[0].SetActive(true);
                            // ReportManager.instance.UpdateReport("DCCB_Fingering_procedures",true);
                            ReportManager.instance.UpdateReportStep(5,true);

                            AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                            currentProcedure =Procedure.MANOEUVRING_HANDLE;
                            ReadStepInstructions();
                    }else if(action == "DCCB"){
                            Debug.Log("DccB");
                            selectedGear.colliderObjects[2].SetActive(false);
                            // selectedGear.triggerObjects[1].SetActive(tr  ue);
                            // Invoke("TriggerAnimation",5f);

                            selectedGear.animatorList[0].SetTrigger("Step1_Change_Control_To_Local");
                            AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");

                            // ReportManager.instance.UpdateReport("DCCB_Control_To_local",true);
                            ReportManager.instance.UpdateReportStep(7,true);

                            currentProcedure = Procedure.MANOEUVRING_HANDLE;
                            ReadStepInstructions();
                    }else if(_waitForPSCCall && (action == "Instructor" || (action =="DebugModeStart" && _isDebugMode))){
                            ReportManager.instance.UpdateReportStep(4,true);
                            _waitForPSCCall = false;
                            selectedGear.colliderObjects[1].SetActive(true);
                            AudioManager.instance.PlayAudioClip("P1_Manoeuvring_handle_Finish");
                            playingPage.ShowEnterSign(false);
                            selectedGear.animatorList[2].SetTrigger("Finger_Procedure"); // after finish called 
                            currentProcedure = Procedure.MANOEUVRING_HANDLE;
                            ReadStepInstructions();
                    }else if(action == "UnlockPadlock"){
                            AudioManager.instance.PlayAudioClip("P1_Unlock_Padlock");
                            ReportManager.instance.UpdateReportStep(6,true);

                            // Suppose need Check Step;

                            selectedGear.animatorList[1].SetTrigger("Step2_Unlock_padlock");
                            selectedGear.colliderObjects[3].SetActive(false);
                            selectedGear.colliderObjects[8].SetActive(true);
                            selectedGear.colliderObjects[8].GetComponent<BoxCollider>().enabled = true;

                            Debug.Log("Insert handle");
                    }else if(action == "Handle"){
                            if(!(ReportManager.instance.CheckStep(4)&&   // Check Wrong step
                                ReportManager.instance.CheckStep(6)&&
                                ReportManager.instance.CheckStep(5)&&
                                ReportManager.instance.CheckStep(7))){

                                ReportManager.instance.UpdateRemark(8,"Explosion");
                                ReportManager.instance.UpdateReportStep(4,false);
                                ReportManager.instance.UpdateReportStep(5,false);
                                ReportManager.instance.UpdateReportStep(6,false);
                                ReportManager.instance.UpdateReportStep(7,false);

                                currentProcedure = Procedure.CALLING_PSC;

                                selectedGear.PlayExplosion(0);
                                selectedGear.colliderObjects[1].SetActive(false);

                                selectedGear.animatorList[0].SetTrigger("Reset_Change_Control_To_Local");
                                selectedGear.animatorList[1].SetTrigger("Reset_Front_Door");
                                selectedGear.animatorList[2].SetTrigger("Reset_Trolley"); // after finish called 
                                _waitForPSCCall = false;
                                _DCCB_PSCCalled = false;

                                Invoke("ReadStepInstructions",EXPLOSIONTIME);
                                Invoke("DestroyHandle",EXPLOSIONTIME);
                                // ReadStepInstructions();
                            }else
                            {
                                selectedGear.colliderObjects[8].GetComponent<BoxCollider>().enabled = false;
                            }
                    }else if(action == "HandleRotateFinish"){
                                Debug.Log("Done");
                                AudioManager.instance.PlayAudioClip("P1_Manoeuvring_handle_Finish");
                                DestroyHandle();
                                selectedGear.animatorList[2].SetTrigger("Step3_Signal_Red_to_Green");
                                ReportManager.instance.UpdateReportStep(8,true);
                                currentProcedure = Procedure.OPEN_FRONT_COVER;
                                ReadStepInstructions();
                    }
                    break;           
            case(Procedure.OPEN_FRONT_COVER): //Open Front Door
                    if(action == "OpenFrontDoor"){
                        selectedGear.animatorList[1].SetTrigger("Step4_Open_Front_Door");
                        currentProcedure +=1;
                        ReadStepInstructions();     
                    }
                    break;          
            case(Procedure.CONTROL_CABLE_PLUG): //Plug
                    if(action == "Plug"){
                        selectedGear.animatorList[3].SetTrigger("Step5_Control_cables_plug");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    break;           
            case(Procedure.HSCB_TROLLEY): //
                    if(action == "TrolleyPulled"){
                        selectedGear.SetReadyToPull(false);
                        selectedGear.SetFinishedPull();
                        ReportManager.instance.UpdateReportStep(9,true);
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    break;
            case(Procedure.BUSBAR_DISCONNECT):
            case(Procedure.PUT_NOTICE3): // PutOnSecondNotice
                    if(action == "BusBarDisconnect"){
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        ReportManager.instance.UpdateReportStep(10,true); 
                        selectedGear.colliderObjects[12].GetComponent<BoxCollider>().enabled = false;

                        selectedGear.animatorList[4].SetTrigger("Busbar_Lock");
                        currentProcedure = Procedure.PUT_NOTICE3;
                        ReadStepInstructions();
                    }
                    if(action == "PutUpNotice"){
                        if(ReportManager.instance.CheckStep(10)){
                            Debug.Log("String name "+ extraInfo);
                            if(!extraInfo.Contains("Danger") && extraInfo.Contains("NoticeHanger2")){
                                ReportManager.instance.UpdateRemark(11,"Wrong Notice");
                            }
                            if(!extraInfo.Contains("Caution") && extraInfo.Contains("NoticeHangerCenter")){
                                ReportManager.instance.UpdateRemark(11,"Wrong Notice");
                            }
                            if(selectedGear.colliderObjects[9].GetComponent<VRTK_SnapDropZone>().GetCurrentSnappedObject() != null &&
                                selectedGear.colliderObjects[14].GetComponent<VRTK_SnapDropZone>().GetCurrentSnappedObject() != null){
                                Debug.Log("test all");
                                ReportManager.instance.UpdateReportStep(11,true);
                                currentProcedure = Procedure.CALLING_TC;
                                ReadStepInstructions();
                            }else Debug.Log("one only");
                        }else{
                            ReportManager.instance.UpdateRemark(10,"No Disconnect before putting notice");
                            Destroy(selectedGear.colliderObjects[9].GetComponent<VRTK_SnapDropZone>().GetCurrentSnappedObject());
                            selectedGear.colliderObjects[9].GetComponent<BoxCollider>().enabled=true;
                            selectedGear.PlayExplosion(0);
                        }
                    }
                    break; 
            case(Procedure.CALLING_TC):
                if(action == "Phone"){
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                    ReportManager.instance.UpdateReportStep(12,true);

                    if(playerController.IsPlayerOne()){
                        playerController.UsePhoneRFC(1);
                    }
                    if(_isManualGameMode)
                        currentProcedure = Procedure.CONFIRM_GO_TRACKSIDE;
                    else
                        currentProcedure = Procedure.CALLING_MOTOR_PSC;
                    ReadStepInstructions();
                }
                break;
            case(Procedure.CALLING_MOTOR_PSC):
                if(!_isManualGameMode && action == "Phone"){
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                    if(playerController.IsPlayerOne()){
                        playerController.UsePhoneRFC(0);
                    }
                    currentProcedure = Procedure.CONFIRM_GO_TRACKSIDE;
                    selectedMotorIsolator.animatorList[3].SetTrigger(0);
                    ReadStepInstructions();
                }
                break;
            case(Procedure.CONFIRM_GO_TRACKSIDE):
                if(action == "Instructor" || (action =="DebugModeStart" && _isDebugMode)){
                    currentProcedure = Procedure.GO_TRACKSIDE;
                    ReadStepInstructions();
                }   
                break;
            case(Procedure.GO_TRACKSIDE): //ToTrackSide
                
                if(action == "GoTrackSide" || action == "Teleport"){
                    switchRoomScene.trackSideSign.SetActive(false);                    
                    switchRoomScene.gameObject.SetActive(false);
                    trackScene.gameObject.SetActive(true);
                    switchRoomScene.gameObject.transform.localPosition = new Vector3(0,0,3);
                    currentProcedure = Procedure.SELECT_ISOLATOR;
                    ReadStepInstructions();
                }
                break;
            case(Procedure.SELECT_ISOLATOR):
                    switchRoomScene.trackSideSign.SetActive(false);
                    if(action == "ManualFingeringProcedure"){
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        ReportManager.instance.UpdateReportStep(13,true);
                        selectedManualIsolator.FingerProcedureObjects[0].SetActive(true);

                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "MotorFingeringProcedure"){
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        selectedMotorIsolator.FingerProcedureObjects[0].SetActive(true);
                        ReportManager.instance.UpdateReportStep(13,true);
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action =="Isolator")
                    {
                        ReportManager.instance.UpdateRemark(13,"Wrong Isolator");
                    }
                break;
                case(Procedure.CABLE_START):
                case(Procedure.CABLE_END):
                    if(action == "CableStart"){
                        Debug.Log("start point ");
                        currentProcedure =Procedure.CABLE_END;
                        ReadStepInstructions();
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        if(_isManualGameMode){
                            selectedManualIsolator.FingerProcedureObjects[1].SetActive(true);
                        }
                        else{
                            selectedMotorIsolator.FingerProcedureObjects[1].SetActive(true);
                        }
                    }
                    if(action == "CableEnd"){
                        Debug.Log("End point");
                        if(_isManualGameMode){
                            currentProcedure = Procedure.UNLOCK_MANUAL_ISOLATOR;
                            selectedManualIsolator.FingerProcedureObjects[2].SetActive(true);
                            AudioManager.instance.PlayAudioClip("Com_Fingering procedures");

                        }else{
                            currentProcedure = Procedure.MOTOR_TURN_TO_LOCAL;
                            selectedMotorIsolator.FingerProcedureObjects[2].SetActive(true);
                            AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        }
                        ReadStepInstructions();
                        ReportManager.instance.UpdateReportStep(14,true);
                    }
                    if(action == "UnlockManualIsolator"){
                        manualIsolatorAnimator.Play("Manual_Unlock");
                        AudioManager.instance.PlayAudioClip("P2_Unlock_Manual_Isolator");
                        selectedManualIsolator.colliderObjects[1].GetComponent<CollideEvent>().setActionValue("CableFingeringProcedure");
                        ReportManager.instance.UpdateReportStep(15,true);
                        // selectedManualIsolator.triggerObjects[0].SetActive(true);
                        // Invoke("TriggerAnimation",5f);
                        Debug.Log("Unlock");
                        currentProcedure = Procedure.OPEN_MANUAL_ISOLATOR;
                        ReadStepInstructions();
                    }
                    if(action == "TurnToLocal"){
                        Debug.Log("Skip Finger procedure for motor");
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        currentProcedure = Procedure.MOTOR_POWER_SUPPLY_OFF;
                        selectedMotorIsolator.animatorList[0].SetTrigger("Control_Box_Turn_to_Local");
                        selectedMotorIsolator.colliderObjects[1].GetComponent<CollideEvent>().setActionValue("CableFingeringProcedure");
                        ReadStepInstructions();
                    }
                break;
                case(Procedure.UNLOCK_MANUAL_ISOLATOR):
                case(Procedure.OPEN_MANUAL_ISOLATOR):
                case(Procedure.MANUAL_ISOLATER_FINGERING_PROCEDURE):
                case(Procedure.LOCK_MANUAL_ISOLATOR):
                    if(action == "UnlockManualIsolator"){
                        selectedManualIsolator.colliderObjects[1].GetComponent<CollideEvent>().setActionValue("CableFingeringProcedure");
                        manualIsolatorAnimator.Play("Manual_Unlock");
                        AudioManager.instance.PlayAudioClip("P2_Unlock_Manual_Isolator");        
                        Debug.Log("Unlock");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "Phone"){
                        AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                        if(playerController.IsPlayerOne()){
                            playerController.UsePhoneRFC(0);
                        }
                        // ReportManager.instance.UpdateReportStep(16,true);
                        currentProcedure = Procedure.GO_DCCB;
                        ReadStepInstructions();
                    }
                    if(action == "OpenManualIsolator"){
                        Debug.Log("Open");
                        manualIsolatorAnimator.Play("Manual_Open");
                        AudioManager.instance.PlayAudioClip("Com_Open_or_Close_Door");

                        selectedManualIsolator.FingerProcedureObjects[0].SetActive(false);
                        selectedManualIsolator.FingerProcedureObjects[1].SetActive(false);                            
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "CableFingeringProcedure"){
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        selectedManualIsolator.FingerProcedureObjects[1].SetActive(true);
                        ReportManager.instance.UpdateReportStep(15,true);                       
                        currentProcedure = Procedure.LOCK_MANUAL_ISOLATOR;
                        ReadStepInstructions();
                    }
                     if(action == "LockManualIsolator"){
                        Debug.Log("Lock");
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        manualIsolatorAnimator.Play("Manual_Lock");
                        ReportManager.instance.UpdateReportStep(16,true);
                        currentProcedure =Procedure.PUT_NOTICE4;
                        ReadStepInstructions();
                    }
                    break;                
                case(Procedure.CALL_TRACKSIDE_PSC):
                    if(action == "Phone"){
                        AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                        if(playerController.IsPlayerOne()){
                            playerController.UsePhoneRFC(0);
                        }
                        ReportManager.instance.UpdateReportStep(18,true);
                        currentProcedure = Procedure.CONFIRM_GO_DCCB;
                        ReadStepInstructions();
                    }
                    break;
                case(Procedure.MOTOR_TURN_TO_LOCAL):
                case(Procedure.MOTOR_POWER_SUPPLY_OFF):
                case(Procedure.MOTOR_CONTROL_UNLOCK):
                case(Procedure.MOTOR_HANDLE_UNLOCK):
                case(Procedure.MOTOR_OPEN_DOOR):
                case(Procedure.MOTOR_TURN_TO_MANUAL):
                case(Procedure.MOTOR_FINGERING_PROCEDURE):
                case(Procedure.MOTOR_CLOSE_DOOR):
                case(Procedure.MOTOR_HANDLE_LOCK):
                case(Procedure.MOTOR_CONTROL_LOCK):
                case(Procedure.MOTOR_PADLOCK_LOCK):
                    if(action == "TurnToLocal"){
                        Debug.Log("control box turn to local ");
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        selectedMotorIsolator.colliderObjects[1].GetComponent<CollideEvent>().setActionValue("CableFingeringProcedure");

                        currentProcedure = Procedure.MOTOR_POWER_SUPPLY_OFF;
                        selectedMotorIsolator.animatorList[0].SetTrigger("Control_Box_Turn_to_Local");
                        ReadStepInstructions();
                    }
                    if(action == "PowerSupplyTurnOff"){
                        selectedMotorIsolator.animatorList[1].SetTrigger("Control_Power_Supply_TurnOff");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "Phone"){
                        AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                        if(playerController.IsPlayerOne()){
                            playerController.UsePhoneRFC(0);
                        }
                        // ReportManager.instance.UpdateReportStep(16,true);
                        currentProcedure = Procedure.CONFIRM_GO_DCCB;
                        ReadStepInstructions();
                    }
                    if(action == "ControlUnlock"){
                        selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_Unlock_Step1");
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action.Contains("HandleUnlock")){
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        selectedMotorIsolator.animatorList[5].SetTrigger("Handle_Open");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "ControlOpenDoor"){
                        selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_OpenDoor");
                        AudioManager.instance.PlayAudioClip("Com_Open_or_Close_Door");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "TurnToManual"){
                        selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_Turn_to_Manual");
                        selectedMotorIsolator.FingerProcedureObjects[1].SetActive(false);
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "CableFingeringProcedure"){
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        selectedMotorIsolator.FingerProcedureObjects[0].SetActive(true);
                        selectedMotorIsolator.FingerProcedureObjects[1].SetActive(true);
                        ReportManager.instance.UpdateReportStep(15,true);                       
                        currentProcedure = Procedure.MOTOR_CLOSE_DOOR;
                        ReadStepInstructions();
                    }
                    if(action == "ControlCloseDoor"){
                        selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_CloseDoor");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "HandleLock"){
                        selectedMotorIsolator.animatorList[5].SetTrigger("Handle_Close");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action == "ControlLock"){
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_Lock_Step1");
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    if(action.Contains("PadlockLock")){
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        selectedMotorIsolator.animatorList[4].gameObject.SetActive(true);
                        currentProcedure = Procedure.PUT_NOTICE4;
                        ReportManager.instance.UpdateReportStep(16,true);
                        ReadStepInstructions();
                    }
                    break;
                case(Procedure.PUT_NOTICE4):
                    if(action == "PutUpNotice"){
                        ReportManager.instance.UpdateReportStep(17,true);
                        if(!extraInfo.Contains("Caution")){
                           ReportManager.instance.UpdateRemark(17,"Wrong Notice");
                        }
                        currentProcedure = Procedure.CALL_TRACKSIDE_PSC;
                        ReadStepInstructions();
                    }
                    if(action == "Phone"){
                        AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                        if(playerController.IsPlayerOne()){
                            playerController.UsePhoneRFC(0);
                        }
                        ReportManager.instance.UpdateReportStep(18,true);
                        currentProcedure = Procedure.CONFIRM_GO_DCCB;
                        ReadStepInstructions();
                    }
                    break; 
                case(Procedure.CONFIRM_GO_DCCB):
                    if(action == "Instructor" || (action =="DebugModeStart" && _isDebugMode)){
                        currentProcedure = Procedure.GO_DCCB;
                        ReadStepInstructions();
                    }   
                    break;
                case(Procedure.GO_DCCB):
                    if(action == "GoSwitchGearRoom" || action == "Teleport"){
                        switchRoomScene.gameObject.SetActive(true);
                        switchRoomScene.ChangeIsoCameraToPart3();
                        trackScene.gameObject.SetActive(false);
                        trackScene.switchGearSign.SetActive(false);
                        playerController.SendAll("SpotLightEnable", false);
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    break;
                case(Procedure.REMOVE_BACK_COVER):
                    if(action == "CorrectCabinate"){
                        if(!ReportManager.instance.CheckStep(14) || !ReportManager.instance.CheckStep(16) ){
                            selectedGear.PlayExplosion(1);
                            if(!ReportManager.instance.CheckStep(14))
                                ReportManager.instance.UpdateRemark(19,"Didn't finish Fingering procedure ");
                            else if(!ReportManager.instance.CheckStep(16)){
                                ReportManager.instance.UpdateRemark(16,"No Notice");
                            }
                            if(_isManualGameMode){
                                selectedManualIsolator.animator.SetTrigger("Reset_Manual_Isolator"); 
                                selectedManualIsolator.colliderObjects[6].GetComponent<NoticeSnapEvent>().DestroySnappedNotice();
                                selectedManualIsolator.FingerProcedureObjects[0].SetActive(false);
                                selectedManualIsolator.FingerProcedureObjects[1].SetActive(false);                            
                                selectedManualIsolator.FingerProcedureObjects[2].SetActive(false);
                                selectedManualIsolator.colliderObjects[1].GetComponent<CollideEvent>().setActionValue("CableStart");
                            }
                            else{
                                selectedMotorIsolator.animatorList[0].SetTrigger("Reset_Control_Box");  
                                selectedMotorIsolator.animatorList[1].SetTrigger("Reset_Control_Power_Supply_TurnOff");  
                                selectedMotorIsolator.animatorList[2].SetTrigger("Reset_Isolator");  
                                selectedMotorIsolator.animatorList[3].SetTrigger("Reset_Motor_Isolator_Control");  
                                selectedMotorIsolator.animatorList[4].gameObject.SetActive(false);
                                selectedMotorIsolator.animatorList[4].SetTrigger("Reset_Player1_Motor_Isolator_Padlock_Lock");  
                                selectedMotorIsolator.animatorList[5].SetTrigger("Reset_Handle"); 
                                selectedMotorIsolator.colliderObjects[12].GetComponent<NoticeSnapEvent>().DestroySnappedNotice();
                                selectedMotorIsolator.FingerProcedureObjects[0].SetActive(false);
                                selectedMotorIsolator.FingerProcedureObjects[1].SetActive(false);                            
                                selectedMotorIsolator.FingerProcedureObjects[2].SetActive(false);
                                selectedMotorIsolator.colliderObjects[1].GetComponent<CollideEvent>().setActionValue("CableStart");
                            }
                            currentProcedure = Procedure.SELECT_ISOLATOR;
                            ReportManager.instance.UpdateReportStep(14,false);
                            ReportManager.instance.UpdateReportStep(15,false);
                            ReportManager.instance.UpdateReportStep(16,false);
                            ReportManager.instance.UpdateReportStep(17,false);
                            ReportManager.instance.UpdateReportStep(18,false);
                            Invoke("SwitchToTrackSide",4);
                            Invoke("ReadStepInstructions",4);
                        }else if(!ReportManager.instance.CheckStep(17)){
                            selectedGear.PlayExplosion(1);
                            ReportManager.instance.UpdateRemark(17,"Didn't Lock");

                            if(_isManualGameMode){
                                selectedManualIsolator.FingerProcedureObjects[0].SetActive(false);
                                selectedManualIsolator.FingerProcedureObjects[1].SetActive(false);                            
                                selectedManualIsolator.FingerProcedureObjects[2].SetActive(false);
                                ReportManager.instance.UpdateReportStep(15,false);
                                currentProcedure = Procedure.UNLOCK_MANUAL_ISOLATOR;
                                selectedManualIsolator.animator.SetTrigger("Reset_Manual_Isolator"); 
                                selectedManualIsolator.colliderObjects[6].GetComponent<NoticeSnapEvent>().DestroySnappedNotice();
                            }
                            else{
                                selectedMotorIsolator.FingerProcedureObjects[0].SetActive(false);
                                selectedMotorIsolator.FingerProcedureObjects[1].SetActive(false);                            
                                selectedMotorIsolator.FingerProcedureObjects[2].SetActive(false);
                                // selectedMotorIsolator.animatorList[3].SetTrigger("Reset_Motor_Isolator_Control");
                                // selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_Unlock_Step1"); 
                                // selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_OpenDoor"); 
                                // selectedMotorIsolator.animatorList[3].SetTrigger("Motor_Isolator_Control_Turn_t o_Manual");
                                selectedMotorIsolator.animatorList[4].gameObject.SetActive(false);
                                selectedMotorIsolator.animatorList[4].SetTrigger("Reset_Player1_Motor_Isolator_Padlock_Lock");  
                                selectedMotorIsolator.animatorList[5].SetTrigger("Reset_Handle"); 

                                foreach(GameObject collider in selectedMotorIsolator.colliderObjects){
                                    collider.SetActive(false);
                                }
                                selectedMotorIsolator.colliderObjects[12].GetComponent<NoticeSnapEvent>().DestroySnappedNotice();
                                currentProcedure = Procedure.MOTOR_CLOSE_DOOR;
                            }
                            ReportManager.instance.UpdateReportStep(16,false);
                            ReportManager.instance.UpdateReportStep(17,false);
                            ReportManager.instance.UpdateReportStep(18,false);
                            Invoke("SwitchToTrackSide",4);
                            Invoke("ReadStepInstructions",4);
                        }else{
                            ReportManager.instance.UpdateReportStep(19,true);
                            AudioManager.instance.PlayAudioClip("COM_Open_or_Close_Door");
                            selectedGear.animatorList[5].SetTrigger("Step6_Open_Back_Door");
                            currentProcedure +=1;
                            ReadStepInstructions();
                        }
                    }else if (action.StartsWith("OpenBackDoor")){
                        int doorIndex = System.Int32.Parse(action.Substring(action.Length-1));
                        Debug.Log("Show index" + doorIndex);
                        switchRoomScene.switchCabinateList[doorIndex].PlayExplosion(1);
                        ReportManager.instance.UpdateRemark(19,"Wrong Cover");
                    }
                    break;
                case(Procedure.SELFTEST_TESTER):
                case(Procedure.PUT_ON_GLOVES):
                case(Procedure.USE_TESTER):
                case(Procedure.SECOND_SELFTEST):
                case(Procedure.CONNECT_EARTHING):
                    if(action == "MeggerTest"){
                        Debug.Log("First megger Test");
                        ReportManager.instance.UpdateReportStep(20,true);
                        currentProcedure = Procedure.PUT_ON_GLOVES;
                        // ReportManager.instance.UpdateReportStep(20,true);
                        // AudioManager.instance.PlayAudioClip("P3_Tester");
                        // switchRoomScene.MeggerTesterTool.GetComponentInChildren<Megger>().CollideAction();
                        // switchRoomScene.MeggerTesterTool.GetComponentInChildren<DCTester>().CollideAction();
                        ReadStepInstructions();
                    }
                    if(action == "Gloves" ){
                        // currentProcedure = Procedure.USE_TESTER;
                        ReadStepInstructions();
                    }
                    if(action =="TesterConnected"){
                        if(ReportManager.instance.CheckStep(20) && _player1GlovesStatus ){
                            AudioManager.instance.PlayAudioClip("P3_Tester");
                            ReportManager.instance.UpdateReportStep(21,true);
                            ReportManager.instance.UpdateReportStep(22,true);
                            // switchRoomScene.MeggerTesterTool.GetComponentInChildren<Megger>().CollideAction();
                            // switchRoomScene.MeggerTesterTool.GetComponentInChildren<DCTester>().CollideAction();
                            selectedGear.colliderObjects[10].SetActive(false);
                            switchRoomScene.MeggerTesterTool.GetComponentInChildren<Megger>().gameObject.GetComponentInChildren<CollideEvent>().setActionValue("SecondMeggerTest");
                            currentProcedure = Procedure.SECOND_SELFTEST;
                            ReadStepInstructions();
                        }else{
                            selectedGear.PlayExplosion(1);
                            Debug.Log("Tester exploded" );
                            Debug.Log("Glove? " + _player1GlovesStatus);
                            ReportManager.instance.UpdateReportStep(19,false);
                            ReportManager.instance.UpdateReportStep(20,false);
                            ReportManager.instance.UpdateReportStep(21,false);
                            ReportManager.instance.UpdateRemark(22, "Explosion");
                            switchRoomScene.MeggerTesterTool.GetComponentInChildren<Megger>().gameObject.GetComponentInChildren<CollideEvent>().setActionValue("MeggerTest");
                            selectedGear.colliderObjects[10].SetActive(false);
                            currentProcedure =Procedure.REMOVE_BACK_COVER;
                            UIPanelActionCall("MeggerTester");
                            Invoke("ReadStepInstructions",EXPLOSIONTIME);
                            AMVRNetworkManager.instance.GetCurrentPlayer().PlayerChangeGlovesRFC(false);
                        }
                    }
                    if(action == "SecondMeggerTest"){
                        Debug.Log("Second megger Test");
                        ReportManager.instance.UpdateReportStep(23,true);
                        // AudioManager.instance.PlayAudioClip("P3_Tester");
                        // switchRoomScene.MeggerTesterTool.GetComponentInChildren<Megger>().CollideAction();
                        // switchRoomScene.MeggerTesterTool.GetComponentInChildren<DCTester>().CollideAction();
                        currentProcedure = Procedure.CONNECT_EARTHING;
                        ReadStepInstructions();
                    }
                    if(action == "EarthingConnectionStart"){
                        if(ReportManager.instance.CheckStep(23)&& _player1GlovesStatus){
                            switchRoomScene.earthingToWall.SetActive(true);
                            switchRoomScene.earthingToWall.GetComponent<Animator>().SetTrigger("Earthing_to_wall_snap");
                            AudioManager.instance.PlayAudioClip("P3_LockEarthing");

                            if(playerController.IsPlayerOne()){
                                playerController.PlayerConnectEarthingCableToWallRFC();
                            }
                            _connectedEarthingStart=true;
                            Debug.Log("earthing True");          
                        }else{
                            selectedGear.PlayExplosion(1);
                            currentProcedure =Procedure.REMOVE_BACK_COVER;
                            ReportManager.instance.UpdateRemark(24,"Unfinished Test");
                            ReportManager.instance.UpdateReportStep(19,false);
                            ReportManager.instance.UpdateReportStep(20,false);
                            ReportManager.instance.UpdateReportStep(21,false);
                            ReportManager.instance.UpdateReportStep(22,false);
                            ReportManager.instance.UpdateReportStep(23,false);
                            AMVRNetworkManager.instance.GetCurrentPlayer().PlayerChangeGlovesRFC(false);
                            switchRoomScene.MeggerTesterTool.GetComponentInChildren<Megger>().gameObject.GetComponentInChildren<CollideEvent>().setActionValue("MeggerTest");
                            UIPanelActionCall("Earthing");
                            Invoke("ReadStepInstructions",EXPLOSIONTIME);
                        }
                    }else if (action == "EarthingConnectionEnd"){
                        if(_connectedEarthingStart == true && _player1GlovesStatus){
                            AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                            ReportManager.instance.UpdateReportStep(24,true);
                            currentProcedure = Procedure.CALLING_PSC2;
                            // TNetPlayer.
                            if(playerController.IsPlayerOne()){  
                                AMVRNetworkManager.instance.GetCurrentPlayer().PlayerActiveEarthingSetRFC(false);
                            }
                            // selectedGear.EarthingToSwitchGear.GetComponentInChildren<CableComponent>().SetCableEndPoint(switchRoomScene.earthingToWall.transform.Find("to_wall_cable"));
                            selectedGear.EarthingToSwitchGear.SetActive(true);

                            ReadStepInstructions();
                        }else{
                            selectedGear.PlayExplosion(1);
                            ReportManager.instance.UpdateReportStep(19,false);
                            ReportManager.instance.UpdateReportStep(20,false);
                            ReportManager.instance.UpdateReportStep(21,false);
                            ReportManager.instance.UpdateReportStep(22,false);
                            ReportManager.instance.UpdateReportStep(23,false);
                            switchRoomScene.MeggerTesterTool.GetComponentInChildren<Megger>().gameObject.GetComponentInChildren<CollideEvent>().setActionValue("MeggerTest");
                            ReportManager.instance.UpdateRemark(24,"Incorrect Order");
                            switchRoomScene.roomEarthingStartCollider.enabled = false;
                            selectedGear.colliderObjects[11].SetActive(false);
                            currentProcedure =Procedure.REMOVE_BACK_COVER;
                            _connectedEarthingStart = false;
                            AMVRNetworkManager.instance.GetCurrentPlayer().PlayerChangeGlovesRFC(false);
                            UIPanelActionCall("Earthing");
                            Invoke("ReadStepInstructions",EXPLOSIONTIME);
                        }
                    }
                    
                    break;
                case(Procedure.CALLING_PSC2):
                    if(action == "Phone"){
                        AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                        if(playerController.IsPlayerOne()){
                            playerController.phone.CallPhone(0);
                            playerController.UsePhoneRFC(0);
                        }
                        // ReportManager.instance.UpdateReport("CalledPSC2",true);
                        ReportManager.instance.UpdateReportStep(25,true);

                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    break;
                case(Procedure.GIVE_SAFETY_DOCUMENT):
                    if(action == "Report"){
                        AudioManager.instance.PlayAudioClip("Com_Lock_or_Unlock");
                        ReportManager.instance.UpdateReportStep(26,true);
                        currentProcedure +=1;
                        ReadStepInstructions();
                    }
                    break;
                case(Procedure.REPORT):
                if(action == "FinishTraining"){
                    audioSource.clip = audioList[11];
                    PlayAudio();
                    ReportManager.instance.UpdateReportStep(27,true);
                    Invoke("GameEnd",5);
                }
                    break;
            default:
                    break;

        }
    }
    public void BackToWaitingRoom()
    {        
        if (GlobalConfig.instance.IsServer())
        {
            startPage.SetActive(true);
        }

        ReportManager.instance.Init();
        
        AMVRNetworkManager.instance.GetCurrentPlayer().PlayerInitRFC();

        InitScene();
    } 
    public void TriggerAnimation(){
        foreach(GameObject obj in selectedGear.FingerProcedureObjects){
            obj.SetActive(false);
        }        
        foreach(GameObject obj in selectedManualIsolator.FingerProcedureObjects){
            obj.SetActive(false);
        }
    }

    public void PlayAudio(){

        if(_isVoiceOver){
            audioSource.Play();
            // _PlayingAudioToSkip = true;
           } 
    }

    public void SetTrolleyValue(float z)
    {
        selectedGear.trolleyController.SetValue(z);
    }

    public void SetRotatorAngle(float angle)
    {
        selectedGear.SetRotatorAngle(angle);
    }

    public void SetRotator(GameObject obj)
    {
        selectedGear.SetRotator(obj);
    }
    public void DestroyHandle(){
        Destroy(selectedGear.rotator);
    }

    public void PasteSafetyDoc()
    {
        switchRoomScene.folderSafetyDoc.SetActive(true);
        switchRoomScene.folderSafetyDocBlank.SetActive(false);

        // ReportManager.instance.UpdateReportStep(27,true);

        // Invoke("GameEnd",5);
        ActionCallResult("FinishTraining");
    }

    public void TesterPenInit(){
        AMVRNetworkManager.instance.GetCurrentPlayer().testerPens.Add(selectedGear.testerBlackClip);
    }
    public void MeggerTesterInit(List<GameObject> testerPens)
    {
        // if(AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() == 1)
        //    switchRoomScene.MeggerTesterTool.GetComponentInChildren<DCTester>().redCableStart.GetComponent<CableComponent>().SetCableEndPoint(testerPens[0].transform.GetChild(0));

        // switchRoomScene.MeggerTesterTool.GetComponentInChildren<DCTester>().blackCableStart.GetComponent<CableComponent>().SetCableEndPoint(testerPens[testerPens.Count-1].transform.GetChild(0));
    }
    public void ActivateMeggerTester(bool value){
        switchRoomScene.MeggerTesterTool.SetActive(value);
    }
    public Transform GetCurrentEarthingWallTransfom(){
        return switchRoomScene.earthingToWall.transform.Find("to_wall_cable");
    }

    public GameObject safetyDocumentClone
    {
        get
        {
            return switchRoomScene.safetyDocumentClone;
        }
        set
        {
            switchRoomScene.safetyDocumentClone = value;
        }
    }

    public void SwitchToTrackSide(){
        switchRoomScene.gameObject.SetActive(false);
        trackScene.gameObject.SetActive(true);
    }

    public GameObject GetTesterBlackClip()
    {
        return selectedGear.testerBlackClip;
    }
    public void SetPlayer1GloveBool(bool value){
        _player1GlovesStatus = value;
    }
}
