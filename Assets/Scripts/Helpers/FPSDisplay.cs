﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FPSDisplay : MonoBehaviour
{
    const int BUFFER_SIZE = 100;
    List<float> deltaTimes = new List<float>();
	float deltaTime = 0.0f;
    public bool showFPS = false;

    private void Start() {
// #if !UNITY_EDITOR
//         showFPS = false;
// #endif
    }
 
	void Update()
	{
        if (Input.GetKeyDown(KeyCode.F1))
        {
            showFPS = !showFPS;
        }

		deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
	}
 
	void OnGUI()
	{
        if (showFPS)
        {
            int w = Screen.width, h = Screen.height;
    
            GUIStyle style = new GUIStyle();
    
            Rect rect = new Rect(0, 0, w, h * 2 / 100);
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / 100 * 2;
            style.normal.textColor = new Color (0.0f, 0.0f, 0.5f, 1.0f);
            // float msec = deltaTime * 1000.0f;
            // float fps = 1.0f / deltaTime;
            // string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            // GUI.Label(rect, text, style);

            if (deltaTimes.Count < BUFFER_SIZE)
                deltaTimes.Add(deltaTime);
            else
            {
                deltaTimes.RemoveAt(0);
                deltaTimes.Add(deltaTime);
            }
            float avg = deltaTimes.Average();
            float msec = avg * 1000.0f;
            float fps = 1.0f / avg;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            GUI.Label(rect, text, style);

        }
	} 
}