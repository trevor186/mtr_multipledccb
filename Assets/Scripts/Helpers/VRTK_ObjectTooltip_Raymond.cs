﻿// Object Tooltip|Prefabs|0060
namespace VRTK
{
    using UnityEngine;

    public class VRTK_ObjectTooltip_Raymond : MonoBehaviour
    {
        // public VRTK_DestinationMarker pointer;
        public GameObject hoverObject;

        private void OnEnable()
        {
            hoverObject.SetActive(false);
            if(hoverObject.GetComponent<Animator>() != null){
                hoverObject.GetComponent<Animator>().SetTrigger("FP");
            }
        }

        // protected void OnEnable()
        // {
        //     pointer = (pointer == null ? GetComponent<VRTK_DestinationMarker>() : pointer);

        //     if (pointer != null)
        //     {
        //         pointer.DestinationMarkerEnter += DestinationMarkerEnter;
        //         pointer.DestinationMarkerExit += DestinationMarkerExit;
        //     }
        //     else
        //     {
        //         VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTK_ObjectTooltip_Raymond", "VRTK_DestinationMarker", "the Controller Alias"));
        //     }
        // }

        // protected void OnDisable()
        // {
        //     if (pointer != null)
        //     {
        //         pointer.DestinationMarkerEnter -= DestinationMarkerEnter;
        //         pointer.DestinationMarkerExit -= DestinationMarkerExit;
        //     }
        // }

        // protected void DestinationMarkerEnter(object sender, DestinationMarkerEventArgs e)
        // {
        //     if (e.target != null)
        //     {
        //         VRTK_ObjectTooltip_Raymond optionTile = e.target.GetComponent<VRTK_ObjectTooltip_Raymond>();
        //         if (optionTile != null)
        //         {
        //             optionTile.hoverObject.SetActive(true);
        //         }
        //     }
        // }

        // protected void DestinationMarkerExit(object sender, DestinationMarkerEventArgs e)
        // {
        //     if (e.target != null)
        //     {
        //         VRTK_ObjectTooltip_Raymond optionTile = e.target.GetComponent<VRTK_ObjectTooltip_Raymond>();
        //         if (optionTile != null)
        //         {
        //             optionTile.hoverObject.SetActive(false);
        //         }
        //     }
        // }
    }
}