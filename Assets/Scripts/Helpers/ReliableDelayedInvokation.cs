using UnityEngine;
using System.Collections;

public class ReliableDelayedInvokation : MonoBehaviour {
	public float countdown = float.MaxValue;
	public System.Action action;



	public static ReliableDelayedInvokation AddInvokation(GameObject parent , System.Action action , float delayInSecond){
		ReliableDelayedInvokation inv = parent.AddComponent<ReliableDelayedInvokation>();
		inv.action = action;
		inv.countdown = delayInSecond;
		return inv;
	}



	public static void CancelInvokation(ReliableDelayedInvokation inv){
		if (inv != null){
			inv.Cancel();
		}
	}

	public static void CancelInvokations(GameObject obj){
		ReliableDelayedInvokation[] invs = obj.GetComponents<ReliableDelayedInvokation>();
		for(int i = 0 ; i < invs.Length ; i++){
			invs[i].Cancel();
		}
	}

	public void Update(){
		countdown -= Time.deltaTime;
		if (countdown <= 0){
			if (action != null) action();
			countdown = float.MaxValue;
			Destroy(this);
		}
	}

	public void Cancel(){
		action = null;
		Destroy(this);
	}


}