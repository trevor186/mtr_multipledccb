﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour
{
    // public LineRenderer line;
    float laserWidth = 0.005f;
    void Update ()
    {
        LineRenderer lineRenderer = this.GetComponent<LineRenderer>();

        if (lineRenderer != null)
        {
            lineRenderer.useWorldSpace = false;
            lineRenderer.startWidth = laserWidth;
            lineRenderer.endWidth =laserWidth;
            lineRenderer.positionCount = 2;
            RaycastHit hit;
            if(Physics.Raycast(transform.position,transform.forward, out hit))
            {
                lineRenderer.SetPosition(1,new Vector3(0,0,hit.distance));
            }
            else
            {
                lineRenderer.SetPosition(1,new Vector3(0,0,5000));
            }
        }
    }
}
