﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TNet;

public class TNetPlayer : TNBehaviour
{
    private int _playerID = -1;
    public int GetPlayerID()
    {
        return _playerID;
    }


    public AudioListener audioListener;
    public System.Collections.Generic.List<GameObject> headObjects;
    public StudentController localStudentController;
    // public PlayerController playerController;

    private const float playerHeight = 1.65f;

    Transform rootTrans;
    public Transform headTrans;
    public Transform leftHandTrans;
    public Transform rightHandTrans;
    Vector3 [] playerTransReceived = new Vector3[6];

    public GameObject laserObject;
	
    public GameObject toolsBox;
    public BriefDocController briefDoc;
    public BriefDocController briefDocLeft;
    public BriefDocController briefDocRight;
    public ReportPanel reportPanel;
    public PhoneController phone;
    public System.Collections.Generic.List<GameObject> testerPens = new System.Collections.Generic.List<GameObject>();
    public System.Collections.Generic.List<GameObject> Gloves = new System.Collections.Generic.List<GameObject>();
    public System.Collections.Generic.List<GameObject> EarthingClips = new System.Collections.Generic.List<GameObject>();
    private bool _glovesBool;
    private bool _penBool;
    // private bool _glovesBool;
    // private bool _penBool;
    // public GameObject ManoeuvringHandle;
    // public GameObject DangerNotice;
    // public GameObject CautionNotice;
    GameObject rightHandObject;
    public GameObject [] rightHandObjectList;
    bool _canOpenToolBox;
    public bool canOpenToolBox
    {
        get
        {
            return _canOpenToolBox;
        }
    }
    public BoxCollider [] receiveSafetyDocumentBoxes;
    public GameObject spotLight;

    bool _canPointerInteract;
    public bool canPointerInteract
    {
        get
        {
            return _canPointerInteract;
        }
    }

    public SkinnedMeshRenderer playerTop;
    public Material player2TopMaterial;

	protected override void Awake ()
	{
		base.Awake();
		// mTrans = transform;
		// mTarget = mTrans.position;
		// mTargetRotation = mTrans.rotation.eulerAngles;
        rootTrans = transform;
        audioListener.enabled = tno.isMine;
	}

	void Start()
	{
        if (tno.isMine)
        {
            // if (playerHead != null)
            //     playerHead.SetActive(false);

            foreach (GameObject go in headObjects)
            {
                //go.SetActive(false);
                go.layer = 1;       //TransparentFX
            }

            localStudentController.SetTrackTarget(VRTK_ExtendInput.instance.headTrackedObect.transform, VRTK_ExtendInput.instance.rightTrackedObect.transform, VRTK_ExtendInput.instance.leftTrackedObect.transform);

            ReportManager.instance.AddReportPanel(reportPanel);
        }

        // if(_playerID == 1)
        if (GlobalConfig.instance.GetNetworkID() == _playerID)
        {
            // RFCMessenger.instance.player = this;
            ScenarioEventManager.instance.playerController = this;
        }

        if (_playerID == 2)
        {
            playerTop.material = player2TopMaterial;
        }
        
        // if(_playerID == 1)
        // {
        //     ObjectHolderManager.instance.DCTester.GetComponent<DCTester>().redCableStart.GetComponent<CableComponent>().SetCableEndPoint(testerPens[0].transform);
        //     ObjectHolderManager.instance.DCTester.GetComponent<DCTester>().blackCableStart.GetComponent<CableComponent>().SetCableEndPoint(testerPens[1].transform);
        // }

        Debug.Log("_playerID" + _playerID);

        laserObject.SetActive(!tno.isMine);

        Init();
	}

    void Init()
    {
        _canOpenToolBox = true;

        briefDoc = briefDocLeft;
        briefDocLeft.CloseDoc();
        briefDocRight.CloseDoc();

        reportPanel.Init();
        phone.ClosePhone();
        EnableTesterPens(false);
        EnableGlovesWithValue(false);
        InitEarthingCable();
        spotLight.SetActive(false);
        _canPointerInteract = _playerID == 1;        //only player 1 can interact at start

        RemoveRightHandObject();

        foreach (var item in receiveSafetyDocumentBoxes)
        {
            item.enabled = false;
        }
    }

    public void InitEarthingCable(){
        // EarthingClips[1].GetComponentInChildren<CableComponent>().SetCableEndPoint(EarthingClips[0].transform.Find("to_wall_cable"));
        // EarthingClips[1].GetComponentInChildren<CableComponent>().ReinitCable();
    }
	public void SendAll(string rfcName)
	{
		tno.Send(rfcName, Target.All);
	}    
	public void SendAll(string rfcName, object obj1)
	{
		tno.Send(rfcName, Target.All, obj1);
	}


    public void RemoveRightHandObjectRFC()
    {
        tno.Send("RemoveRightHandObject", Target.All);
    }


    [RFC] void RemoveRightHandObject()
    {
        if (rightHandObject != null)
        {
            rightHandObject.SetActive(false);
            rightHandObject = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tno.isMine)
        {
            Vector3 [] playerTrans = new Vector3[6];
            playerTrans[0] = VRTK_ExtendInput.instance.headTrackedObect.transform.position;
            playerTrans[1] = VRTK_ExtendInput.instance.headTrackedObect.transform.rotation.eulerAngles;
            playerTrans[2] = VRTK_ExtendInput.instance.leftTrackedObect.transform.position;
            playerTrans[3] = VRTK_ExtendInput.instance.leftTrackedObect.transform.rotation.eulerAngles;
            playerTrans[4] = VRTK_ExtendInput.instance.rightTrackedObect.transform.position;
            playerTrans[5] = VRTK_ExtendInput.instance.rightTrackedObect.transform.rotation.eulerAngles;
            tno.SendQuickly("PlayerTransforms", Target.Others, playerTrans);

            playerTransReceived = playerTrans;
        }

        rootTrans.position = Vector3.Lerp(rootTrans.position, playerTransReceived[0] - new Vector3(0,playerHeight,0), Time.deltaTime * 20f);
        rootTrans.rotation = Quaternion.Euler(0, playerTransReceived[1].y, 0);

        headTrans.localPosition = new Vector3(0,playerHeight,0);
        headTrans.localRotation = Quaternion.Euler(playerTransReceived[1].x, 0, playerTransReceived[1].z);

        leftHandTrans.position = Vector3.Lerp(leftHandTrans.position, playerTransReceived[2], Time.deltaTime * 20f);
        leftHandTrans.rotation = Quaternion.Euler(playerTransReceived[3]);
        
        rightHandTrans.position = Vector3.Lerp(rightHandTrans.position, playerTransReceived[4], Time.deltaTime * 20f);
        rightHandTrans.rotation = Quaternion.Euler(playerTransReceived[5]);
        
//         toolsBox.SetActive(ScenarioEventManager.instance.isGameStarted);
// #if UNITY_EDITOR
//         toolsBox.SetActive(true);
// #endif
    }

	/// <summary>
	/// Save the target position.
	/// </summary>

	// [RFC(3)] void MoveObject (Vector3 pos) { mTarget = pos; }
	// [RFC(4)] void RotateObject (Vector3 rot) { mTargetRotation = rot; }


    public bool IsPlayerOne(){
		return   GetPlayerID() == 1 ;
	}
    public void ActiveTesterPens(){
        // _penBool = !_penBool;

        bool currentStatus = testerPens[0].activeInHierarchy;
        EnableTesterPens(!currentStatus);
    }

    public void EnableTesterPens(bool value){
        foreach(GameObject obj in testerPens){
            if(obj != null)
            obj.SetActive(value);
        }
    }

    public void ChangeGloves(){
        Debug.Log("???");
        // Debug.Log("Chenge " + _glovesBool);
        // _glovesBool = !_glovesBool;
        EnableGloves();
    }

    public void EnableGloves(){
        Gloves[0].SetActive(!Gloves[0].activeSelf);
        Gloves[1].SetActive(!Gloves[1].activeSelf);

        ScenarioEventManager.instance.SetPlayer1GloveBool(Gloves[1].activeInHierarchy);
    }
    public void EnableGlovesWithValue(bool value){

        Gloves[0].SetActive(!value);
        Gloves[1].SetActive(value);
        ScenarioEventManager.instance.SetPlayer1GloveBool(Gloves[1].activeInHierarchy);
    }
    public void ActiveEarthingSet(bool value){
        EarthingClips[0].SetActive(value);
        EarthingClips[1].SetActive(value);
    }

    public void PlayerConnectEarthingCableToWall(){
        EarthingClips[0].SetActive(false);
        // EarthingClips[1].GetComponentInChildren<CableComponent>().SetCableEndPoint(ScenarioEventManager.instance.GetCurrentEarthingWallTransfom());
        // EarthingClips[1].GetComponentInChildren<CableComponent>().ReinitCable();
    }
    public void PlayerConnectEarthingCableToWallRFC(){
        tno.Send("ConnetEarthingWall",Target.All);
    }
    [RFC] void PlayerTransforms(Vector3 [] playerTrans)
    {
        playerTransReceived = playerTrans;
    }

    public void ToolsBoxSetActiveRFC(bool value){
        // toolsBox.SetActive(value);
        tno.Send("ToolsBoxSetActive",Target.All,value);
    }
    
    public void OpenDocRFC(){
        // briefDoc.OpenDoc();
        tno.Send("OpenDoc",Target.All);
    }
    public void CloseDocRFC(){
        tno.Send("CloseDoc",Target.All);
    }

    public void GiveDocRFC(string name){
        tno.Send("GiveDoc",Target.All,name);
    }

    [RFC] void GiveDoc(string name){
        if(this.GetPlayerID ()== 1){
            tno.Send("CloseDoc",Target.All);
            RFCMessenger.instance.NextStepRFC("Report");
        }
        if (this.GetPlayerID ()== 2){
            tno.Send("SafetyDocumentReceived",Target.All,name);
        }
    }
    public void NextPageRFC(){
        // briefDoc.NextPage();
        tno.Send("NextPage",Target.All);
    }
    // public void SwitchPageRFC(int value){
    //     tno.Send("SwitchPage",Target.Others,value);
    // }
    public void OpenReportRFC()
    {
        // reportPanel.gameObject.SetActive(true);
        ToolsBoxSetActiveRFC(false);
        tno.Send("OpenReport",Target.All);        
    }
    public void CloseReportRFC()
    {
        // reportPanel.gameObject.SetActive(false);
        tno.Send("CloseReport",Target.All);
    }
    public void UsePhoneRFC(int value){
        tno.Send("UsePhone",Target.All,value);
    }
    // public void ClosePhoneRFC(){
    //     tno.Send("ClosePhone",Target.Others);
    // }
    public void PlayerActiveTesterPensRFC(){
        tno.Send("PlayerActiveTesterPens",Target.All);
    }
    public void PlayerChangeGlovesRFC(){
        tno.Send("PlayerChangeGloves",Target.All);
    }
    public void PlayerChangeGlovesRFC( bool value){
        tno.Send("PlayerChangeGlovesWithValue",Target.All,value);
    }
    public void PlayerActiveEarthingSetRFC(bool value){
        tno.Send("PlayerActiveEarthingSet",Target.All,value);
    }
    [RFC] void ToolsBoxSetActive(bool value){
        toolsBox.SetActive(value);
    }    
    [RFC] void OpenDoc(){
        briefDoc.OpenDoc();
    }
    [RFC] void CloseDoc(){
        briefDoc.CloseDoc();
    }
    [RFC] void NextPage(){
        briefDoc.NextPage();
    }   
    // [RFC] void SwitchPage(int value){
    //     briefDoc.SwitchPage(value);
    // }   
    [RFC] void OpenReport()
    {
        reportPanel.gameObject.SetActive(true);
    }    
    [RFC] void CloseReport()
    {
        reportPanel.gameObject.SetActive(false);
    }    
    [RFC] void UsePhone(int value){
        phone.CallPhone(value);
    }
    // [RFC] void ClosePhone(){
    //     phone.ClosePhone();
    // }
    [RFC] void PlayerActiveTesterPens(){
        ActiveTesterPens();
    }
    [RFC] void PlayerChangeGloves(){
        ChangeGloves();
    }
    [RFC] void PlayerChangeGlovesWithValue(bool value){
        EnableGlovesWithValue(value);
    }
    [RFC] void PlayerActiveEarthingSet(bool value){
        ActiveEarthingSet(value);
    }
    public void SetPlayerID(int value)
    {
        _playerID = value;
    }
    public void PlayerInitRFC()
    {
        tno.Send("PlayerInit", Target.All);
    }
    [RFC] void PlayerInit()
    {
        Init();
    }
    public void ShowCautionNoticeRFC()
    {
        tno.Send("ShowRightHandObject", Target.All, "CautionNotice");        
    }
    public void ShowDangerNoticeRFC()
    {
        tno.Send("ShowRightHandObject", Target.All, "DangerNotice");        
    }
    public void ShowRotatorHandleRFC()
    {
        tno.Send("ShowRightHandObject", Target.All, "RotatorHandle");
    }


    [RFC] void ShowRightHandObject(string target)
    {
        int idx = -1;
        switch (target)
        {
            case "CautionNotice": idx = 0; break;
            case "DangerNotice":  idx = 1; break;
            case "RotatorHandle": idx = 2; break;
        }

        if (idx != -1 && !phone.isPhoneUsing)
        {
            if (rightHandObject == rightHandObjectList[idx])
            {
                RemoveRightHandObject();
            }
            else
            {
                RemoveRightHandObject();
                rightHandObjectList[idx].SetActive(true);
                rightHandObject = rightHandObjectList[idx];
            }
        }
    }

    [RFC] void ShowSafetyDocument()
    {
        _canOpenToolBox = false;
        briefDoc.SetFinishList();
        briefDoc.OpenDoc();
    }

    [RFC] void PrepareReceiveSafetyDocument()
    {
        foreach (var item in receiveSafetyDocumentBoxes)
        {
            item.enabled = true;
        }
    }

    [RFC] void SafetyDocumentReceived(string name)
    {
        Debug.Log("Safe doc name? " + name );
        // briefDoc = ScenarioEventManager.instance.safetyDocumentClone.GetComponent<BriefDocController>();
        if (name.Contains("Left"))
            briefDoc = briefDocLeft;
        else            
            briefDoc = briefDocRight;

        briefDoc.SetFinishList();
        briefDoc.OpenDoc();

        foreach (var item in receiveSafetyDocumentBoxes)
        {
            item.enabled = false;
        }
    }

    [RFC] void SpotLightEnable(bool value)
    {
        spotLight.SetActive(value);
    }


    [RFC] void ActivateMeggerTester(bool value)
    {
        ScenarioEventManager.instance.ActivateMeggerTester(value);

    }
    [RFC] void ConnetEarthingWall(){
        PlayerConnectEarthingCableToWall();
    }
    public void SnapObjectCreateRFC(string path, Vector3 pos, Quaternion rot)
    {
        Debug.Log("SnapObjectCreateRFC: " + path);
        tno.Send("SnapObjectCreate", Target.Others, path, pos, rot);
    }

    [RFC] void SnapObjectCreate(string path, Vector3 pos, Quaternion rot)
	{
		Debug.Log("SnapObjectCreate");

        GameObject clone;
        if (ScenarioEventManager.instance.GetSwitchRoom().gameObject.activeInHierarchy)
		    clone = GameObject.Instantiate(Resources.Load<GameObject>(path), pos, rot, ScenarioEventManager.instance.GetSwitchRoom().transform);
        else
            clone = GameObject.Instantiate(Resources.Load<GameObject>(path), pos, rot, ScenarioEventManager.instance.GetTrackside().transform);
        // clone.GetComponent<BoxCollider>().enabled = false;
    }

    // [RFC] void SetBriefDoc(GameObject )

    public void PlayerTwoSetPointerInteract(bool value)
    {
        if (GetPlayerID() == 2)
            _canPointerInteract = value;
    }
    public void TesterPenInitRFC(){
        tno.Send("TesterPenInit",Target.All);
    }
    [RFC] void TesterPenInit(){
        testerPens.Add(ScenarioEventManager.instance.GetTesterBlackClip());
    }
    public void MeggertesterInitRFC(){
        tno.Send("MeggerTesterInit",Target.All);
    }
    [RFC] void MeggerTesterInit(){
        ScenarioEventManager.instance.MeggerTesterInit(testerPens);
    }

    public void BriefDocInitRFC(bool obj1, int obj2){
        tno.Send("BriefDocInit",Target.All,obj1,obj2);
    }
    [RFC] void BriefDocInit(bool obj1,int obj2){
        briefDoc.Init(obj1,obj2);
        briefDocLeft.Init(obj1,obj2);
        briefDocRight.Init(obj1,obj2);
    }
    // public void CheckPhoneRFC(){
    //     tno.Send("CheckPhone",Target.All);
    // }
    // [RFC] void CheckPhone(){
    //     PlayerOneCheckPhone();
    // }
    // public bool PlayerOneCheckPhone(){
    //     if(AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() ==1){
    //         return AMVRNetworkManager.instance.GetCurrentPlayer().phone.basePhoneObject.activeSelf;
    //     }
    //     else return false;
    // }
    public void SwapPlayerID(){
        if(_playerID ==1){
            SetPlayerID(2);
        }else if(_playerID == 2){
            SetPlayerID(1);
        }
            
    }
}