﻿using UnityEngine;
using TNet;

public class SyncObject : TNBehaviour
{
	Transform rootTrans;
    Vector3 [] transReceived = new Vector3[2];
    public bool needSync = true;

	protected override void Awake ()
	{
		base.Awake();

        rootTrans = transform;
	}

	void Update ()
	{
        if (needSync)
        {
            if (tno.isMine)
            {
                Vector3 [] objectTrans = new Vector3[2];
                objectTrans[0] = rootTrans.position;
                objectTrans[1] = rootTrans.eulerAngles;
                tno.SendQuickly("SyncObjectTransfrom", Target.Others, objectTrans);

                transReceived = objectTrans;
            }
            else
            {
                rootTrans.position = Vector3.Lerp(rootTrans.position, transReceived[0], Time.deltaTime * 20f);
                rootTrans.rotation = Quaternion.Euler(transReceived[1]);
            }
        }
	}

	/// <summary>
	/// Save the target position.
	/// </summary>

	[RFC] void SyncObjectTransfrom (Vector3 [] objectTrans) 
    {
        transReceived = objectTrans;
    }

    public void SetNeedSync(bool value)
    {
        needSync = value;
    }


}
