﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using UnityEngine.SceneManagement;
public class AMVRNetworkManager : TNBehaviour
{
    #region Singleton
    public static AMVRNetworkManager instance = null;
    protected override void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(gameObject);  
    }
    #endregion

    bool useTNet = true;
	public bool useServerAddressIfEditor = true;
	public string serverAddress = "127.0.0.1";
	// Raymond Server ID : 192.168.1.192
	public int serverPort = 5127;
	// public string firstLevel = "Example 1";
	private int _channelID = 1;
	public int channelID;
	// {
	// 	get
	// 	{
	// 		return _channelID;
	// 	}
	// }

	public bool persistent = false;
	public string disconnectLevel;
	public bool allowUDP = true;
	public bool connectOnStart = true;    
	public bool isConnected
	{
		get
		{
			return TNManager.isConnected;
		}
	}

	public bool TrevorMain = false;
	public bool RaymondMain = false;
	public bool WaitingRoom = false;
	public bool workingScene = false;
	public string workingSceneName;
	public string workingSceneId;
    public string playerPrefabName = "Prefabs/NetworkPlayer";
	private bool isWaitingRoomScene;
	public bool soloTest;
	public GameObject soloTestPrefab;
	private MTRPlayer currentMTRPlayer= null;
	public MTRPlayer GetCurrentMTRPlayer()
    {
        return currentMTRPlayer;
    }
	public void SetCurrentMTRPlayer(MTRPlayer p)
	{
		currentMTRPlayer = p;
	}
    private TNetPlayer currentPlayer = null;
    public TNetPlayer GetCurrentPlayer()
    {
        return currentPlayer;
    }
	public void SetCurrentPlayer(TNetPlayer p)
	{
		currentPlayer = p;
	}	


	void OnEnable ()
	{
		TNManager.onConnect += OnConnect;
		TNManager.onDisconnect += OnDisconnect;
		TNManager.onPlayerJoin += OnPlayerJoin;
		TNManager.onPlayerLeave += OnPlayerLeave;
		TNManager.onJoinChannel += OnJoinChannel;
	}

	void OnDisable ()
	{
		TNManager.onConnect -= OnConnect;
		TNManager.onDisconnect -= OnDisconnect;
		TNManager.onPlayerJoin -= OnPlayerJoin;
		TNManager.onPlayerLeave -= OnPlayerLeave;
		TNManager.onJoinChannel -= OnJoinChannel;
	}/// <summary>
	/// Connect to the server if requested.
	/// </summary>

	void Start () {
	    if (connectOnStart) Connect(); 
		// if(soloTest) 
		//  	SceneManager.LoadScene("MTR_WaitingRoom");
	}

	/// <summary>
	/// Connect to the server.
	/// </summary>

	void Update()
	{
		isWaitingRoomScene = SceneManager.GetActiveScene().name  =="MTR_WaitingRoom";
		if(Input.GetKeyDown(KeyCode.Alpha1) && isWaitingRoomScene  ){
			MultiDCCBSceneRFCMessenger.instance.EnterSceneRFC(2,"MTR_MultiDCCB_Scene1",1,"Scene1A");
		}
		if(Input.GetKeyDown(KeyCode.Alpha2) && isWaitingRoomScene  ){
			SetSceneParam("Scene1B");
			// MultiDCCBSceneHandlingManager.instance.SetCurrentScene(1);
			MultiDCCBSceneRFCMessenger.instance.EnterSceneRFC(3,"MTR_MultiDCCB_Scene1",1,"Scene1B");
		}
		if(Input.GetKeyDown(KeyCode.Alpha3)&& isWaitingRoomScene){
			SetSceneParam("Scene2A");
			// MultiDCCBSceneHandlingManager.instance.SetCurrentScene(2);

			MultiDCCBSceneRFCMessenger.instance.EnterSceneRFC(4,"MTR_MultiDCCB_Scene2A",2,"Scene2A");
		}		
		if(Input.GetKeyDown(KeyCode.Alpha4)&& isWaitingRoomScene){
			SetSceneParam("Scene2B");
			// MultiDCCBSceneHandlingManager.instance.SetCurrentScene(2);
			MultiDCCBSceneRFCMessenger.instance.EnterSceneRFC(5,"MTR_MultiDCCB_Scene2B",2,"Scene2B");
		}	
		if(Input.GetKeyDown(KeyCode.Alpha5)&& isWaitingRoomScene){
			// EnterScene(3,"MTR_MultiDCCB_Scene3");
		}
		if(Input.GetKeyDown(KeyCode.Alpha6)&& isWaitingRoomScene){
			// EnterScene(3,"MTR_MultiDCCB_Scene3");
		}			
		if(Input.GetKeyDown(KeyCode.Alpha7)&& isWaitingRoomScene){
			// EnterScene(5,"MTR_MultiDCCB_Scene4");
		}		
		if(Input.GetKeyDown(KeyCode.Alpha8)&& isWaitingRoomScene){
			// EnterScene(5,"MTR_MultiDCCB_Scene4");
		}
		if(Input.GetKeyDown(KeyCode.R)){
			// EnterScene(1,"MTR_WaitingRoom");
			MultiDCCBSceneRFCMessenger.instance.EnterSceneRFC(1,"MTR_WaitingRoom",0,"");
		}
		if(Input.GetKeyDown(KeyCode.Alpha0)){
			TNManager.GetChannelList(delegate{
				Debug.Log("Checking");
			});
		}
	}

    public string GetServerIP()
    {
        return GlobalConfig.instance.data.serverIP;        
    }

	public void Connect ()
	{
		// We don't want mobile devices to dim their screen and go to sleep while the app is running
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

        if (GetServerIP() != "")
            serverAddress = GetServerIP();
		
		// Connect to the remote server

		if (!TNManager.isConnected)
		{
			// if (GlobalConfig.isServer)
			// {
			// 	TNManager.player.name = "Server";
			// }
						
			Debug.Log("Connecting to " + serverAddress + ":" + serverPort);
			TNManager.Connect(serverAddress, serverPort);
		}
		else
		{
			Debug.Log("It is already connected...");
		}
	}

	/// <summary>
	/// On success -- join a channel.
	/// </summary>

	void OnConnect (bool result, string message)
	{
		Debug.Log("?????????????????????????????????? result " + result + "Wt mesg> " + message);
		if (result)
		{
			Debug.Log("Connect ok");
			// Make it possible to use UDP using a random port
			if (allowUDP) TNManager.StartUDP(Random.Range(10000, 50000));
			// TNManager.JoinChannel(channelID, persistent);

			// if(soloTest){
			// 	TNManager.JoinChannel(0,workingSceneName,persistent);
			// 	Vector3 pos = Vector3.zero;
			// 	Quaternion rot = Quaternion.identity;
			// 	SoloTestCreatePlayer(pos,rot,1);
			// }else{
			// if(!soloTest){
				if(WaitingRoom){
					Debug.Log("??????????????????????");
					TNManager.JoinChannel(channelID, "MTR_WaitingRoom", persistent);
				}else if (RaymondMain)
					TNManager.JoinChannel(channelID, "MTR_Main_Raymond", persistent);
				else if (TrevorMain){
					TNManager.JoinChannel(channelID, "MTR_MultiDCCB_Trevor", persistent);
				}else if(workingScene){
					Debug.Log("Go here?");
					TNManager.JoinChannel(0,workingSceneName,persistent);
				}else
					TNManager.JoinChannel(channelID, "MTR_DCCB_Main", persistent);
			// }


			// if (GlobalConfig.isServer)
			// {
			// 	if (!TNManager.IsHosting(channelID))
			// 	{
			// 		// Debug.Log("Host id:" + TNManager.GetHost(channelID).id);

			// 		Debug.Log("I am not hosting, change the host to me now.1..");
			// 		TNManager.SetHost(channelID, TNManager.player);
			// 		Debug.Log("Am I hosting now ??" + TNManager.IsHosting(channelID));
			// 		Debug.Log("Host id:" + TNManager.GetHost(channelID).id);
			// 	}
			// }
		}
		else Debug.LogError(message);
	}

	void OnJoinChannel(int channelID, bool success, string message)
	{
        Debug.Log("OnJoinChannel:" + channelID + "," + success + "," + message + ".");

		if (success)
		{
            Vector3 pos = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            TNManager.Instantiate(channelID, "MTRPlayerCreate", playerPrefabName, false, pos, rot, GlobalConfig.instance.GetNetworkID());
            
			Debug.Log("Get networkID??? " + GlobalConfig.instance.GetNetworkID() );

			if(GlobalConfig.instance.GetNetworkID() ==1){
				// RFCMessenger.instance.getpl()
			}
			
			// if(TNManager.IsHosting(channelID)){
			// 	// Random.Range(0,2);
			// 	RFCMessenger.instance.InitSetupRFC(0);
			// }
			// if (GlobalConfig.isServer)
			// {
			// 	if (!TNManager.IsHosting(channelID))
			// 	{
			// 		Debug.Log("Host id:" + TNManager.GetHost(channelID).id);

			// 		Debug.Log("I am not hosting, change the host to me now...");
			// 		// TNManager.SetHost(channelID, TNManager.player);
			// 		TNManager.SetHost(channelID,TNManager.GetPlayer(TNManager.playerID));
			// 		Debug.Log("Am I hosting now ??" + TNManager.IsHosting(channelID));
			// 		Debug.Log("Host id:" + TNManager.GetHost(channelID).id);
			// 	}
			// }
		}

	}
	public GameObject SoloTestCreatePlayer(Vector3 pos, Quaternion rot, int playerID){
		GameObject go = GameObject.Instantiate(soloTestPrefab, pos, rot);
		go.GetComponent<TNetPlayer>().SetPlayerID(1);

		return go;
	}

    [RCC]
	static GameObject NetworkPlayerCreate (GameObject prefab, Vector3 pos, Quaternion rot, int playerID)
	{
		Debug.Log("NetworkPlayerCreate");
		// Instantiate the prefab
		GameObject go = GameObject.Instantiate(prefab, pos, rot);
		go.GetComponent<TNetPlayer>().SetPlayerID(playerID);
		Debug.Log("playerID ??" + playerID);
		if (GlobalConfig.instance.GetNetworkID() == playerID)
			AMVRNetworkManager.instance.SetCurrentPlayer(go.GetComponent<TNetPlayer>());

		// Set the position and rotation based on the passed values
		// Transform t = go.transform;
		// t.position = pos;
		// t.rotation = rot;

		// Set the renderer's color as well
		// go.GetComponentInChildren<Renderer>().material.color = c;
		// ServerController.instance.clientVRPlayer = go;

		return go;
	}    
	[RCC]
	static GameObject MTRPlayerCreate (GameObject prefab, Vector3 pos, Quaternion rot, int playerID)
	{
		Debug.Log("MTRplayerCreate");
		// Instantiate the prefab
		GameObject go = GameObject.Instantiate(prefab, pos, rot);
		go.GetComponent<MTRPlayer>().SetPlayerID(playerID);
		Debug.Log("playerID ??" + playerID);
		if (GlobalConfig.instance.GetNetworkID() == playerID){
			AMVRNetworkManager.instance.SetCurrentMTRPlayer(go.GetComponent<MTRPlayer>());
		}
		// Set the position and rotation based on the passed values
		// Transform t = go.transform;
		// t.position = pos;
		// t.rotation = rot;

		// Set the renderer's color as well
		// go.GetComponentInChildren<Renderer>().material.color = c;
		// ServerController.instance.clientVRPlayer = go;

		return go;
	}    


	void Disconnect()
	{
		TNManager.Disconnect();
	}

	/// <summary>
	/// Disconnected? Go back to the menu.
	/// </summary>

	void OnDisconnect ()
	{
// #if UNITY_4_6 || UNITY_4_7 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2
// 		if (!string.IsNullOrEmpty(disconnectLevel) && Application.loadedLevelName != disconnectLevel)
// 			Application.LoadLevel(disconnectLevel);
// #else
// 		if (!string.IsNullOrEmpty(disconnectLevel) &&
// 			UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != disconnectLevel)
// 			UnityEngine.SceneManagement.SceneManager.LoadScene(disconnectLevel);
// #endif
        Debug.Log("OnDisconnect");
		Connect();
	} 

	void OnPlayerJoin(int ch, Player p)
	{
		Debug.Log("OnPlayerJoin");
	}

	void OnPlayerLeave(int ch, Player p)
	{
		// if(GlobalConfig.instance.GetNetworkID()==2){
		// 	ScenarioEventManager.instance.BackToWaitingRoom();
		// }
		Debug.Log("OnPlayerLeave");
	}

	public void QuitApp()
	{
		Application.Quit();
	}

	private void OnApplicationQuit()
	{
		Debug.Log("Get player count" + GetPlayerCount());
		if(GetPlayerCount() ==1)
			RemoveAllSavedRFCs();

		if (!Application.isEditor)
		{
			System.Diagnostics.Process.GetCurrentProcess().Kill();
		}
	}

	public int GetPlayerCount()
	{
		return TNManager.GetPlayers(channelID).Count + 1;			//the list doesn't include the current player
	}
	// public void EnterSceneRFC(int sceneIndex,string sceneName){
	// 	// Debug.Log("?index "  + sceneIndex + " sceneame???  " + sceneName);
	// 	// channelID = sceneIndex;
	// 	// TNManager.JoinChannel(sceneIndex, sceneName, false, 255, null);
	// 	Debug.Log("Test?");
	// 	tno.Send("EnterScene",Target.All,sceneIndex,sceneName);
	// }
	public void SetSceneParam(string param){
		Debug.Log("Save? + " + param);
		TNManager.SetPlayerData("SceneParameter",param);
	}

	public bool IsSoloPlayMode(){
		return soloTest;
	}
}
