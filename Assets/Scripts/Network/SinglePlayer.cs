﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinglePlayer : MonoBehaviour
{
    public AudioListener audioListener;
    public System.Collections.Generic.List<GameObject> headObjects;
    public StudentController localStudentController;
    private const float playerHeight = 1.65f;
    Transform rootTrans;
    public Transform headTrans;
    public Transform leftHandTrans;
    public Transform rightHandTrans;
    Vector3 [] playerTransReceived = new Vector3[6];
    public GameObject laserObject;
    public GameObject toolsBox;
    public PhoneController phone;

    // Start is called before the first frame update
	void Awake ()
	{

        rootTrans = transform;
	}
void Start()
	{
        // if (playerHead != null)
        //     playerHead.SetActive(false);

        foreach (GameObject go in headObjects)
        {
            //go.SetActive(false);
            go.layer = 1;       //TransparentFX
        }
        localStudentController.SetTrackTarget(VRTK_ExtendInput.instance.headTrackedObect.transform, VRTK_ExtendInput.instance.rightTrackedObect.transform, VRTK_ExtendInput.instance.leftTrackedObect.transform);

        Init();
	}

    void Init()
    {
        phone.ClosePhone();
    }
    void Update()
    {

        Vector3 [] playerTrans = new Vector3[6];
        playerTrans[0] = VRTK_ExtendInput.instance.headTrackedObect.transform.position;
        playerTrans[1] = VRTK_ExtendInput.instance.headTrackedObect.transform.rotation.eulerAngles;
        playerTrans[2] = VRTK_ExtendInput.instance.leftTrackedObect.transform.position;
        playerTrans[3] = VRTK_ExtendInput.instance.leftTrackedObect.transform.rotation.eulerAngles;
        playerTrans[4] = VRTK_ExtendInput.instance.rightTrackedObect.transform.position;
        playerTrans[5] = VRTK_ExtendInput.instance.rightTrackedObect.transform.rotation.eulerAngles;

        playerTransReceived = playerTrans;


        rootTrans.position = Vector3.Lerp(rootTrans.position, playerTransReceived[0] - new Vector3(0,playerHeight,0), Time.deltaTime * 20f);
        rootTrans.rotation = Quaternion.Euler(0, playerTransReceived[1].y, 0);

        headTrans.localPosition = new Vector3(0,playerHeight,0);
        headTrans.localRotation = Quaternion.Euler(playerTransReceived[1].x, 0, playerTransReceived[1].z);


        leftHandTrans.position = Vector3.Lerp(leftHandTrans.position, playerTransReceived[2], Time.deltaTime * 20f);
        leftHandTrans.rotation = Quaternion.Euler(playerTransReceived[3]);
        
        rightHandTrans.position = Vector3.Lerp(rightHandTrans.position, playerTransReceived[4], Time.deltaTime * 20f);
        rightHandTrans.rotation = Quaternion.Euler(playerTransReceived[5]);
        
//         toolsBox.SetActive(ScenarioEventManager.instance.isGameStarted);
// #if UNITY_EDITOR
//         toolsBox.SetActive(true);
// #endif
    }

}
