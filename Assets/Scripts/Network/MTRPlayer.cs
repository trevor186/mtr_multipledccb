using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TNet;
public class MTRPlayer : TNBehaviour
{
    [SerializeField]
    private int _playerID = -1;
    public int GetPlayerID()
    {
        return _playerID;
    }
    public AudioListener audioListener;
    public System.Collections.Generic.List<GameObject> headObjects;
    public StudentController localStudentController;
    private const float playerHeight = 1.65f;
    Transform rootTrans;
    public Transform headTrans;
    public Transform leftHandTrans;
    public Transform rightHandTrans;
    Vector3 [] playerTransReceived = new Vector3[6];
    public SkinnedMeshRenderer playerTop;
    public Material player2TopMaterial;
    public MTRPlayerController playerController;
    public EarthingTester earthingTester;

	protected override void Awake ()
	{
		base.Awake();
		// mTrans = transform;
		// mTarget = mTrans.position;
		// mTargetRotation = mTrans.rotation.eulerAngles;
        rootTrans = transform;
        audioListener.enabled = tno.isMine;
	}

    // Start is called before the first frame update
void Start()
	{
        if (tno.isMine)
        {
            // if (playerHead != null)
            //     playerHead.SetActive(false);

            foreach (GameObject go in headObjects)
            {
                //go.SetActive(false);
                go.layer = 1;       //TransparentFX
            }

            localStudentController.SetTrackTarget(VRTK_ExtendInput.instance.headTrackedObect.transform, VRTK_ExtendInput.instance.rightTrackedObect.transform, VRTK_ExtendInput.instance.leftTrackedObect.transform);

        }
        
        // if(_playerID == 1)
        if (_playerID == 1)
        {
            // RFCMessenger.instance.player = this;
            MultiDCCBSceneHandlingManager.instance.player1Controller = this;
            
        }

        if (_playerID == 2)
        {
            MultiDCCBSceneHandlingManager.instance.player2Controller = this;
        }
        
        // if(_playerID == 1)
        // {
        //     ObjectHolderManager.instance.DCTester.GetComponent<DCTester>().redCableStart.GetComponent<CableComponent>().SetCableEndPoint(testerPens[0].transform);
        //     ObjectHolderManager.instance.DCTester.GetComponent<DCTester>().blackCableStart.GetComponent<CableComponent>().SetCableEndPoint(testerPens[1].transform);
        // }

        Debug.Log("_playerID" + _playerID);


        Init();
	}
void Init()
    {

    }
    // Update is called once per frame
 void Update()
    {
        if (tno.isMine)
        {
            Vector3 [] playerTrans = new Vector3[6];
            playerTrans[0] = VRTK_ExtendInput.instance.headTrackedObect.transform.position;
            playerTrans[1] = VRTK_ExtendInput.instance.headTrackedObect.transform.rotation.eulerAngles;
            playerTrans[2] = VRTK_ExtendInput.instance.leftTrackedObect.transform.position;
            playerTrans[3] = VRTK_ExtendInput.instance.leftTrackedObect.transform.rotation.eulerAngles;
            playerTrans[4] = VRTK_ExtendInput.instance.rightTrackedObect.transform.position;
            playerTrans[5] = VRTK_ExtendInput.instance.rightTrackedObect.transform.rotation.eulerAngles;
            tno.SendQuickly("PlayerTransforms", Target.Others, playerTrans);

            playerTransReceived = playerTrans;
        }

        rootTrans.position = Vector3.Lerp(rootTrans.position, playerTransReceived[0] - new Vector3(0,playerHeight,0), Time.deltaTime * 20f);
        rootTrans.rotation = Quaternion.Euler(0, playerTransReceived[1].y, 0);

        headTrans.localPosition = new Vector3(0,playerHeight,0);
        headTrans.localRotation = Quaternion.Euler(playerTransReceived[1].x, 0, playerTransReceived[1].z);

        leftHandTrans.position = Vector3.Lerp(leftHandTrans.position, playerTransReceived[2], Time.deltaTime * 20f);
        leftHandTrans.rotation = Quaternion.Euler(playerTransReceived[3]);
        
        rightHandTrans.position = Vector3.Lerp(rightHandTrans.position, playerTransReceived[4], Time.deltaTime * 20f);
        rightHandTrans.rotation = Quaternion.Euler(playerTransReceived[5]);
        
//         toolsBox.SetActive(ScenarioEventManager.instance.isGameStarted);
// #if UNITY_EDITOR
//         toolsBox.SetActive(true);
// #endif
    }
    public void SetPlayerID(int value)
    {
        _playerID = value;
    }
    public bool CheckPlayerID(int value){
        return GetPlayerID() == value;
    }
    public void SwapPlayerID(){
        if(_playerID ==1){
            SetPlayerID(2);
        }else if(_playerID == 2){
            SetPlayerID(1);
        }
            
    }
    public bool IsPlayerOne(){
		return   GetPlayerID() == 1 ;
	}
    public void SendAll(string rfcName)
	{
		tno.Send(rfcName, Target.All);
	}    
	public void SendAll(string rfcName, object obj1)
	{
		tno.Send(rfcName, Target.All, obj1);
	}
    [RFC] void PlayerTransforms(Vector3 [] playerTrans)
    {
        playerTransReceived = playerTrans;
    }
}
