﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesterTrigger : MonoBehaviour
{
    public bool _blackPenConnected;
    public bool _redPenConnected;

    void OnEnable(){
        _redPenConnected = false;
    }
    void OnDisable(){
        _redPenConnected = false;
    }
    void OnTriggerEnter(Collider other){
        Debug.Log("On enter?????" + other.name);
        switch(other.tag){
            // case("TesterBlackPen"): 
            //     _blackPenConnected = true;
            //     break;
            case("TesterRedPen"):
                _redPenConnected = true;
                break;
            default: break;
        }
    }
        void OnTriggerExit(Collider other){

        switch(other.tag){
            // case("TesterBlackPen"): 
            //             Debug.Log("Black");

            //     _blackPenConnected = false;
            //     break;
            case("TesterRedPen"):
                        // Debug.Log("Red");
// 
                _redPenConnected = false;
                break;
            default: break;
        }
    }

    void Update(){
        if(_redPenConnected){
            RFCMessenger.instance.NextStepRFC("TesterConnected");
            _redPenConnected= false;
        }

    }
}
