﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;
using VRTK.Controllables.ArtificialBased;
using VRTK;
public class SwitchGear : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator animator;
    public List<GameObject> colliderObjects = new List<GameObject>();
    public List<Animator> animatorList = new List<Animator>();
    public List<GameObject> FingerProcedureObjects = new List<GameObject>();
    public GameObject safetyBarrier;
    // public List<BoxCollider> step3_trolley_colliders = new List<BoxCollider>();
    public VRTK_ArtificialSlider trolleyController;
    public ParticleSystem[] explosionEffectList;
    public AudioSource explosionSound;
    public Transform meggerTesterTransform;
    public GameObject EarthingToSwitchGear;
    public GameObject rotator;
    public GameObject testerBlackClip;
    private bool _exploded;

    private void Start() {
        SetReadyToPull(false);

        trolleyController.ValueChanged += ValueChanged;
        trolleyController.MaxLimitReached += MaxLimitReached;
    }
    private void Update(){
        foreach(ParticleSystem effect in explosionEffectList){
            if(effect.isPlaying){
                _exploded = true;
                return;
            }else{ 
                _exploded = false;
            }
        }
    }

    public void SetReadyToPull(bool value)
    {
        foreach (var item in trolleyController.onlyInteractWith)
        {
            item.GetComponentInChildren<BoxCollider>(true).enabled = value;
        }       

    }
    
    protected virtual void ValueChanged(object sender, ControllableEventArgs e)
    {
        // Debug.Log(e.value.ToString("F3"));
        if (AMVRNetworkManager.instance.GetCurrentPlayer().IsPlayerOne())
            RFCMessenger.instance.SendOthers("TrolleyZ", e.value);
    }
    public void SetFinishedPull(){
        // trolleyController.enabled = false;
        // trolleyController.transform.localPosition = new Vector3(0,0,1.8f);
        // trolleyController.setva
        Debug.Log("Test");
        trolleyController.GetComponent<VRTK_InteractableObject>().enabled = false;
        trolleyController.GetComponent<VRTK_InteractableObject>().isGrabbable = false;
        // trolleyController.GetComponent<VRTK_InteractGrab>().ForceRelease();
        // trolleyController.GetComponent<VRTK_InteractGrab>().enabled = false;
    }

    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        Debug.Log("TrolleyZ MaxLimitReached");
        if (GlobalConfig.instance.GetNetworkID() == 1)
            RFCMessenger.instance.NextStepRFC("TrolleyPulled");
        // RFCMessenger.instance.SendOthers("Step3Finish");
    }

    // float z = 0;
    // private void Update() {
    //     if (Input.GetKey(KeyCode.DownArrow))
    //     {
    //         z += 0.001f;
    //         trolleyController.SetValue(z);
    //     }
    // }

    public void PlayExplosion(int index)
    {   _exploded = true;
        if(explosionEffectList[index].isPlaying){
            explosionEffectList[index].Stop();
        }
        explosionEffectList[index].Play();
        
        HapticsManager.instance.PlayVibration();

        explosionSound.PlayOneShot(explosionSound.clip);
    }

    // private void Update() {
    //     if (Input.GetKeyDown(KeyCode.DownArrow))
    //     {
    //         PlayExplosion();
    //     }
    // }
    public void SetRotatorAngle(float angle)
    {
        if (rotator != null)
            rotator.GetComponentInChildren<VRTK_ArtificialRotator>().SetValue(angle);
    }
    
    public void SetRotator(GameObject obj)
    {
        if (rotator != null)
        {
            Destroy(rotator.gameObject);
            rotator = null;
        }
        rotator = obj;
    }
    public bool GetExplosionBool(){
        return _exploded;
    }
}
