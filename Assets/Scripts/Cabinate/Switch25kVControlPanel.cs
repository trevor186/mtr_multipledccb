﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch25kVControlPanel : MonoBehaviour
{
    public enum ControlPanelProcedure{
        FINGERING_VT_LIGHT,
        SET_CONTROL_TO_LOCAL,
        FINGERING_LOCAL_LIGHT,
        APPLY_PADLOCK,
        APPLY_CAUTION_NOTICE,
        TURN_ISOLATOR_OPEN,
        FINGERING_ISOLATOR_GREEN_LIGHT,
        FINGERING_ISOLATOR_IN_OPEN_POSITION,
        FINISH,
        CLOSE_EARTH_SWITCH,
        FINGERING_EARTH_LIGHT,
        FINGERING_EARTH_SWITCH,
        PROCEDURE_FINISH
    }
    public enum CloseSwitchProcedure{

    }
    public ControlPanelProcedure current503AProcedure;
    public ControlPanelProcedure current504AProcedure;
    public List<GameObject> localPadlockList = new List<GameObject>();
    public List<GameObject> isolatorPadlockList = new List<GameObject>();    
    public List<GameObject> earthPadlockList = new List<GameObject>();
    public bool controlPanel503AIsolatorFingering;
    public bool controlPanel504AIsolatorFingering;
    public bool finishedProcedure;
public void ControlPanelActionCallResult(string action,string extraInfo =null ){
        if(extraInfo.StartsWith("503A")) { 
            CheckAction(current503AProcedure,action,extraInfo);
        }else if ( extraInfo =="504A"){
            CheckAction(current504AProcedure,action,extraInfo);
        }
    }
    public void CheckAction(ControlPanelProcedure currentProcedure,string action,string extraInfo =null ){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        Debug.Log("Control panel procedure ???  "+ action);

        if(GetCurrentPlayerID() ==1){
            switch(action){
                case("SetControlToLocal"):
                    if(!IncorrectStepHandleManager.instance.CheckAction(new string[]{extraInfo+"_VTLight"})){
                        Debug.Log("error");
                        IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_SetControlToLocal");
                    }
                    break;
                case("IsolatorOpen"):
                    if(!IncorrectStepHandleManager.instance.CheckAction(new string[]{extraInfo+"_ApplyPadlock"})){
                        Debug.Log("error");
                        IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_IsolatorOpen");
                    }
                    break;
                default:break;
            }
        }

        switch(currentProcedure){
            case(ControlPanelProcedure.FINGERING_VT_LIGHT):
                // if(action =="SetControlToLocal"){
                //     Debug.Log("Error");
                //     IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_SetControlToLocal");
                // }
                if(action == "VTLight" && GetCurrentPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_VTLight",1 ,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_VTLight",2 ,true);   
                    // IncorrectStepHandleManager.instance.StoreAction(extraInfo+"_VTLight1");             
                }
                if(action == "VTLight" && GetCurrentPlayerID() == 2){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    // IncorrectStepHandleManager.instance.StoreAction(extraInfo+"_VTLight2");             
                }
                break;
            case(ControlPanelProcedure.SET_CONTROL_TO_LOCAL):
                if(action =="SetControlToLocal" && GetCurrentPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_SetControlToLocal",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringSetControlToLocal",2,true);
                    rfcMessenger.PlayAnimationBoolRFC("LocalOnOff_switch_On",extraInfo+"_LocalOnOff_switch",true);
                    rfcMessenger.PlayAnimationBoolRFC("Light_on",extraInfo+"_Local_light",true);
                }                
                if(action =="FingeringSetControlToLocal" && GetCurrentPlayerID() == 2){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(ControlPanelProcedure.FINGERING_LOCAL_LIGHT):
                if(action =="FingeringLocalLight" && GetCurrentPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLocalLight",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLocalLight",2,true);
                }                
                if(action =="FingeringLocalLight" && GetCurrentPlayerID() == 2){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(ControlPanelProcedure.APPLY_PADLOCK):
                if(action =="ApplyPadlock" && GetCurrentPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_ApplyPadlock",1,false);
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");

                }                

                break;
            case(ControlPanelProcedure.APPLY_CAUTION_NOTICE):
                if(action =="ControlPadLock_Caution_Notice_prefab(Clone)" && GetCurrentPlayerID() == 1){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }                
                break;
            case(ControlPanelProcedure.TURN_ISOLATOR_OPEN):
                if(action =="IsolatorOpen" && GetCurrentPlayerID() == 1){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }                
                break;
            case(ControlPanelProcedure.FINGERING_ISOLATOR_GREEN_LIGHT):
                if(action =="FingeringIsolatorLight" && GetCurrentPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringIsolatorLight",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringIsolatorLight",2,true);
                }                
                if(action =="FingeringIsolatorLight" && GetCurrentPlayerID() == 2){
                    if(extraInfo.StartsWith("503A")){
                        controlPanel503AIsolatorFingering = true;
                    }else if (extraInfo.StartsWith("504A")){
                        controlPanel504AIsolatorFingering = true;
                    }
                    if(controlPanel503AIsolatorFingering &&controlPanel504AIsolatorFingering){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }
                }
                break;
            case(ControlPanelProcedure.FINGERING_ISOLATOR_IN_OPEN_POSITION):
                if(action =="FingeringIsolator"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    finishedProcedure = true;
                }                
                break;
            case(ControlPanelProcedure.CLOSE_EARTH_SWITCH):
                if(action =="CloseEarthSwitch" && GetCurrentPlayerID() == 1){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;           
            case(ControlPanelProcedure.FINGERING_EARTH_LIGHT):
                if(action =="FingeringEarthSwitchCloseLight" && GetCurrentPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitchCloseLight",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitchCloseLight",2,true);
  
                }                
                if(action =="FingeringEarthSwitchCloseLight" && GetCurrentPlayerID() == 2){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;            
            case(ControlPanelProcedure.FINGERING_EARTH_SWITCH):
                 if(action =="FingeringEarthSwitch" && GetCurrentPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitch",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitch",2,true);
                }                
                if(action =="FingeringEarthSwitch" && GetCurrentPlayerID() == 2){
                    ControlPanelReadStepInstructions(false,extraInfo+"_True");
                }
                break;   
            default:break;
        }
    }
    public void ControlPanelReadStepInstructions(bool result,string extraInfo = null, bool soloTest = false){
        Debug.Log("bool?   " + result);
        Debug.Log("??? extra  " + extraInfo);
    if(extraInfo.StartsWith("503A")) { 
            StartCoroutine(UpdateStepInstructions(current503AProcedure,result,extraInfo,soloTest));
        
        }else if ( extraInfo =="504A"){
            StartCoroutine(UpdateStepInstructions(current504AProcedure,result,extraInfo,soloTest));
        
        }
    }
    public IEnumerator UpdateStepInstructions(ControlPanelProcedure currentProcedure,bool result,string extraInfo = null, bool soloTest = false){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        if(result){
            Debug.Log("? current procedure" + currentProcedure.ToString());
            currentProcedure += 1;
            if(extraInfo.StartsWith("503A")){
                current503AProcedure = currentProcedure;
            }else{
                current504AProcedure = currentProcedure;
            }
            Debug.Log("New? " + currentProcedure.ToString());
            switch(currentProcedure){
                case(ControlPanelProcedure.SET_CONTROL_TO_LOCAL):
                    IncorrectStepHandleManager.instance.StoreAction(extraInfo+"_VTLight");
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_VTLight",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_VTLight",2,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_SetControlToLocal",1,true);
                    break;
                case(ControlPanelProcedure.FINGERING_LOCAL_LIGHT):
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_SetControlToLocal",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringSetControlToLocal",2,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLocalLight",1,true);
                    break;
                case(ControlPanelProcedure.APPLY_PADLOCK):
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLocalLight",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLocalLight",2,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_ApplyPadlock",1,true);
                    break;
                case(ControlPanelProcedure.APPLY_CAUTION_NOTICE):
                    foreach(GameObject obj in localPadlockList){
                        if(obj.name.Contains(extraInfo)){
                            obj.SetActive(true);
                            AnimationManager.instance.RunAnimationBool("Lock_open",obj.name,false);
                        }
                    }
                    rfcMessenger.SetSnapEventRFC(extraInfo+"_ControlPadLock");
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_ApplyPadlock",1,false);
                    break;
                case(ControlPanelProcedure.TURN_ISOLATOR_OPEN):
                    IncorrectStepHandleManager.instance.StoreAction(extraInfo+"_ApplyPadlock");
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_IsolatorOpen",1,true);
                    break;
                case(ControlPanelProcedure.FINGERING_ISOLATOR_GREEN_LIGHT):
                    foreach(GameObject obj in isolatorPadlockList){
                        if(obj.name.Contains(extraInfo)){
                            AnimationManager.instance.RunAnimationBool("Lock_open",obj.name,true);
                        }
                    }
                    AnimationManager.instance.RunAnimationBool("Isolator_switch_open",extraInfo+"_Isolator_switch", true);
                    AnimationManager.instance.RunAnimationBool("Isolator_switch_open",extraInfo+"_Isolator_switch", true);
                    AnimationManager.instance.RunAnimationBool("Light_on",extraInfo+"_IsolatorOpen_light", true);
                    AnimationManager.instance.RunAnimationBool("Light_on",extraInfo+"_IsolatorClose_light", false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_IsolatorOpen",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringIsolatorLight",1,true);
                    yield return new WaitForSeconds(2f);
                    foreach(GameObject obj in isolatorPadlockList){
                        if(obj.name.Contains(extraInfo)){
                            AnimationManager.instance.RunAnimationBool("Lock_open",obj.name,false);
                        }
                    }
                    break;
                case(ControlPanelProcedure.FINGERING_ISOLATOR_IN_OPEN_POSITION):
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringIsolatorLight",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringIsolatorLight",2,false);
                    TeleportManager.instance.Teleport(MultiDCCBSceneEventManager.instance.player1AreaTransform[5]);
                    TeleportManager.instance.SetCurrentPointerList(new string[] {"Isolator503ABack","Isolator504ABack"});
                    TeleportManager.instance.SetPointer("Isolator503ABack",true);
                    rfcMessenger.SetCollideEventRFC("503A_FingeringIsolator",1,true);
                    rfcMessenger.SetCollideEventRFC("504A_FingeringIsolator",1,true);
                    current503AProcedure = ControlPanelProcedure.FINGERING_ISOLATOR_IN_OPEN_POSITION;
                    current504AProcedure = ControlPanelProcedure.FINGERING_ISOLATOR_IN_OPEN_POSITION;

                    break;
                case(ControlPanelProcedure.FINISH):
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringIsolator",1,false);

                    if(current503AProcedure == ControlPanelProcedure.FINISH &&
                        current504AProcedure == ControlPanelProcedure.FINISH &&
                        AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID()==1
                    ){
                        MultiDCCBSceneEventManager.instance.current25kProcedure = MultiDCCBSceneEventManager.Cabinate25kProcedure.AC_SWITCHGEAR_PROCEDURE;
                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                    break;
                case(ControlPanelProcedure.CLOSE_EARTH_SWITCH):
                    break;           
                case(ControlPanelProcedure.FINGERING_EARTH_LIGHT):
                    AnimationManager.instance.RunAnimationBool("Earth_switch_close",extraInfo+"_Earth_switch",true);
                    rfcMessenger.PlayAnimationBoolRFC("Light_on",extraInfo+"_EarthSwitchOpen_light",false);
                    rfcMessenger.PlayAnimationBoolRFC("Light_on",extraInfo+"_EarthSwitchClose_light",true);
                    foreach(GameObject obj in earthPadlockList){
                        if(obj.name.Contains(extraInfo)){
                            AnimationManager.instance.RunAnimationBool("Lock_open",obj.name,true);
                        }
                    }
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitchCloseLight",1,true);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_CloseEarthSwitch",1,false);

                    yield return new WaitForSeconds(2);
                    foreach(GameObject obj in earthPadlockList){
                        if(obj.name.Contains(extraInfo)){
                            AnimationManager.instance.RunAnimationBool("Lock_open",obj.name,false);
                        }
                    }
                    break;            
                case(ControlPanelProcedure.FINGERING_EARTH_SWITCH):
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitchCloseLight",1,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitchCloseLight",2,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitch",1,true);
                    break;
                case(ControlPanelProcedure.PROCEDURE_FINISH):
                    if(current503AProcedure == ControlPanelProcedure.PROCEDURE_FINISH &&
                        current504AProcedure == ControlPanelProcedure.PROCEDURE_FINISH
                    ){
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitch",1,false);
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthSwitch",2,false);
                        TeleportManager.instance.CloseAllPointers();

                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                    break; 
                default:break;
            }
        }else{
            switch(currentProcedure){
                case(ControlPanelProcedure.FINGERING_VT_LIGHT):
                    break;
                case(ControlPanelProcedure.SET_CONTROL_TO_LOCAL):
                    break;
                case(ControlPanelProcedure.FINGERING_LOCAL_LIGHT):
                    break;
                case(ControlPanelProcedure.APPLY_PADLOCK):
                    break;
                case(ControlPanelProcedure.APPLY_CAUTION_NOTICE):
                    break;
                case(ControlPanelProcedure.TURN_ISOLATOR_OPEN):
                    break;
                case(ControlPanelProcedure.FINGERING_ISOLATOR_GREEN_LIGHT):
                    break;
                case(ControlPanelProcedure.FINGERING_ISOLATOR_IN_OPEN_POSITION):
                    break;
                default:break;
            }
        }
        yield return 0;
    }
    public int GetCurrentPlayerID(){
        return AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID();
    }
}

