﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene2KSRSwitchCabinate : MonoBehaviour
{
    public string cabinateCode;
    public enum CabinateProcedure{
        START,
        FINGERING_PANEL,
        FINGERING_ISOLATOR,
        OPEN_DOOR,
        TURN_CONTROL_TO_LOCAL,
        CLOSE_DOOR,
        APPLY_LOCK,
        APPLY_CAUTION_NOTICE,
        FINISH_CONTROL_PROCEDURE
    }
    public bool controlToLocalProcedureBool;
    public CabinateProcedure currentProcedure;

 public void CabinateActionCallResult(string action,string extraInfo =null){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        switch(currentProcedure){
           case(CabinateProcedure.FINGERING_PANEL):
                if(action =="FingeringPanel"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",1,false);
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",2,true);
                    }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }
                }
                break;
            case(CabinateProcedure.FINGERING_ISOLATOR):
                if(action =="FingeringIsolator"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",1,false);
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",2,true);
                    }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }
                }
                break;
            case(CabinateProcedure.OPEN_DOOR):
                if(action =="Door"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_open",cabinateCode+"_KSR_machine");
                }
                break;
            case(CabinateProcedure.TURN_CONTROL_TO_LOCAL):
                if(action =="LocalSwitch"){
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_LocalSwitch",1,true);
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_local",cabinateCode+"_KSR_machine");   
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_LocalSwitch",1,false);                
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLocalSwitch",2,true);                
                }
                if(action =="FingeringLocalSwitch"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(CabinateProcedure.CLOSE_DOOR):
                if(action =="Door"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_close",cabinateCode+"_KSR_machine");
                }
                break;
            case(CabinateProcedure.APPLY_LOCK):
                if(action =="Lock"){
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_padlock",cabinateCode+"_KSR_machine");   
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_Lock",1,false);   
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLock",2,true);   
                }
                if(action =="FingeringLock"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(CabinateProcedure.APPLY_CAUTION_NOTICE):
                if(action =="Panel_Caution_Notice_prefab(Clone)"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_notice",cabinateCode+"_KSR_machine");   
                }
                break;
            default:
                break;
        }
    }
    public IEnumerator CabinateReadStepInstructions(bool result,bool soloTest = false){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        if(result){
            currentProcedure +=1;
            switch(currentProcedure){
                case(CabinateProcedure.FINGERING_ISOLATOR):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",2,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",1,true);

                    break;
                case(CabinateProcedure.OPEN_DOOR):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",2,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,true);
                    break;
                case(CabinateProcedure.TURN_CONTROL_TO_LOCAL):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,false);
                    yield return new WaitForSeconds(2f);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_LocalSwitch",1,true);   
                    break;
                case(CabinateProcedure.CLOSE_DOOR):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_LocalSwitch",1,false);   
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringLocalSwitch",2,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,true);

                    break;
                case(CabinateProcedure.APPLY_LOCK):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_Lock",1,true);
                    break;
                case(CabinateProcedure.APPLY_CAUTION_NOTICE):
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_Lock",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_FingeringLock",2,false);
        
                    rfcMessenger.SetSnapEventRFC(cabinateCode+"_Panel");               
                    break;
                case(CabinateProcedure.FINISH_CONTROL_PROCEDURE):
                    controlToLocalProcedureBool = true;
                    rfcMessenger.ReadStepInstructionsRFC(false,cabinateCode+"_Done");
                    break;
                default: break;
            }

        }
        yield return 0;
    }
}
