﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCabinate : MonoBehaviour
{
    public enum SwitchCabinateType{ SWITCH25KV,SWITCH750V};
    public SwitchCabinateType cabinateType;
    public List<GameObject> animatorObjList = new List<GameObject>();
    public List<CollideEvent> collideEventList = new List<CollideEvent>();
    public List<MultiDCCBNoticeSnapEvent> snapEventList = new List<MultiDCCBNoticeSnapEvent>();
    // Start is called before the first frame update

    public void SetColliderObjectActive(string value, int playerID, bool activeStatus){
        Debug.Log("Open ");
         GameObject colliderObj = null;
        foreach(CollideEvent obj in collideEventList){
            Debug.Log("?????? player obj " + obj.objectPlayerID);
            Debug.Log("??? player? " + playerID);
            if(obj.actionValue == value && obj.objectPlayerID == playerID){
                colliderObj = obj.gameObject;
                break;
            }
        }
        if(colliderObj != null){
            if(colliderObj.name.StartsWith("collider"))
                colliderObj.SetActive(true);
            colliderObj.GetComponent<BoxCollider>().enabled = activeStatus;
        }
    }
        public void RunAnimationTrigger(string animation, string animatorName){
        Debug.Log("Get? " + animation + " in the name " + animatorName);
        GameObject obj = animatorObjList.Find(T => T.name.Contains(animatorName)); 
        Animator objAnimator = obj.GetComponentInChildren<Animator>();        // if(objAnimator == null) objAnimator = obj.GetComponent<Animator>();
        List<AnimatorControllerParameter> triggerList = new List<AnimatorControllerParameter>();
        List<string> triggerNameList = new List<string>();  // Ensure Animation is in the animator
        for(int i = 0; i< objAnimator.parameterCount ; i++){
            int ii=i;
            // triggerNameList.Add(objAnimator.GetParameter(ii).name);
            if(animation == objAnimator.GetParameter(ii).name){
                objAnimator.SetTrigger(animation);
            }
        }
    }
    public void RunAnimationBool(string animation,string animatorName, bool boolValue){   
        Debug.Log("Get? " + animation + " in the name " + animatorName);

        // GameObject obj =).gameObject; 
        GameObject obj = animatorObjList.Find(T => T.name.Contains(animatorName)); 
        Animator objAnimator = obj.GetComponentInChildren<Animator>();        List<AnimatorControllerParameter> triggerList = new List<AnimatorControllerParameter>();
        List<string> triggerNameList = new List<string>();  // Ensure Animation is in the animator
        for(int i = 0; i< objAnimator.parameterCount ; i++){
            int ii=i;
            // triggerNameList.Add(objAnimator.GetParameter(ii).name);
            if(animation == objAnimator.GetParameter(ii).name){
                objAnimator.SetBool(animation,boolValue);
            }
        }
    }
    public void SetSnapEvent(string eventName){
        GameObject obj= snapEventList.Find(T => T.name.Contains(eventName)).gameObject;
        if(obj != null){
            obj.SetActive(true);
        }
    }


    public virtual void CabinateActionCallResult(string action,string extraInfo =null ){

    }
    public virtual IEnumerator CabinateReadStepInstructions(bool result,bool soloTest = false){
        yield return 0;
    }

}
