using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;
using VRTK.Controllables.ArtificialBased;
using VRTK;
public class Switch25kVCabinate : SwitchCabinate
{
    public enum Part1Procedure{
        PREPARE,
        FINGERING_SELECT,
        FINGERING_VOLTAGE_INDICATOR,
        FINGERING_SWITCHGEAR_STATUS,  // 2.4
        CALL_PSC,  //2.5
        FINGERING_CB_STATUS,  // 2.6a
        SET_CONTROL_POSITION,   // 2.7
        APPLY_CAUTION_NOTICE1, //2.6b
        FINISH_PART1

    }
    public enum ACProcedure{
        PREPARE,
        READY,
        FINGERING_VOLTAGE_INDICATOR,
        // CALL_PSC_1,
        FINGERING_CB_STATUS,
        SET_CONTROL_TO_LOCAL,
        APPLY_CAUTION_NOTICE1,
        OPEN_DISCONNECTOR1,
        TURN_HANDLE_TO_ISOLATED_OPEN,
        TAKE_KEY_1,
        CALL_PSC_2,
        OPEN_DISCONNECTOR2,
        TURN_HANDLE_FROM_READY_TO_EARTH,
        TAKE_KEY_2,
        FINGERING_LIGHTING_STATUS,
        PRESS_CLOSE_BUTTON,
        APPLY_PADLOCK,
        FINISH
    }

    public Part1Procedure currentPart1Procedure = Part1Procedure.PREPARE;
    public ACProcedure currentACProcedure = ACProcedure.PREPARE;
    public bool part1ProcedureFinished;
    public bool part1CallPSC;
    public bool acProcedureFinished;
    public bool part1CallPSCPowerOff;
    public string cabinateCode;
    public bool applyCircuitStatus;
    public GameObject currentHandle;
    public List<GameObject> handleColliderList = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void CabinateActionCallResult(string action,string extraInfo =null ){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        
        
        if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() ==1){  //Error Checking
            switch(action){
                case("ControlRemoteToLocal"):
                    if(!IncorrectStepHandleManager.instance.CheckAction(new string[]{"CallPsc",extraInfo+"_FingeringCB"})){
                        Debug.Log("error");
                        IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_ControlRemoteToLocal");
                    }
                    break;          
                default:break;
            }
        }
        Debug.Log("Action   " + action + " continues");
        
        switch(currentPart1Procedure){
            case(Part1Procedure.FINGERING_SWITCHGEAR_STATUS):
                if(action == "FingeringStatus"){
                    Debug.Log("Go here");
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        Debug.Log("Open this");
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringStatus",1 ,false);
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringStatus",2 ,true); 
                                       
                    }
                    
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringStatus",2,false);                
                    
                    }
                }
                break;
            case(Part1Procedure.CALL_PSC):
                if(action =="Phone" && AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    rfcMessenger.ReadStepInstructionsRFC(false,"Phone");
                }
                // if(action =="ControlRemoteToLocal"){
                //     Debug.Log("Error");
                //     IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_ControlRemoteToLocal");
                // }
                break;
            case(Part1Procedure.FINGERING_CB_STATUS):
                // if(action =="ControlRemoteToLocal"){
                //     Debug.Log("Error");
                //     IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_ControlRemoteToLocal");
                // }
                if(action == "CBStatus"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){

                    rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",1 ,false);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",2 ,true);                
                    }
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",2 ,false);
                        // IncorrectStepHandleManager.instance.StoreAction(extraInfo+"_CheckCB");                
                    }
                }
                break;
            case(Part1Procedure.SET_CONTROL_POSITION):
                if(action =="ControlRemoteToLocal" ){
                    // if(IncorrectStepHandleManager.instance.CheckAction(new string[] {"0911_CallPSC","HA09_CheckCB","HA_11_CheckCB"})){
                    string code = extraInfo =="HA09"? "403": "404";
                    rfcMessenger.PlayAnimationBoolRFC("Local",code+"_Button_Local_prefab",true);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_ControlRemoteToLocal",1 ,false);                
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringControlToLocal",2 ,true);                
                    // }else{
                    //     Debug.Log("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
                    // }
                }
                if(action =="FingeringControlToLocal"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringControlToLocal",2 ,false);                
                }                
                break;

            case(Part1Procedure.APPLY_CAUTION_NOTICE1):
                if(action =="CBNotice_Caution_Notice_prefab(Clone)"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            default:break;
        }
    }
    public override IEnumerator CabinateReadStepInstructions(bool result,bool soloTest = false){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        if(result){
            currentPart1Procedure += 1 ;
            Debug.Log("CABINATEEEE   Enter procedure " + currentPart1Procedure.ToString());
            switch(currentPart1Procedure){
                case(Part1Procedure.CALL_PSC):
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_ControlRemoteToLocal",1,true);    

                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringStatus",1 ,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringStatus",2 ,false);                
                    if(part1CallPSCPowerOff){
                        StartCoroutine(CabinateReadStepInstructions(true));
                    }
                    break;
                case(Part1Procedure.FINGERING_CB_STATUS):
                    IncorrectStepHandleManager.instance.StoreAction("CallPsc");
                    yield return new WaitForSeconds(2f);
                    AnimationManager.instance.RunAnimationBool("light_off","HA09_Signal_Button",true);
                    AnimationManager.instance.RunAnimationBool("light_off","HA11_Signal_Button",true);
                    AnimationManager.instance.RunAnimationTrigger("CB_call_psc","403_Button_CB_prefab");
                    AnimationManager.instance.RunAnimationTrigger("CB_call_psc","404_Button_CB_prefab");
                    string code = cabinateCode =="HA09" ? "403": "404";
                    AnimationManager.instance.RunAnimationTrigger("CB_call_psc",code+"_Button_CB_prefab");
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_CBStatus",1,true);
                    break;
                case(Part1Procedure.SET_CONTROL_POSITION):
                    IncorrectStepHandleManager.instance.StoreAction(cabinateCode+"_FingeringCB");
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_ControlRemoteToLocal",1,true);
                    break;
                case(Part1Procedure.APPLY_CAUTION_NOTICE1):
                    Debug.Log("Set up caution notice");
                    rfcMessenger.SetSnapEventRFC(cabinateCode+"_CBNotice");
                    break;
                case(Part1Procedure.FINISH_PART1):  
                    Debug.Log("Go here??????");
                    part1ProcedureFinished = true;
                    StartCoroutine(MultiDCCBSceneEventManager.instance.ReadStepInstructions(false,"CheckStatus"));
                    break;
                default: break;
            }
        }else{
            switch(currentPart1Procedure){
                case(Part1Procedure.CALL_PSC):
                    StartCoroutine(CabinateReadStepInstructions(true));
                    break;

                default:break;
            }
        }

        if(soloTest){
            switch(currentPart1Procedure){
            case(Part1Procedure.FINGERING_CB_STATUS):
                SetColliderObjectActive("CBStatus_2",2 ,true);
                break;
            default: break;
            }
        }

        yield return 0;
    }


    public  void ACActionCallResult(string action,string extraInfo =null ){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        string animationCode = cabinateCode =="HA01" ? "TIS401" :"TIS402";


        if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() ==1){  //Error Checking
            switch(action){
                case("KeyOpenDisconnector"):
                    if(!IncorrectStepHandleManager.instance.CheckAction(new string[]{extraInfo+"_ACSetToLocal"})){
                        Debug.Log("error");
                        IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_KeyOpenDisconnector");
                    }
                    break;
                case("CloseCB"):
                    if(!IncorrectStepHandleManager.instance.CheckAction(new string[]{extraInfo+"_FingeringLightingStatus"})){
                        Debug.Log("error");
                        IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_CloseCB");
                    }
                    break;     
                case("KeyOpenDisconnector2"):
                    if(!IncorrectStepHandleManager.instance.CheckAction(new string[]{"ACCallPSCForEarthing"})){
                        Debug.Log("error");
                        IncorrectStepHandleManager.instance.CreateSmokeError(extraInfo+"_CloseCB");
                    }
                    break;             
                default:break;
            }
        }

        Debug.Log("get      answer??/    " + action);        
        switch(currentACProcedure){
            // case(ACProcedure.CALL_PSC_1):
            //     if(action =="Phone" && AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
            //         rfcMessenger.ReadStepInstructionsRFC(false,"Phone");
            //     }
            //     break;
            case(ACProcedure.FINGERING_CB_STATUS):
                if(action == "CBStatus"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",1 ,false);
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",2 ,true);                
                    }
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",2,false);                
                    }
                }
                break;
            case(ACProcedure.SET_CONTROL_TO_LOCAL):
                if(action =="ACSetToLocal"){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_ACSetToLocal",1 ,false);                
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringControlToLocal",2 ,true);                
                    rfcMessenger.PlayAnimationBoolRFC("Local",animationCode+"_Button_Local_prefab",true);
                }
                if(action =="FingeringControlToLocal"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringControlToLocal",2 ,false);                
                }  
                // part1ProcedureFinished = true;
                // RunAnimationBool("Local","Button_Local_prefab",true);
                // rfcMessenger.ReadStepInstructionsRFC(false,"CheckStatus");
                break;
            case(ACProcedure.APPLY_CAUTION_NOTICE1):
                if(action =="CBNotice_Caution_Notice_prefab(Clone)"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(ACProcedure.OPEN_DISCONNECTOR1):
                if(action =="KeyOpenDisconnector"){
                    // AnimationManager.instance.RunAnimationBool("Local",code+"_Button_Local_prefab",true);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_KeyOpenDisconnector",1 ,false);                
                    // rfcMessenger.PlayAnimationBoolRFC(extraInfo+"")
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(ACProcedure.TURN_HANDLE_TO_ISOLATED_OPEN):   // Turn with handle
                if(action =="IsolatorOpen"){
                    // AnimationManager.instance.RunAnimationBool("Local",code+"_Button_Local_prefab",true);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringDisconnector",2 ,true);                
                    rfcMessenger.PlayAnimationTriggerRFC("CB_isolator_handle",animationCode+"_Button_CB_prefab");
                }
                if(action =="FingeringDisconnector"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;            
            case(ACProcedure.TAKE_KEY_1):
                if(action =="TakeKeyDisconnector"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            // case(ACProcedure.CALL_PSC):   // this will be checked at event manager
            //     if(action =="Phone"){
            //     }
            //     break;
            case(ACProcedure.OPEN_DISCONNECTOR2):   //Use key to open
                if(action =="KeyOpenDisconnector2"){
                    // AnimationManager.instance.RunAnimationBool("Local",code+"_Button_Local_prefab",true);
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            
            case(ACProcedure.TURN_HANDLE_FROM_READY_TO_EARTH):   // Turn with handle
                if(action =="ReadyToEarth"){
                    // AnimationManager.instance.RunAnimationBool("Local",code+"_Button_Local_prefab",true);
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringDisconnector2",2 ,true);                
                    rfcMessenger.PlayAnimationTriggerRFC("CB_RTE_handle",animationCode+"_Button_CB_prefab");
                }
                if(action =="FingeringDisconnector2"){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;   
            case(ACProcedure.TAKE_KEY_2):
                if(action =="TakeKeyDisconnector2"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;         
            case(ACProcedure.FINGERING_LIGHTING_STATUS):   // Turn with handle
                if(action =="FingeringLightingStatus"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;            
            case(ACProcedure.PRESS_CLOSE_BUTTON):   // Turn with handle
                if(action =="CloseCB"){
                    // AnimationManager.instance.RunAnimationBool("Local",code+"_Button_Local_prefab",true);
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;            
            case(ACProcedure.APPLY_PADLOCK):   // Turn with handle
                if(action =="ApplyPadlock"){
                    // AnimationManager.instance.RunAnimationBool("Local",code+"_Button_Local_prefab",true);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_ApplyPadlock",1,false);                
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringPadlock",2 ,true);                
                    rfcMessenger.PlayAnimationTriggerRFC("Lock",animationCode+"_CloseButton_Padlock");
                }
                if(action =="FingeringPadlock"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            
            // case(ACActionCallResult.FINGERING_SWITCHGEAR_STATUS):

            // case(Part1Procedure.CALL_PSC):
            //     if(action =="Phone"){
            //         rfcMessenger.ReadStepInstructionsRFC(false,"Phone");
            //     }
            //     break;
            // case(Part1Procedure.FINGERING_CB_STATUS):
            //     if(action == "CBStatus"){
            //         rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",1 ,false);
            //         rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",2 ,true);                
            //     }
            //     if(action == "CBStatus"){
            //         rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
            //         rfcMessenger.SetCollideEventRFC(extraInfo+"_CBStatus",2 ,false);                
            //     }
            //     break;


                // break;
            default:break;
        }
    }
    public void CheckACAction(Part1Procedure currentProcedure, string action, string extraInfo){
                MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        switch(currentProcedure){
            default: break;;
        }
    }
    public IEnumerator ACReadStepInstructions(bool result,bool soloTest = false){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        string animationCode = cabinateCode =="HA01" ? "TIS401" :"TIS402";
        if(result){
            currentACProcedure += 1 ;
            Debug.Log("Enter procedure " + currentACProcedure.ToString());
            switch(currentACProcedure){
                case(ACProcedure.FINGERING_CB_STATUS):
                    // yield return new  WaitForSeconds(2);
                    // AnimationManager.instance.RunAnimationBool("light_off","HA01_Signal_Button",true);
                    // AnimationManager.instance.RunAnimationBool("light_off","HA03_Signal_Button",true);
                    // Debug.Log("Call this???");
                    // AnimationManager.instance.RunAnimationTrigger("CB_call_psc","TIS401_Button_CB_prefab");
                    // AnimationManager.instance.RunAnimationTrigger("CB_call_psc","TIS402_Button_CB_prefab");
                    // rfcMessenger.SetCollideEventRFC(cabinateCode+"_CBStatus",1 ,true);
                    break;
                case(ACProcedure.SET_CONTROL_TO_LOCAL):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_CBStatus",1 ,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_CBStatus",2 ,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_ACSetToLocal",1 ,true);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_KeyOpenDisconnector",1 ,true);                
                    break;           
                case(ACProcedure.APPLY_CAUTION_NOTICE1):
                    Debug.Log("Set up caution notice");
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_ControlRemoteToLocal",1 ,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringControlToLocal",2 ,false); 

                    rfcMessenger.PlayAnimationBoolRFC("Local",animationCode+"_Button_Local_prefab",true);
                    rfcMessenger.SetSnapEventRFC(cabinateCode+"_CBNotice");
                    break;
     
                case(ACProcedure.OPEN_DISCONNECTOR1):
                    IncorrectStepHandleManager.instance.StoreAction(cabinateCode+"_ACSetToLocal");
                    AnimationManager.instance.RunAnimationBool("Local",animationCode+"_Button_Local_prefab",true);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_ACSetToLocal",1 ,false); 
                    break;
                case(ACProcedure.TURN_HANDLE_TO_ISOLATED_OPEN):
                    AnimationManager.instance.RunAnimationTrigger("CB_isolator_start",animationCode+"_Button_CB_prefab");
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_KeyOpenDisconnector",1 ,false);
                    // rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringDisconnector",2 ,false);
                    handleColliderList[0].SetActive(true);
                    handleColliderList[0].GetComponent<BoxCollider>().enabled = true;
                    break;
                case(ACProcedure.TAKE_KEY_1):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_TakeKeyDisconnector",1 ,true);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringDisconnector",2 ,false);                

                    break;
                case(ACProcedure.CALL_PSC_2):
                    AnimationManager.instance.RunAnimationTrigger("CB_isolator_close",animationCode+"_Button_CB_prefab");
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_KeyOpenDisconnector2",1,true);                

                    // this will be checked at eventmanager. 
                    break;
                case(ACProcedure.OPEN_DISCONNECTOR2):  //NEED CHECK THIS 
                    IncorrectStepHandleManager.instance.StoreAction("ACCallPSCForEarthing");
                    break;               
                     
                case(ACProcedure.TURN_HANDLE_FROM_READY_TO_EARTH):
                    AnimationManager.instance.RunAnimationTrigger("CB_RTE_start",animationCode+"_Button_CB_prefab");
                    handleColliderList[1].SetActive(true);
                    handleColliderList[1].GetComponent<BoxCollider>().enabled = true;
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_KeyOpenDisconnector2",1,false);                

                    break;   
                case(ACProcedure.TAKE_KEY_2):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_TakeKeyDisconnector2",1 ,true);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringDisconnector2",2 ,false);                
                    break;             
                case(ACProcedure.FINGERING_LIGHTING_STATUS):
                    AnimationManager.instance.RunAnimationTrigger("CB_RTE_close",animationCode+"_Button_CB_prefab");
                    yield return new WaitForSeconds(1);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringLightingStatus",1,true);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_CloseCB",1,true);                                
                    break;                
                case(ACProcedure.PRESS_CLOSE_BUTTON):
                    IncorrectStepHandleManager.instance.StoreAction(cabinateCode+"_FingeringLightingStatus");
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringLightingStatus",1,false);                
                    break;             
                case(ACProcedure.APPLY_PADLOCK):
                    AnimationManager.instance.RunAnimationBool("CloseButton_open",animationCode+"_Close_Button_prefab",true);
                    AnimationManager.instance.RunAnimationBool("CloseButton_enter",animationCode+"_Close_Button_prefab",true);
                    AnimationManager.instance.RunAnimationBool("CloseButton_close",animationCode+"_Close_Button_prefab",true);
                    AnimationManager.instance.RunAnimationBool("CB_cb_close",animationCode+"_Button_CB_prefab",true);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_CloseCB",1,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_ApplyPadlock",1,true);                
                    break;
                case(ACProcedure.FINISH):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_ApplyPadlock",1,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPadlock",2 ,false);
                    acProcedureFinished = true;                
                    StartCoroutine(MultiDCCBSceneEventManager.instance.ReadStepInstructions(false,"CheckStatus"));
                    break;
                default: break;
            }
        }else{
            switch(currentACProcedure){
                case(ACProcedure.CALL_PSC_2):
                    StartCoroutine(CabinateReadStepInstructions(true));
                    break;

                default:break;
            }
        }

        if(soloTest){
            switch(currentPart1Procedure){
            case(Part1Procedure.FINGERING_CB_STATUS):
                SetColliderObjectActive("CBStatus_2",2 ,true);
                break;
            default: break;
            }
        }

        yield return 0;
    }

    public void SetRotatorAngle(float angle)
    {
        if (currentHandle != null)
            currentHandle.GetComponentInChildren<VRTK_ArtificialRotator>().SetValue(angle);
    }
    public void CloseHandleSnap(string code,string hole){
        foreach(GameObject handlesnapper in handleColliderList){
            SwitchHandleSnapEvent snapEvent = handlesnapper.GetComponent<SwitchHandleSnapEvent>() ;
            if(snapEvent.cabinateCode == code && snapEvent.holeName == hole){
                snapEvent.GetComponent<BoxCollider>().enabled = false;
                return;
            }
        }
    }
}
