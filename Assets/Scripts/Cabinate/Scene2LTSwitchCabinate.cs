﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene2LTSwitchCabinate : MonoBehaviour
{
    public enum CabinateProcedure{
        START,
        FINGERING_PANEL,
        FINGERING_ISOLATOR,
        OPEN_DOOR,
        TURN_CONTROL_TO_OUT_OF_USE,
        CLOSE_DOOR,
        APPLY_LOCK,
        APPLY_CAUTION_NOTICE,
        FINISH_CONTROL_PROCEDURE,
        FINGERING_EARTHING_TOP,
        // FINGERING_EARTHING_BOTTOM,
        // ATTACH_CLIP,
        ATTACH_LOCK,
        FINISH_APPLY_EARTHING
    }
    public bool controlToOffProcedureBool;
    public string cabinateCode;
    public bool applyCircuitStatus;
    public bool applyLockStatus;
    public CabinateProcedure currentProcedure;
    public GameObject lockCollider;
    public GameObject clipCollider;
    public GameObject clipCable;

    // Start is called before the first frame update
 public void CabinateActionCallResult(string action,string extraInfo =null){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        switch(currentProcedure){
            case(CabinateProcedure.FINGERING_PANEL):
                if(action =="FingeringPanel"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",1,false);
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",2,true);
                    }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }
                }
                break;
            case(CabinateProcedure.FINGERING_ISOLATOR):
                if(action =="FingeringIsolator"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",1,false);
                        rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",2,true);
                    }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }
                }
                break;
            case(CabinateProcedure.OPEN_DOOR):
                if(action =="Door"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_open",cabinateCode);
                }
                break;
            case(CabinateProcedure.TURN_CONTROL_TO_OUT_OF_USE):
                if(action =="LocalSwitch"){
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_LocalSwitch",1,true);
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_local",cabinateCode);   
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_LocalSwitch",1,false);                
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLocalSwitch",2,true);                
                }
                if(action =="FingeringLocalSwitch"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(CabinateProcedure.CLOSE_DOOR):
                if(action =="Door"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_close",cabinateCode);
                }
                break;
            case(CabinateProcedure.APPLY_LOCK):
                if(action =="Lock"){
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_padlock",cabinateCode);   
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringLock",2,true);   
                }
                if(action =="FingeringLock"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;
            case(CabinateProcedure.APPLY_CAUTION_NOTICE):
                if(action =="Panel_Caution_Notice_prefab(Clone)"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.PlayAnimationTriggerRFC("KSR_notice",cabinateCode);   
                }
                break;
            case(CabinateProcedure.FINGERING_EARTHING_TOP):
                if(action =="FingeringCircuit"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().IsPlayerOne()){
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringCircuit",1,false);
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringCircuit",2,true);
                    }else{
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }  
                }
                break;
            // case(CabinateProcedure.FINGERING_EARTHING_BOTTOM):
            //     if(action =="FingeringEarthingClip"){
            //         if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().IsPlayerOne()){
            //             rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthingClip",1,false);
            //             rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringEarthingClip",2,true);
            //         }else{
            //             rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
            //         }  
            //     }
            //     break;
            // case(CabinateProcedure.ATTACH_CLIP):
            //     if(action =="AttachClip"){
            //         Debug.Log("Get clipped");
            //         clipCollider.GetComponent<BoxCollider>().enabled = false;
            //         rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
            //     }
            //     break;
            case(CabinateProcedure.ATTACH_LOCK):
                if(action =="AttachLock"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                }
                break;

            default:
                break;
        }
    }
    public IEnumerator CabinateReadStepInstructions(bool result,bool soloTest = false){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        if(result){
            currentProcedure +=1;
            switch(currentProcedure){
                case(CabinateProcedure.FINGERING_ISOLATOR):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",2,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringIsolator",1,true);
                    break;
                case(CabinateProcedure.OPEN_DOOR):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringPanel",2,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,true);
                    break;
                case(CabinateProcedure.TURN_CONTROL_TO_OUT_OF_USE):
                    
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_LocalSwitch",1,true);   
                    break;
                case(CabinateProcedure.CLOSE_DOOR):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_LocalSwitch",1,false);   
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringLocalSwitch",2,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,true);

                    break;
                case(CabinateProcedure.APPLY_LOCK):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_Door",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_Lock",1,true);
                    break;
                case(CabinateProcedure.APPLY_CAUTION_NOTICE):
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_Lock",1,false);
                    rfcMessenger.SetCollideEventRFC(cabinateCode +"_FingeringLock",2,false);
        
                    rfcMessenger.SetSnapEventRFC(cabinateCode+"_Panel");               
                    break;
                case(CabinateProcedure.FINISH_CONTROL_PROCEDURE):
                    controlToOffProcedureBool = true;
                    rfcMessenger.ReadStepInstructionsRFC(false,"CheckProcedure");
                    break;
                case(CabinateProcedure.FINGERING_EARTHING_TOP):
                    break;
                // case(CabinateProcedure.FINGERING_EARTHING_BOTTOM):
                //     rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringCircuit",1,false);
                //     rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringCircuit",2,false);
                //     rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringEarthingClip",1,true);
                //     break;
                // case(CabinateProcedure.ATTACH_CLIP):
                //     rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringEarthingClip",1,false);
                //     rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringEarthingClip",2,false);
                //     clipCollider.GetComponent<EarthingContactCollider>().earthLockStart = true;
                //     clipCollider.SetActive(true);

                //     break;
                case(CabinateProcedure.ATTACH_LOCK):
                    clipCollider.GetComponent<BoxCollider>().enabled = false;
                    clipCable.SetActive(true);
                    lockCollider.SetActive(true);
                    break;

                case(CabinateProcedure.FINISH_APPLY_EARTHING):
                
                    rfcMessenger.RemoveLeftHandObjectRFC();
                    applyLockStatus = true;
                    rfcMessenger.ReadStepInstructionsRFC(false,"CheckProcedure");
                    break;
                default: break;
            }

        }
        yield return 0;
    }
}
