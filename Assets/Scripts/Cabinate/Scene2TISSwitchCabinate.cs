﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene2TISSwitchCabinate : MonoBehaviour
{
    public enum CabinateProcedure{
        START,
        FINISHED_FINGERING,
        FINISHED_FINGERING_VOLTAGE_INDICATOR,
        FINGERING_CB,
        FINGERING_VT_LIGHT,
        FINISH_FINGERING_CB_PROCEDURE,
        TURN_CONTROL_TO_LOCAL,
        FINISH_TURN_CONTROL_TO_LOCAL_PROCEDURE
    }
    public bool finishFingeringCB;
    public bool controlToLocalProcedureBool;
    public string cabinateCode;
    public CabinateProcedure currentProcedure;
    // Start is called before the first frame update
    public void CabinateActionCallResult(string action,string extraInfo =null){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        switch(currentProcedure){
            case(CabinateProcedure.FINGERING_CB):
                if(action =="FingeringCB"){
                     if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringCB",1,false);                
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringCB",2,true);                
                    }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringCB",2,false);                
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }
                }
                break;
            case(CabinateProcedure.FINGERING_VT_LIGHT):
                if(action == "FingeringVoltageIndicator"){
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringVoltageIndicator",1,false);                
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringVoltageIndicator",2,true);                
                    }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringVoltageIndicator",2,false);                
                        rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    }
                }
                break;
            case(CabinateProcedure.TURN_CONTROL_TO_LOCAL):
                if(action =="SetToLocal"){
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_SetToLocal",1,false);     
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringSetToLocal",2,true); 
                    rfcMessenger.PlayAnimationBoolRFC("Local",extraInfo+"_Button_Local_prefab",true);
                }
                if(action =="FingeringSetToLocal"){
                    rfcMessenger.ReadStepInstructionsRFC(false,extraInfo+"_True");
                    rfcMessenger.SetCollideEventRFC(extraInfo+"_FingeringSetToLocal",2,false);                
                }
                break;
            

            default:
                break;
        }
    }
    public IEnumerator CabinateReadStepInstructions(bool result){
        MultiDCCBSceneRFCMessenger rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        if(result){
            currentProcedure +=1;
            switch(currentProcedure){
                case(CabinateProcedure.FINGERING_VT_LIGHT):
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringCB",1,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringCB",2,false);                
                    rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringVoltageIndicator",1,true);   
                    break;
                case(CabinateProcedure.FINISH_FINGERING_CB_PROCEDURE):
                    finishFingeringCB = true;
                    rfcMessenger.ReadStepInstructionsRFC(false,"CheckProcedure");
                    break;
                // case(CabinateProcedure.TURN_CONTROL_TO_LOCAL):
                //     // rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringVoltageIndicator",1,true);   
                //     // rfcMessenger.SetCollideEventRFC(cabinateCode+"_FingeringVoltageIndicator",1,true);   
                //     rfcMessenger.SetCollideEventRFC(cabinateCode+"_SetToLocal",1,true);   
                //     break;
                case(CabinateProcedure.FINISH_TURN_CONTROL_TO_LOCAL_PROCEDURE):
                    controlToLocalProcedureBool = true;
                    rfcMessenger.ReadStepInstructionsRFC(false,"CheckProcedure");
                    break;
                default: break;
            }

        }
        yield return 0;
    }
}
