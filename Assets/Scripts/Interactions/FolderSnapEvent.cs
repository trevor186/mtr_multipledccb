﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FolderSnapEvent : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("FolderSnapEvent - OnTriggerEnter" + other.name);

        if (other.tag == "Document")
        {
            this.GetComponent<BoxCollider>().enabled = false;
            RFCMessenger.instance.SendAll("PasteSafetyDoc");

            TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
            if (player != null && player.GetPlayerID() == 2)
            {
                player.CloseDocRFC();
            }
        }
    }
}
