﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCPlayer2 : MonoBehaviour
{
    public GameObject safetyDocumentDone;
    
    private void Start() {
        safetyDocumentDone.SetActive(false);
    }
    private void OnTriggerEnter(Collider other) {
        Debug.Log(other.name);  

        if (other.tag == "Document")
        {
            this.GetComponent<BoxCollider>().enabled = false;
            this.GetComponent<Animator>().SetTrigger("Safety_document");
            RFCMessenger.instance.NextStepRFC("Report");

        }
    }

    public void PasteItem()
    {
        Debug.Log("PasteItem");
        safetyDocumentDone.SetActive(false);
        ScenarioEventManager.instance.PasteSafetyDoc();
    }

    public void TakeItem()
    {
        Debug.Log("TakeItem");
        if (AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() == 1)
        {
            AMVRNetworkManager.instance.GetCurrentPlayer().CloseDocRFC();
        }
        safetyDocumentDone.SetActive(true);
    }
}
