﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class Step1Interaction : AbstractInteraction
{
    // Update is called once per frame
    public override void ObjectGrab(object sender, InteractableObjectEventArgs e)
    {
        Debug.Log("Im Grabbed In Step One");
    }
    public override void ObjectUse(object sender, InteractableObjectEventArgs e)
    {
        Debug.Log("Im Used In step One");
    }
}
