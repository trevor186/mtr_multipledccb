﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
public class DocumentSnapEvent : TNBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log("OnTriggerEnter" + other.name);

        if (other.tag == "Document")
        {
            this.GetComponent<BoxCollider>().enabled = false;

            // ScenarioEventManager.instance.safetyDocumentClone = GameObject.Instantiate(other.gameObject, this.transform.parent);

            TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
            if (player != null)
            {
                if (player.GetPlayerID() == 1)
                {
                    RFCMessenger.instance.ReceiveDocActionRFC(this.name); // only player 1 send this 
                }

                // if (player.GetPlayerID() == 2)
                // {
                //     // player.briefDoc = this.transform.parent.GetComponentInChildren<BriefDocController>(true);
                //     player.SendAll("SafetyDocumentReceived", this.name);
                // }
            }

            // GameObject clone = Instantiate(other.gameObject);
            // clone.GetComponent<BoxCollider>().enabled = false;
            // clone.GetComponentInChildren<VRTK_ArtificialRotator>().enabled = true;
            // snapDropZone.ForceSnap(clone);

            // AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();

            // ScenarioEventManager.instance.SetRotator(clone);
        }
    }
}
