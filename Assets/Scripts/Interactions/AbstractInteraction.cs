﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class AbstractInteraction : MonoBehaviour
{
    public VRTK_DestinationMarker pointer;
    public VRTK_InteractableObject interactableObject;
void Start (){
        if (GetComponent<VRTK_InteractableObject>() != null){
            interactableObject = GetComponent<VRTK_InteractableObject>();
            
            interactableObject.InteractableObjectGrabbed += new InteractableObjectEventHandler(ObjectGrab);
            interactableObject.InteractableObjectUsed += new InteractableObjectEventHandler(ObjectUse);
        }else {
            Debug.LogError("This script needs to attach to an interactable object to work");
            return;
        }
        // GetComponent<VRTK_InteractableObject>()


        pointer = (pointer == null ? GetComponent<VRTK_DestinationMarker>() : pointer);
        if (pointer != null)
            {
                pointer.DestinationMarkerEnter += ObjectPointerEnter;
                pointer.DestinationMarkerHover += ObjectPointerHover;
                pointer.DestinationMarkerExit += ObjectPointerExit;
                pointer.DestinationMarkerSet += ObjectPointerSet;
            }
            else
            {
                VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTKExample_PointerObjectHighlighterActivator", "VRTK_DestinationMarker", "the Controller Alias"));
                return;
            }
    }

    public virtual void ObjectGrab(object sender, InteractableObjectEventArgs e)
    {
        Debug.Log("Im Grabbed");
    }
    public virtual void ObjectUse(object sender, InteractableObjectEventArgs e)
    {
        Debug.Log("Im Used");
    }
    public virtual void ObjectPointerEnter(object sender,DestinationMarkerEventArgs e){
        Debug.Log("Im Pointed");
    }    
    public virtual void ObjectPointerHover(object sender,DestinationMarkerEventArgs e){
        Debug.Log("Im Hovering");
    }   
    public virtual void ObjectPointerExit(object sender,DestinationMarkerEventArgs e){
        Debug.Log("Im Exited");
    }    
    public virtual void ObjectPointerSet(object sender,DestinationMarkerEventArgs e){
        Debug.Log("Im Setted");
    }
}
