﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotLightTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log("SpotLightTrigger OnTriggerEnter: " + other.name);

        if (other.tag == "SpotLight")
        {
            // Debug.Log("it is spot light");
            // other.gameObject.SetActive(true);

            TNetPlayer player = other.GetComponentInParent<TNetPlayer>();
            if (player != null)
            {
                player.SendAll("SpotLightEnable", true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Debug.Log("SpotLightTrigger OnTriggerExit: " + other.name);

        if (other.tag == "SpotLight")
        {
            // Debug.Log("it is spot light");
            // other.gameObject.SetActive(true);

            TNetPlayer player = other.GetComponentInParent<TNetPlayer>();
            if (player != null)
            {
                player.SendAll("SpotLightEnable", false);
            }
        }
    }    
}
