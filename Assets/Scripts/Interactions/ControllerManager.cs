﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.Controllables;
using TNet;
public class ControllerManager : TNBehaviour
{
    public VRTK_DestinationMarker pointer;
    public VRTK_ControllerEvents controllerEvents;
    public VRTK_ControllerHaptics controllerHaptics;
    
    public bool isPointing;
    private GameObject _pointedTarget;
    public string pointedTargetValue;
    public GameObject leftHandToolBox;
    public bool controlWithKey;
    private bool _init= false;
    
    // Start is called before the first frame update
    void Start()
    {
        if(!_init)
        {
            // if(tno.isMine){
            //     if(this.name.StartsWith("Left"))
            //         RFCMessenger.instance.leftHandControllerManager =this;
            //     else if (this.name.StartsWith("Right"))
            //         RFCMessenger.instance.rightHandControllerManager = this;
            // } 

            pointer = (pointer == null ? GetComponent<VRTK_DestinationMarker>() : pointer);

            if (pointer != null)
            {
                pointer.DestinationMarkerEnter += ObjectPointerEnter;
                pointer.DestinationMarkerHover += ObjectPointerHover;
                pointer.DestinationMarkerExit += ObjectPointerExit;
                pointer.DestinationMarkerSet += ObjectPointerSet;
            }
            else
            {
                VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTKExample_PointerObjectHighlighterActivator", "VRTK_DestinationMarker", "the Controller Alias"));
                return;
            }
            // Debug.Log(AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() + "    Showwwwwwwwwwwwwwwww");

            controllerEvents = (controllerEvents ==null? GetComponent<VRTK_ControllerEvents>() : controllerEvents);
            if( controllerEvents != null)
            {
                controllerEvents.TriggerPressed += new ControllerInteractionEventHandler(delegate{
                    // if(!ScenarioEventManager.instance.GetSwitchRoom().CheckExplosion()){
                    //     TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
                    //     if (player != null)
                    //     {
                    //         if (player.briefDoc.gameObject.activeInHierarchy)
                    //         {
                    //             BriefingNextPage();
                    //         }
                    //         else
                    //         {
                                if (isPointing)
                                {
                                    AudioManager.instance.PlayAudioClip("Com_Select");
                                    if(_pointedTarget.GetComponent<CollideEvent>() != null){
                                        pointedTargetValue = _pointedTarget.GetComponent<CollideEvent>().getActionValue();
                                        if(pointedTargetValue != null)
                                            RFCMessenger.instance.NextStepRFC(pointedTargetValue,_pointedTarget.name);
                                        else
                                            RFCMessenger.instance.NextStepRFC();
                                    }
                                    if(_pointedTarget.GetComponentInParent<Megger>()!= null){
                                        Debug.Log("???" + _pointedTarget.name);
                                        AudioManager.instance.PlayAudioClip("P3_Tester");
                                        ScenarioEventManager.instance.GetSwitchRoom().MeggerTesterTool.GetComponentInChildren<Megger>().CollideAction();
                                        ScenarioEventManager.instance.GetSwitchRoom().MeggerTesterTool.GetComponentInChildren<DCTester>().CollideAction();
                                    }
                                }
                    //         }
                    //     }                   
                    // }
                });

                controllerEvents.GripPressed += new ControllerInteractionEventHandler(delegate{
                     RFCMessenger.instance.NextStepRFC("DebugModeStart");
                     Debug.Log("Show next");
                });
                
                

                //simply left hand control
                if(this.name.StartsWith("Left"))
                {
                    controllerEvents.ButtonTwoPressed += new ControllerInteractionEventHandler(delegate{MenuButtonPress();});
                    controllerEvents.TouchpadPressed += new ControllerInteractionEventHandler(delegate{MenuButtonPress();});
                    // controllerEvents.TouchpadPressed += MenuButtonPress;
                    // controllerEvents.ButtonTwoPressed += new ControllerInteractionEventHandler(delegate{
                    //     TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();

                    //     if(player!= null && player.GetPlayerID()==1){
                    //         if(player.briefDoc.gameObject.activeInHierarchy){
                    //             player.briefDoc.CloseDoc();
                    //             player.CloseDocRFC();

                    //         }else if(!player.toolsBox.activeInHierarchy){
                    //             player.toolsBox.SetActive(true);
                    //             player.ToolsBoxSetActiveRFC(true);
                    //         }else{
                    //             player.toolsBox.SetActive(false);
                    //             player.ToolsBoxSetActiveRFC(false);
                    //         }
                    //     }
                    // });

                    // controllerEvents.TouchpadPressed += new ControllerInteractionEventHandler(delegate{
                    //     TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
                    //     if(controllerEvents.GetTouchpadAxis().x<-0.7){
                    //         player.briefDoc.SwitchPage(-1);
                    //         player.SwitchPageRFC(-1);
                    //     }
                    //     if(controllerEvents.GetTouchpadAxis().x>0.7){
                    //         player.briefDoc.SwitchPage(1);
                    //         player.SwitchPageRFC(1);
                    //     }
                    // });
                }


            }
            _init =true;
        }



    }


    public void ObjectPointerEnter(object sender,DestinationMarkerEventArgs e){
        if (AMVRNetworkManager.instance.GetCurrentPlayer() != null && AMVRNetworkManager.instance.GetCurrentPlayer().canPointerInteract)
        {
            isPointing = true;
            _pointedTarget = e.target.gameObject;

            if (e.target != null)
            {
                VRTK_ObjectTooltip_Raymond optionTile = e.target.GetComponent<VRTK_ObjectTooltip_Raymond>();
                if (optionTile != null)
                {
                    optionTile.hoverObject.SetActive(true);
                }
            }        
        }
    }    
    public void SendResult(){

    }
    public virtual void ObjectPointerHover(object sender,DestinationMarkerEventArgs e){
    }   
    // public virtual void ObjectGrabbed(object sender,)
    public virtual void ObjectPointerExit(object sender,DestinationMarkerEventArgs e){
        isPointing = false;
        _pointedTarget = null;

        if (e.target != null)
        {
            VRTK_ObjectTooltip_Raymond optionTile = e.target.GetComponent<VRTK_ObjectTooltip_Raymond>();
            if (optionTile != null)
            {
                optionTile.hoverObject.SetActive(false);
            }
        }        
    }    
    public virtual void ObjectPointerSet(object sender,DestinationMarkerEventArgs e){
    }
    // public virtual void DoTriggerEvent(objec)
    // void OnDestroy(){
    //     co
    // }

    public void MenuButtonPress()
    {
        TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
        if (player != null && player.canOpenToolBox)
        {
            if (player.GetPlayerID() == 1)
            {
                if (player.briefDoc.gameObject.activeInHierarchy)
                {
                    player.CloseDocRFC();
                }
                else if (player.reportPanel.gameObject.activeInHierarchy)
                {
                    player.CloseReportRFC();
                    player.ToolsBoxSetActiveRFC(true);

                }        
                else if (!player.toolsBox.activeInHierarchy)
                {
                    player.ToolsBoxSetActiveRFC(true);
                }
                else
                {
                    player.ToolsBoxSetActiveRFC(false);
                }
            }
            else if (player.GetPlayerID() == 2)
            {
                if (player.reportPanel.gameObject.activeInHierarchy)
                {
                    player.CloseReportRFC();
                }
                else
                {
                    player.OpenReportRFC();                    
                }
            }   
        } 
    }

    public void BriefingNextPage()
    {
        TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
        if (player != null)
        {
            if (player.briefDoc.gameObject.activeInHierarchy)
            {
                player.NextPageRFC();
            }
        }   
    }

    public void Vibrate(float duration = 1.0f)
    {
        Debug.Log("Vibrate");
        VRTK_ControllerReference controllerReference = VRTK_ControllerReference.GetControllerReference(gameObject);
        VRTK_ControllerHaptics.TriggerHapticPulse(controllerReference, 1, duration/7.0f, 1.0f/300);
    }

    // void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.Space))
    //     {
    //         Vibrate();
    //     }
    // }    
}
