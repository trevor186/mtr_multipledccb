using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.Controllables;
public class PlayerVRController : MonoBehaviour
{
    public VRTK_DestinationMarker pointer;
    public VRTK_ControllerEvents controllerEvents;
    public VRTK_ControllerHaptics controllerHaptics;
    
    public bool isPointing;
    
    private GameObject _pointedTarget;
    public string pointedTargetValue;
    public GameObject leftHandToolBox;
    public bool controlWithKey;
    private bool _init= false;
    
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("???? _init ? "  + _init);
        if(!_init)
        {
            // if(tno.isMine){
            //     if(this.name.StartsWith("Left"))
            //         RFCMessenger.instance.leftHandControllerManager =this;
            //     else if (this.name.StartsWith("Right"))
            //         RFCMessenger.instance.rightHandControllerManager = this;
            // } 

            pointer = (pointer == null ? GetComponent<VRTK_DestinationMarker>() : pointer);

            if (pointer != null)
            {
                pointer.DestinationMarkerEnter += ObjectPointerEnter;
                pointer.DestinationMarkerHover += ObjectPointerHover;
                pointer.DestinationMarkerExit += ObjectPointerExit;
                pointer.DestinationMarkerSet += ObjectPointerSet;
            }
            else
            {
                VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTKExample_PointerObjectHighlighterActivator", "VRTK_DestinationMarker", "the Controller Alias"));
                return;
            }
            // Debug.Log(AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() + "    Showwwwwwwwwwwwwwwww");
            controllerEvents = (controllerEvents ==null? GetComponent<VRTK_ControllerEvents>() : controllerEvents);
            if( controllerEvents != null)
            {
                controllerEvents.TriggerPressed += new ControllerInteractionEventHandler(delegate{
                     if (isPointing){
                        AudioManager.instance.PlayAudioClip("Com_Select",false);
                        if(_pointedTarget.GetComponent<CollideEvent>() != null){
                            pointedTargetValue = _pointedTarget.GetComponent<CollideEvent>().getActionValue();
                            MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(pointedTargetValue);
                        }else if(_pointedTarget.GetComponentInParent<EarthingTester>() != null && _pointedTarget.name =="EarthTesterButton"){
                            _pointedTarget.GetComponentInParent<EarthingTester>().OnTesterButtonPressed();
                        }else if (_pointedTarget.GetComponentInChildren<TeleportPointer>() != null){
                            _pointedTarget.GetComponentInChildren<TeleportPointer>().TeleportToPoint();
                        }
                    }else{
                        Debug.Log("trigger is pressed!");
                    }        
                });

                controllerEvents.GripPressed += new ControllerInteractionEventHandler(delegate{
                    //  RFCMessenger.instance.NextStepRFC("DebugModeStart");
                    //  Debug.Log("Show next");
                });
                controllerEvents.TriggerReleased += new ControllerInteractionEventHandler(delegate{
                    if (isPointing){
                        if(_pointedTarget.GetComponentInParent<EarthingTester>() != null && _pointedTarget.name =="EarthTesterButton"){
                            _pointedTarget.GetComponentInParent<EarthingTester>().OnTesterButtonReleased();
                        }
                    }
                });
                
                if(this.name.StartsWith("Right")){
                    controllerEvents.ButtonTwoPressed += new ControllerInteractionEventHandler(delegate{SoloModeSwitchPlayer();});
                }
                //simply left hand control
                if(this.name.StartsWith("Left"))
                {
                    // if(controllerModel.transform.childCount>0){
                    // }
                    controllerEvents.ButtonTwoPressed += new ControllerInteractionEventHandler(delegate{SoloModeNextStep();});
                    // controllerEvents.TouchpadPressed += new ControllerInteractionEventHandler(delegate{TestNextStep();});
                    // controllerEvents.TouchpadPressed += MenuButtonPress;
                    // controllerEvents.ButtonTwoPressed += new ControllerInteractionEventHandler(delegate{
                    // });

                    // controllerEvents.TouchpadPressed += new ControllerInteractionEventHandler(delegate{
                    // });
                }
                
            }
            _init =true;
            Debug.Log("Test add controller");
            if(this.name.StartsWith("Right")){
                MultiDCCBSceneHandlingManager.instance.rightHandController = this;
            }
            if(this.name.StartsWith("Left")){
                MultiDCCBSceneHandlingManager.instance.leftHandController = this;
            }
        }
    }


    public void ObjectPointerEnter(object sender,DestinationMarkerEventArgs e){
        // if (AMVRNetworkManager.instance.GetCurrentMTRPlayer() != null && AMVRNetworkManager.instance.GetCurrentMTRPlayer().canPointerInteract)
        if (AMVRNetworkManager.instance.GetCurrentMTRPlayer() != null )
        {
            isPointing = true;
            _pointedTarget = e.target.gameObject;
            if (e.target != null)
            {
                VRTK_ObjectTooltip_Raymond optionTile = e.target.GetComponent<VRTK_ObjectTooltip_Raymond>();
                if (optionTile != null)
                {
                    optionTile.hoverObject.SetActive(true);
                }
            }        
        }
    }    
    public void SendResult(){

    }
    public virtual void ObjectPointerHover(object sender,DestinationMarkerEventArgs e){
    }   
    // public virtual void ObjectGrabbed(object sender,)
    public virtual void ObjectPointerExit(object sender,DestinationMarkerEventArgs e){
        isPointing = false;
        _pointedTarget = null;

        if (e.target != null)
        {
            VRTK_ObjectTooltip_Raymond optionTile = e.target.GetComponent<VRTK_ObjectTooltip_Raymond>();
            if (optionTile != null)
            {
                optionTile.hoverObject.SetActive(false);
            }
        }        
    }    
    public virtual void ObjectPointerSet(object sender,DestinationMarkerEventArgs e){
    }
    // public virtual void DoTriggerEvent(objec)
    // void OnDestroy(){
    //     co
    // }

    public void MenuButtonPress()
    {

        Debug.Log("Pressed");
        MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).SwapPlayerID();
        MultiDCCBSceneHandlingManager.instance.SceneReadStepInstructions(false,null,true);
        // TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
        // if (player != null && player.canOpenToolBox)
        // {
        //     if (player.GetPlayerID() == 1)
        //     {
        //         if (player.briefDoc.gameObject.activeInHierarchy)
        //         {
        //             player.CloseDocRFC();
        //         }
        //         else if (player.reportPanel.gameObject.activeInHierarchy)
        //         {
        //             player.CloseReportRFC();
        //             player.ToolsBoxSetActiveRFC(true);

        //         }        
        //         else if (!player.toolsBox.activeInHierarchy)
        //         {
        //             player.ToolsBoxSetActiveRFC(true);
        //         }
        //         else
        //         {
        //             player.ToolsBoxSetActiveRFC(false);
        //         }
        //     }
        //     else if (player.GetPlayerID() == 2)
        //     {
        //         if (player.reportPanel.gameObject.activeInHierarchy)
        //         {
        //             player.CloseReportRFC();
        //         }
        //         else
        //         {
        //             player.OpenReportRFC();                    
        //         }
        //     }   
        // } 
    }
    public void SoloModeNextStep(){
        if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() ==1){
            // MultiDCCBSceneRFCMessenger.instance.TestNextStepRFC();
            MultiDCCBSceneHandlingManager.instance.SceneSkipStep();
        }
    }
    public void SoloModeSwitchPlayer(){
        MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).SwapPlayerID();
        CollideEventObjectManager.instance.ChangeColliderList();
    }
    public void BriefingNextPage()
    {
        // TNetPlayer player = AMVRNetworkManager.instance.GetCurrentPlayer();
        // if (player != null)
        // {
        //     if (player.briefDoc.gameObject.activeInHierarchy)
        //     {
        //         player.NextPageRFC();
        //     }
        // }   
    }

    public void Vibrate(float duration = 1.0f)
    {
        Debug.Log("Vibrate");
        VRTK_ControllerReference controllerReference = VRTK_ControllerReference.GetControllerReference(gameObject);
        VRTK_ControllerHaptics.TriggerHapticPulse(controllerReference, 1, duration/7.0f, 1.0f/300);
    }

    void Update()
    {
        
        if(this.name.StartsWith("Left"))
        {  
            bool controllerBool = (controllerEvents.transform.parent.localRotation.x< -0.50f) && (controllerEvents.transform.parent.localRotation.x>-0.87f);
            // if(controllerBool && 
            //     MultiDCCBSceneHandlingManager.instance.player1Controller.earthingTester.gameObject.activeInHierarchy){
            // }
            // bool openRod =  && 
            
            if( MultiDCCBSceneHandlingManager.instance.currentSceneIndex ==2){
                if(MultiDCCBScene2EventManager.instance.sceneSection == null) return;
                if(MultiDCCBScene2EventManager.instance.sceneSection =="Scene2A" &&
                    MultiDCCBSceneHandlingManager.instance.player1Controller.earthingTester.gameObject.activeInHierarchy)
                    MultiDCCBSceneHandlingManager.instance.player1Controller.earthingTester.extendRod(controllerBool);
                else
                    MultiDCCBSceneHandlingManager.instance.player1Controller.earthingTester.extendRod(false);
            }      
        }
        // if (Input.GetKeyDown(KeyCode.Space))
        // {
        //     Vibrate();
        // }
    }    


}
