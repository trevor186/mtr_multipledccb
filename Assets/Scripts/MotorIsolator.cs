﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class MotorIsolator : MonoBehaviour
{
    public Animator animator;
    public List<GameObject> colliderObjects = new List<GameObject>();
    public List<Animator> animatorList = new List<Animator>();
    public List<VRTK_ObjectTooltip_Raymond> TooltipList = new List<VRTK_ObjectTooltip_Raymond>();
    public List<GameObject> FingerProcedureObjects = new List<GameObject>();
    public List<GameObject> playerOneTooltip = new List<GameObject>();
    public List<GameObject> playerTwoTooltip = new List<GameObject>();

    void Start(){
        if(AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() ==1){
            for(int i = 0; i< TooltipList.Count; i++){
                int ii = i;
                TooltipList[ii].hoverObject = playerOneTooltip[ii];
            }
        }else if (AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() ==2){
            for(int i = 0; i< TooltipList.Count; i++){
                int ii = i;
                TooltipList[ii].hoverObject = playerTwoTooltip[ii];
            }
        }
    }


}
