﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using VRTK;

public class NoticeSnapEvent : AbstructNoticeSnapEvent
{
    public override void Start()
    {
        target = transform.Find("HighlightContainer/HighlightObject");
        
        snapDropZone.ObjectSnappedToDropZone += new SnapDropZoneEventHandler(delegate{
            Debug.Log("NoticeSnapEvent Snapped");
            this.GetComponent<BoxCollider>().enabled =false;
            RFCMessenger.instance.NextStepRFC("PutUpNotice",this.name +"_"+snapDropZone.GetCurrentSnappedObject().name);
            AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();
        });
    }
    public override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Notice")
        {
            // {
            //     this.GetComponent<BoxCollider>().enabled =false;
            //     GameObject clone = Instantiate(other.gameObject);
            //     clone.GetComponent<BoxCollider>().enabled = false;
            //     // clone.SetActive(false);
            //     snapDropZone.ForceSnap(clone);

            //     {
            //         string path = "Prefabs/" + other.name;
            //         //TNManager.Instantiate(AMVRNetworkManager.instance.channelID, "NoticeSnapCreate", path, false, target.position, target.rotation);
            //         // tno.Send("NoticeSnapCreate_RFC", Target.Others, path, target.position, target.rotation);
            //         AMVRNetworkManager.instance.GetCurrentPlayer().SnapObjectCreateRFC(path, target.position, target.rotation);

            //         AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();
            //     }
            //     else
            //     {
            //         RFCMessenger.instance.NextStepRFC("PutUpNotice",other.name);
            //     }
            //     if(GlobalConfig.instance.GetNetworkID() == 2){
            //         // clone.gameObject.SetActive(false);
            //         other.gameObject.SetActive(false);
            //     }
            // // RFCMessenger.instance.NextStepRFC("PutUpNotice",other.name);
            // }

            // RFCMessenger.instance.NextStepRFC("PutUpNotice",other.name);
            // if (GlobalConfig.instance.GetNetworkID() == 1){
            //     SnapNoticeRFC(other.name);
            // }
        }
    }
    // public void SnapNoticeRFC(string name){
    //     tno.Send("SnapNotice",Target.All,name);
    // }
    // [RFC] void SnapNotice(string name){
    //     SnapToHolder(name);
    // }

    public override void SnapToHolder(string name ){
        foreach(GameObject notice in noticeList){
            if(name == notice.name){
                GameObject clone;
                clone = GameObject.Instantiate(notice);
                clone.GetComponent<BoxCollider>().enabled = false;
                clone.SetActive(true);
                // if(this.name =="Collider_NoticeHanger2"){
                //     Debug.Log("Change this ");
                //     clone.transform.localPosition = new Vector3(0,0,1);
                //     clone.transform.localRotation = Quaternion.Euler(0,3,0);
                // }
                snapDropZone.ForceSnap(clone);
                snappedObject = clone;
            }
        }
    }
    public override void DestroySnappedNotice(){
        if(snappedObject != null){
            Destroy(snappedObject);
            this.GetComponent<BoxCollider>().enabled =true;
        }
    }
}
