﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InspcectorCameraManager : MonoBehaviour
{
    public static InspcectorCameraManager instance;
    public List<GameObject> cameraList = new List<GameObject>();
    // Start is called before the first frame update

    void Awake(){
        if( instance == null){
            instance = this;
        }else if(instance != this){
            Destroy(gameObject);
        }
    }
    public void LoadCamera(int index){
        foreach(GameObject obj in cameraList){
            obj.SetActive(false);
        }
        cameraList[index].SetActive(true);
    }
}
