
using System.Collections.Generic;
using UnityEngine;

public class SnapEventObjectManager : MonoBehaviour
{
    public  static SnapEventObjectManager instance;
    public List<SnapEvent> snapEventList = new List<SnapEvent>();
    // Start is called before the first frame update
    void Awake(){
        if(instance == null){
            instance = this;
        }else if(instance != this){
            Destroy(gameObject);
        }
    }
    public void ShowSnapEvent(string name,bool activeStatus){
        GameObject obj = snapEventList.Find(p => p.eventName == name).gameObject;
        if(obj != null){
            obj.SetActive(activeStatus);
        }
    }
    public void ActionCall (string actionEvent){
    //    sceneManager.GetComponent<ShuntingOperationSceneManager>().ActionCallResult(actionEvent);
    }
}
