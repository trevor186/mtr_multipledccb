using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioSource audioSource;
    public List<AudioClip> audioList = new List<AudioClip>();
    protected void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void PlayAudioClip(string clipToPlay, bool stopBool = true)
        {
            if(stopBool)
                audioSource.Stop();
            foreach (AudioClip clip in audioList)
            {
                if(clip.name == clipToPlay)
                    audioSource.PlayOneShot(clip);
            }
            // audioList.Find(x=>(x.name == clipToPlay));
        }
    public void StopAudio(){
        audioSource.Stop();
    }

}
