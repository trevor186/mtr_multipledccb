﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncorrectStepHandleManager : MonoBehaviour
{
    public static IncorrectStepHandleManager instance;
    public List<string> inputList = new List<string>();
    public GameObject smokePrefab;
    // Start is called before the first frame update
    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StoreAction(string actionValue){
        Debug.Log("Add" + actionValue + " ???????????????????????");
        inputList.Add(actionValue);
    }
    public bool CheckAction(string[] actionList){
        bool result = true;

        foreach(string action in actionList){
            if(inputList.Contains(action)){
                // inputList.Remove(action);
            }else{
                // inputList.Clear();
                return false;
            }
        }
        return result;
    }

    public void CreateSmokeError(string colliderName){
        Debug.Log("trigger???????????????????????????");
        Transform colliderTransform = CollideEventObjectManager.instance.colliderList.Find(target =>target.actionValue == colliderName).transform;
        GameObject smoke = GameObject.Instantiate(smokePrefab,Vector3.zero,Quaternion.identity,colliderTransform);
        smoke.transform.localPosition = Vector3.zero;
        smoke.SetActive(true);
        ReliableDelayedInvokation.AddInvokation(smoke,delegate{
            Destroy(smoke.gameObject);
        },2f);
    }
    public void CreateSmokeError(Transform errorTransform){
        GameObject smoke = GameObject.Instantiate(smokePrefab,Vector3.zero,Quaternion.identity,errorTransform);
        smoke.transform.localPosition = Vector3.zero;
        smoke.SetActive(true);
        ReliableDelayedInvokation.AddInvokation(smoke,delegate{
            Destroy(smoke.gameObject);
        },2f);
    }
}
