﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportManager : MonoBehaviour
{
    public Transform playerHeadset;
    public Transform cameraEyeTransform;
    public Transform steamVRSetup;
    public GameObject teleportPosistionReference;
    public GameObject teleportPointReference;
    public List<TeleportPointer> pointerList = new List<TeleportPointer>();
    public List<TeleportPointer> currentPointerList = new List<TeleportPointer>();
    public List<Transform> playerTeleportPositionList = new List<Transform>();
    public int currentTeleportPointIndex;
    #region Singleton
    public static TeleportManager instance = null;
    public MTRTeleportArea playerTeleportArea;
    public bool playerOneEnterTeleportArea;
    public bool playerTwoEnterTeleportArea;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        foreach(TeleportPointer pointer in teleportPointReference.GetComponentsInChildren<TeleportPointer>(true)){
            pointerList.Add(pointer);
        }
    
        // foreach(Transform t in teleportPosistionReference.GetComponentsInChildren<Transform>(true)){
        //   
        // }
    }
    public void Teleport(){
        playerTeleportArea.gameObject.SetActive(false);
        Teleport(playerTeleportPositionList[currentTeleportPointIndex]);
    }
     #endregion 
    public void Teleport(Transform teleportPosition){
        playerHeadset.transform.SetParent(teleportPosition);
        playerHeadset.transform.localPosition = Vector3.zero;
        playerHeadset.localRotation = Quaternion.Euler(Vector3.zero);
        playerHeadset.transform.SetParent(steamVRSetup);
        playerHeadset.transform.localScale = Vector3.one;
    }

    public void PointerTeleport(TeleportPointer pointer){
        TeleportManager.instance.Teleport(pointer.transform);
        foreach(TeleportPointer obj in currentPointerList){
            obj.gameObject.SetActive(true);
        }
        pointer.gameObject.SetActive(false);
    }
    public void PointerTeleport(TeleportPointer pointer,string[] validPointerList){
        TeleportManager.instance.Teleport(pointer.transform);

        foreach(string name in validPointerList){
            foreach(TeleportPointer obj in pointerList){
            obj.gameObject.SetActive(true);
            }
        }
        pointer.gameObject.SetActive(false);
    }
    // public void Teleport(Transform teleportPosition , Vector3 rotationVector){
    //     playerHeadset.transform.SetParent(teleportPosition);
    //     playerHeadset.transform.localPosition = Vector3.zero;
    //     playerHeadset.localRotation = Quaternion.Euler(rotationVector);
    //     playerHeadset.transform.SetParent(steamVRSetup);
    //     playerHeadset.transform.localScale = Vector3.one;
    // }
    public void SetPointer(string pointerName,bool value){
        foreach(TeleportPointer obj in currentPointerList){
            Debug.Log("try load" + obj.name);
            Debug.Log("target open " + pointerName);
            if(obj.name == pointerName){
                obj.gameObject.SetActive(value);
            }
        }
    }
    // public void SetPointer(string[] pointerNameList,bool value){
    //     foreach(string name in currentPointerList){
    //         foreach(TeleportPointer obj in pointerList){

    //         }
    //     }
    // }

    public void CloseAllPointers(){
        foreach(TeleportPointer obj in currentPointerList){
            obj.gameObject.SetActive(false);
        }
    }

    public void SetCurrentPointerList(string[] pointerNameList,bool openPointer = false){
        Debug.Log("set teleporter");
        for(int i =0; i < currentPointerList.Count; i++){
            currentPointerList[i].gameObject.SetActive(false);
        }
        currentPointerList.Clear();
        for(int i =0; i < pointerNameList.Length; i++){
            int ii = i;
            Debug.Log("set pointer" + pointerNameList[ii]);
            foreach(TeleportPointer obj in pointerList){
                if(obj.name == pointerNameList[ii]){
                    Debug.Log("add obj?     "+ obj.name);
                    currentPointerList.Add(obj);
                    if(openPointer)
                        obj.gameObject.SetActive(true);                
                }
            }
        }    
    }
    public void EnableTeleportArea(bool value, int index =0){
        if(playerTeleportArea.animatiorObject != null) playerTeleportArea.animatiorObject.SetActive(value);
        playerTeleportArea.gameObject.SetActive(value);
        currentTeleportPointIndex = index;
    }
    // public void PlayerEnteredTeleportArea(int playerID){
    //     if(playerID == 1){
    //         playerOneEnterTeleportArea = true;
    //     }else{
    //         playerTwoEnterTeleportArea = true; 
    //     }
    //     if(playerOneEnterTeleportArea && playerTwoEnterTeleportArea){
    //         playerTeleportArea.DoTeleport();
    //     }
    // }
}
