using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideEventObjectManager : MonoBehaviour
{
    public static CollideEventObjectManager instance;
    public GameObject scene ;
    public List<CollideEvent> colliderList = new List<CollideEvent>();
    [SerializeField]
    private List<CollideEvent> colliderForPlayer1List = new List<CollideEvent>();
    [SerializeField]
    private List<CollideEvent> colliderForPlayer2List = new List<CollideEvent>();

    void Awake(){
       if(instance == null){
           instance = this;
       }else if(instance != this){
           Destroy(gameObject);
       }

        foreach(CollideEvent obj in scene.GetComponentsInChildren<CollideEvent>(true)){
            colliderList.Add(obj);
        }
    }
    public void SetColliderObjectActive(string value,int playerID, bool activeStatus){
        // GameObject colliderObj = colliderList.Find(p=> p.actionValue == obj && p.playerObject == TN).gameObject;
        GameObject colliderObj = null;
        foreach(CollideEvent obj in colliderList){
            if(obj.actionValue == value && obj.objectPlayerID == playerID){
                colliderObj = obj.gameObject;
                break;
            }
        }
        if(colliderObj != null){
            if(colliderObj.name.StartsWith("collider"))
                colliderObj.SetActive(true);
            colliderObj.GetComponent<BoxCollider>().enabled = activeStatus;

            
        }
    }
    public void AddToColliderStackList(int playerID, string actionName,bool value){
        List<CollideEvent> targetList = playerID == 1? colliderForPlayer1List : colliderForPlayer2List;
        CollideEvent colliderObj = null;
        foreach(CollideEvent obj in colliderList){
            if(obj.actionValue == actionName && obj.objectPlayerID == playerID){
                colliderObj = obj;
                break;
            }
        }
        if( colliderObj != null){
            if(value){
                targetList.Add(colliderObj);
            }else{
                targetList.RemoveAll( x => x.actionValue == colliderObj.actionValue);
            }
        }
    }
    public void ChangeColliderList(){
        // int currentPlayerID = AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID();
        bool isPlayerOneBool = AMVRNetworkManager.instance.GetCurrentMTRPlayer().IsPlayerOne();
        foreach(CollideEvent obj in colliderForPlayer1List){
            obj.GetComponent<BoxCollider>().enabled = isPlayerOneBool;
        }
        foreach(CollideEvent obj in colliderForPlayer2List){
            obj.GetComponent<BoxCollider>().enabled = !isPlayerOneBool;    
        }
    }
    public void SetColliderObjectActive(string value,bool activeStatus){
        // GameObject colliderObj = colliderList.Find(p=> p.actionValue == obj && p.playerObject == TN).gameObject;
        GameObject colliderObj = null;
        foreach(CollideEvent obj in colliderList){
            colliderObj = obj.gameObject;
            break;
        }
        if(colliderObj != null){
            if(colliderObj.name.StartsWith("collider"))
                colliderObj.SetActive(true);
            colliderObj.GetComponent<BoxCollider>().enabled = activeStatus;
        }
    }
    public void HideCollideObject(string obj){
        GameObject colliderObj = colliderList.Find(p=> p.actionValue == obj).gameObject;
        if(colliderObj != null){
            colliderObj.SetActive(false);
        }

    }
    public GameObject GetColliderObject(string obj){ // use to get the object of the collider for special event
        return colliderList.Find(p=> p.actionValue == obj).gameObject;
    }
    public void RFCCallFingering(string value){
        foreach(CollideEvent obj in colliderList){
            if(obj.actionValue == value){
                obj.RFCFingering();
            }
        }
    }
}
