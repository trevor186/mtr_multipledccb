﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotManager : MonoBehaviour
{    
    public Camera targetCam;
    public RenderTexture screenTexture;
    public static ScreenshotManager instance;
    public int resWidth = 1920; 
    public int resHeight = 1080;
    private bool takeHiResShot = false;

    void Awake(){
        if(instance == null){
            instance = this;
        }else if (instance != this){
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    public static string ScreenShotName(){

        string folderPath = "Screenshots/";
        if (!System.IO.Directory.Exists(folderPath))
            System.IO.Directory.CreateDirectory(folderPath);
        string fileName =  "Screenshot_" + System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".png";
        return fileName;
    }
    public void TakeScreenshot(){
        takeHiResShot = true;
    }
    void LateUpdate() {
        // RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        // targetCam.targetTexture = screenTexture;
        if(takeHiResShot){
            Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            targetCam.Render();
            RenderTexture.active = screenTexture;
            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            // targetCam.targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            // Destroy(rt);
            byte[] bytes = screenShot.EncodeToPNG();
            string filename = ScreenShotName();
            System.IO.File.WriteAllBytes(filename, bytes);
            Debug.Log(string.Format("Took screenshot to: {0}", filename));
            takeHiResShot = false;
        }
    }
}
