﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class AnimationManager : MonoBehaviour
{
    public  static AnimationManager instance;
    public GameObject scene;
    public List<GameObject> animatorObjList = new List<GameObject>();
    public List<Animator> testAnimatorList = new List<Animator>();
    private bool animationPlaying ;

    // Start is called before the first frame update
    void Awake(){
        if(instance == null){
            instance = this;
        }else if(instance != this){
            Destroy(gameObject);
        }

        if(scene.activeInHierarchy){
            foreach(Animator obj in scene.GetComponentsInChildren<Animator>(true)){
                if(obj.name != "fingering")
                    animatorObjList.Add(obj.gameObject);
            }
        }

    }
    // Start is called before the first frame update
    // public void RunAnimation(string animatorName, string animation){
    //     GameObject obj = animatorObjList.Find(T => T.name == animatorName);
    //     Animator objAnimator = obj.GetComponentInChildren<Animator>();
    //     objAnimator.SetTrigger(animation);
    // }
    public void RunAnimationTrigger(string animation, string animatorName){
        Debug.Log("Get? " + animation + " in the name " + animatorName);
        GameObject obj = animatorObjList.Find(T => T.name == animatorName); 
        Animator objAnimator = obj.GetComponentInChildren<Animator>();
        // if(objAnimator == null) objAnimator = obj.GetComponent<Animator>();
        List<AnimatorControllerParameter> triggerList = new List<AnimatorControllerParameter>();
        List<string> triggerNameList = new List<string>();  // Ensure Animation is in the animator
        for(int i = 0; i< objAnimator.parameterCount ; i++){
            int ii=i;
            // triggerNameList.Add(objAnimator.GetParameter(ii).name);
            if(animation == objAnimator.GetParameter(ii).name){
                objAnimator.SetTrigger(animation);
            }
        }
    }
    public void RunAnimationBool(string animation,string animatorName, bool boolValue){   
        Debug.Log("Get? " + animation + " in the name " + animatorName);

        GameObject obj = animatorObjList.Find(T => T.name == animatorName); 
        Animator objAnimator = obj.GetComponentInChildren<Animator>();
        List<AnimatorControllerParameter> triggerList = new List<AnimatorControllerParameter>();
        List<string> triggerNameList = new List<string>();  // Ensure Animation is in the animator
        for(int i = 0; i< objAnimator.parameterCount ; i++){
            int ii=i;
            // triggerNameList.Add(objAnimator.GetParameter(ii).name);
            if(animation == objAnimator.GetParameter(ii).name){
                objAnimator.SetBool(animation,boolValue);
            }
        }
    }
    public bool GetAnimationBool(string animation,string animatorName){
         GameObject obj = animatorObjList.Find(T => T.name == animatorName); 
        Animator objAnimator = obj.GetComponentInChildren<Animator>();
        List<AnimatorControllerParameter> triggerList = new List<AnimatorControllerParameter>();
        List<string> triggerNameList = new List<string>();  // Ensure Animation is in the animator
        for(int i = 0; i< objAnimator.parameterCount ; i++){
            int ii=i;
            // triggerNameList.Add(objAnimator.GetParameter(ii).name);
            if(animation == objAnimator.GetParameter(ii).name){
                // objAnimator.SetBool(animation,boolValue);
                return objAnimator.GetBool(animation);
            }
        }
        return false;
    }
    public void ResetAniamtion(string animatorName){
         GameObject obj = animatorObjList.Find(T => T.name == animatorName);
        Animator objAnimator = obj.GetComponentInChildren<Animator>();
        objAnimator.SetTrigger("AnyState");
    }
    public IEnumerator SetPlayingAnimation(int value = 5){
        animationPlaying = true;
        yield return new WaitForSeconds(value);
        animationPlaying  = false;
    }
    public bool CheckPlayingAnimation(){
        Debug.Log("ANIMATION PLAYING??   " + animationPlaying);
        return animationPlaying;
    }
}
