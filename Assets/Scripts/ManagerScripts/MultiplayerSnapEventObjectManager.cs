﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class MultiplayerSnapEventObjectManager : MonoBehaviour
{
    public  static MultiplayerSnapEventObjectManager instance;
    public List<AbstructNoticeSnapEvent> snapEventList = new List<AbstructNoticeSnapEvent>();
// Start is called before the first frame update
    void Awake(){
        if(instance == null){
            instance = this;
        }else if(instance != this){
            Destroy(gameObject);
        }
    }
    public void ShowSnapEvent(string name,bool activeStatus){
        GameObject obj = snapEventList.Find(p => p.noticeName == name).gameObject;
        if(obj != null){
            obj.SetActive(activeStatus);
        }
    }
    public void ActionCall (string actionEvent){
    //    sceneManager.GetComponent<ShuntingOperationSceneManager>().ActionCallResult(actionEvent);
    }

    public void EnableSnapHighlight(bool value){
        foreach(AbstructNoticeSnapEvent snapEvent in snapEventList){
            VRTK_SnapDropZone snapdropZone = snapEvent.GetComponent<VRTK_SnapDropZone>();
            snapdropZone.highlightAlwaysActive = value;
        }
    }
    public void ForceSnap(string name){
        Debug.Log("Check name" + name);
        AbstructNoticeSnapEvent obj = snapEventList.Find(p => p.noticeName == name);
        if(obj != null){
            Debug.Log("Check thisssssssssssssssssssss");
            obj.CheckSnapObject();
        }
    }
}
