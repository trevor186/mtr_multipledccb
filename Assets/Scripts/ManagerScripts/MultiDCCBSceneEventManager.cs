﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using UnityEngine.SceneManagement;
using TNet;

public class MultiDCCBSceneEventManager : MonoBehaviour
{
    public static MultiDCCBSceneEventManager instance;
    public enum Cabinate25kProcedure{
        WAITING_ROOM,
        // INTRODUCTION,
        START_BRIEFING,
        FINGERING_IDENTIDY_CIRCUIT,  //2.1
        FINGERING_VOLTAGE_INDICATOR_0911,
        SETUP_BARRIER,  //2.2
        APPLY_DANGER_NOTICE1_HA_09,
        APPLY_DANGER_NOTICE1_HA_11,  //2.3
        SWITCH25K_PROCEDURE,
        FINGERING_CONTROL_PANEL, //2.8
        OPEN_CONTROL_PANEL,    //2.9
        CONTROL_PANEL_PROCEDURE,
        FINGERING_AC_SWITCHGEAR,
        FINGERING_AC_VLOTAGE_INDICATOR_0103,
        SETUP_BARRIER2,
        APPLY_DANGER_NOTICE2_HA_01,
        APPLY_DANGER_NOTICE2_HA_03,
        AC_SWITCHGEAR_PROCEDURE,
        CALL_PSC3, // 5.1,5.2 Back to switch room
        CHECK_TESTER,
        APPLY_TO_CIRCUIT,
        CHECK_TESTER2,
        CLOSE_EARTH_SWITCH_PROCEDURE,
        CALL_PSC4,
        APPLY_PADLOCK2,
        APPLY_SAFETY_DOCUMENT,
        FINISH

    }
    public Cabinate25kProcedure current25kProcedure = Cabinate25kProcedure.WAITING_ROOM;
    public GameObject mainCanvas;
    public GameObject waitingRoom;
    public GameObject dccbRoom;
    public DCCBSwitch25kvRoom switch25kvGearRoom;
    // public SwitchCabinate currentCabinate;
    public System.Collections.Generic.List<Switch25kVCabinate> switch25kVCabinateList = new System.Collections.Generic.List<Switch25kVCabinate>();
    public Switch25kVControlPanel switch25KVControlPanel ;
    public System.Collections.Generic.List<Transform> player1AreaTransform = new System.Collections.Generic.List<Transform>();
    public MultiDCCBSceneRFCMessenger rfcMessenger ;
    public string sceneSection;
    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        TeleportManager.instance.Teleport(player1AreaTransform[8]);
        sceneSection =(string) TNManager.GetPlayerData("SceneParameter").value;
       
        // currentCabinate = switch25kvGear.cabinates25k[3];
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.S)){
            MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).SwapPlayerID();
            CollideEventObjectManager.instance.ChangeColliderList();
            // StartCoroutine(ReadStepInstructions(false,null,true));
        }   
        if(Input.GetKeyDown(KeyCode.Q)){
            SkipStep();
        }
        // if(Input.GetKeyDown(KeyCode.R)){
        //     rfcMessenger.ReloadSceneRFC("MTR_MultiDCCB_Trevor");
        // }
        if(Input.GetKeyDown(KeyCode.Z)){
            TeleportManager.instance.Teleport(player1AreaTransform[0]);
        }
        if(Input.GetKeyDown(KeyCode.X)){
            TeleportManager.instance.Teleport(player1AreaTransform[1]);
        }
        
        if(Input.GetKeyDown(KeyCode.C)){
            TeleportManager.instance.Teleport(player1AreaTransform[2]);
        }        
        if(Input.GetKeyDown(KeyCode.V)){
            TeleportManager.instance.Teleport(player1AreaTransform[3]);
        }        
        if(Input.GetKeyDown(KeyCode.B)){
            TeleportManager.instance.Teleport(player1AreaTransform[4]);
        }        
        if(Input.GetKeyDown(KeyCode.B)){
            TeleportManager.instance.Teleport(player1AreaTransform[5]);
        }        
        if(Input.GetKeyDown(KeyCode.N)){
            TeleportManager.instance.Teleport(player1AreaTransform[6]);
        }
        if(Input.GetKeyDown(KeyCode.N)){
            TeleportManager.instance.Teleport(player1AreaTransform[7]);
        }
        if(Input.GetKeyDown(KeyCode.Return)){
            if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() ==2){
                rfcMessenger.NextStepRFC();
            }
        }
    }
    public void InitSetup(int number,bool obj2, bool obj3, bool obj4)
    {
    }
    public void ActionCallResult(string action,string extraInfo =null ){
        Debug.Log("get      answer??/    " + action);
        if(action.StartsWith("UI_")){
            MultiDCCBSceneHandlingManager.instance.UIPanelActionCall(action.Replace("UI_",""));
        }
        
        switch(current25kProcedure){
        // case(Cabinate25kProcedure.INTRODUCTION):
        //         break;
            case(Cabinate25kProcedure.START_BRIEFING):
                rfcMessenger.PlayAnimationBoolRFC("Light_on","503A_EarthSwitchOpen_light",true);
                rfcMessenger.PlayAnimationBoolRFC("Light_on","504A_EarthSwitchOpen_light",true);
                break;
            case(Cabinate25kProcedure.FINGERING_IDENTIDY_CIRCUIT):
                if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    if(action =="HA_09"){
                        rfcMessenger.SetCollideEventRFC("HA_09",1,false);
                        rfcMessenger.SetCollideEventRFC("HA_09",2,true);
                    }
                    if(action =="HA_11"){
                        rfcMessenger.SetCollideEventRFC("HA_11",1 ,false);
                        rfcMessenger.SetCollideEventRFC("HA_11",2 ,true);
                    } 
                }else if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                    if(action == "HA_09"){
                        rfcMessenger.SetCollideEventRFC("HA_09",2 ,false);
                        switch25kVCabinateList[0].currentPart1Procedure = Switch25kVCabinate.Part1Procedure.FINGERING_SELECT;
                    }    
                    if(action =="HA_11"){
                        rfcMessenger.SetCollideEventRFC("HA_11",2 ,false);
                        switch25kVCabinateList[1].currentPart1Procedure = Switch25kVCabinate.Part1Procedure.FINGERING_SELECT;
                    }
                    if(switch25kVCabinateList[0].currentPart1Procedure == Switch25kVCabinate.Part1Procedure.FINGERING_SELECT&&
                        switch25kVCabinateList[1].currentPart1Procedure == Switch25kVCabinate.Part1Procedure.FINGERING_SELECT){
                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                }

                break;
            case(Cabinate25kProcedure.FINGERING_VOLTAGE_INDICATOR_0911):
                if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    if(action =="HA09_FingeringVoltageIndicator"){
                        rfcMessenger.SetCollideEventRFC("HA09_FingeringVoltageIndicator",1,false);
                        rfcMessenger.SetCollideEventRFC("HA09_FingeringVoltageIndicator",2,true);
                    }
                    if(action =="HA11_FingeringVoltageIndicator"){
                        rfcMessenger.SetCollideEventRFC("HA11_FingeringVoltageIndicator",1 ,false);
                        rfcMessenger.SetCollideEventRFC("HA11_FingeringVoltageIndicator",2 ,true);
                    } 
                }else if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                    if(action == "HA09_FingeringVoltageIndicator"){
                        rfcMessenger.SetCollideEventRFC("HA09_FingeringVoltageIndicator",2 ,false);
                        switch25kVCabinateList[0].currentPart1Procedure = Switch25kVCabinate.Part1Procedure.FINGERING_VOLTAGE_INDICATOR;
                    }    
                    if(action =="HA11_FingeringVoltageIndicator"){
                        rfcMessenger.SetCollideEventRFC("HA11_FingeringVoltageIndicator",2 ,false);
                        switch25kVCabinateList[1].currentPart1Procedure = Switch25kVCabinate.Part1Procedure.FINGERING_VOLTAGE_INDICATOR;
                    }
                    if(switch25kVCabinateList[0].currentPart1Procedure == Switch25kVCabinate.Part1Procedure.FINGERING_VOLTAGE_INDICATOR&&
                        switch25kVCabinateList[1].currentPart1Procedure == Switch25kVCabinate.Part1Procedure.FINGERING_VOLTAGE_INDICATOR){
                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                }
                break;
            case(Cabinate25kProcedure.SETUP_BARRIER):
                if(action == "SafetyBarrier1"){
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier1",1 ,false);
                    rfcMessenger.SetCollideEventRFC("FingeringSafetyBarrier1",2 ,true);
                    rfcMessenger.ReadStepInstructionsRFC(false,"OpenBarrier");
                }                
                if(action == "FingeringSafetyBarrier1"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Cabinate25kProcedure.APPLY_DANGER_NOTICE1_HA_09):
            case(Cabinate25kProcedure.APPLY_DANGER_NOTICE1_HA_11):

                if(action.Contains("HA_09_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }               
                if(action.Contains("HA_11_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;

            case(Cabinate25kProcedure.SWITCH25K_PROCEDURE):
                if(action.StartsWith("HA09")){
                    switch25kVCabinateList[0].CabinateActionCallResult(action.Replace("HA09_",""),"HA09");
                }                
                if(action.StartsWith("HA11")){
                    switch25kVCabinateList[1].CabinateActionCallResult(action.Replace("HA11_",""),"HA11");
                }
                if(action == "Phone"){
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                    switch25kVCabinateList[0].CabinateActionCallResult(action);
                    switch25kVCabinateList[1].CabinateActionCallResult(action);
                }
                
                break;
            case(Cabinate25kProcedure.FINGERING_CONTROL_PANEL):
                if(action.StartsWith("Fingering_Isolator") && AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    rfcMessenger.SetCollideEventRFC("Fingering_Isolator",1,false);
                    rfcMessenger.SetCollideEventRFC("Fingering_Isolator",2 ,true);

                }               
                if(action.StartsWith("Fingering_Isolator") && AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                    rfcMessenger.SetCollideEventRFC("Fingering_Isolator",2 ,false);
                }
                break;
            case(Cabinate25kProcedure.OPEN_CONTROL_PANEL):
                if(action.StartsWith("Open_Isolator")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                    rfcMessenger.SetCollideEventRFC("Open_Isolator",1 ,false);
                }
                break;
            case(Cabinate25kProcedure.CONTROL_PANEL_PROCEDURE):
                if(action.StartsWith("503A")|| action.StartsWith("504A")){
                    switch25KVControlPanel.ControlPanelActionCallResult(action.Substring(5),action.Substring(0,4));
                }
                break;
            case(Cabinate25kProcedure.FINGERING_AC_SWITCHGEAR):
                if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    if(action =="HA_01"){
                            rfcMessenger.SetCollideEventRFC("HA_01",1,false);
                            rfcMessenger.SetCollideEventRFC("HA_01",2,true);
                        }
                        if(action =="HA_03"){
                            rfcMessenger.SetCollideEventRFC("HA_03",1 ,false);
                            rfcMessenger.SetCollideEventRFC("HA_03",2 ,true);
                        } 
                }else if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        if(action == "HA_01"){
                            rfcMessenger.SetCollideEventRFC("HA_01",2 ,false);
                            switch25kVCabinateList[0].currentACProcedure = Switch25kVCabinate.ACProcedure.READY;
                        }    
                        if(action =="HA_03"){
                            rfcMessenger.SetCollideEventRFC("HA_03",2 ,false);
                            switch25kVCabinateList[1].currentACProcedure = Switch25kVCabinate.ACProcedure.READY;
                        }
                        if(switch25kVCabinateList[0].currentACProcedure == Switch25kVCabinate.ACProcedure.READY &&
                            switch25kVCabinateList[1].currentACProcedure == Switch25kVCabinate.ACProcedure.READY){
                            rfcMessenger.ReadStepInstructionsRFC(true);
                        }
                    }
                break;
            case(Cabinate25kProcedure.FINGERING_AC_VLOTAGE_INDICATOR_0103):
                if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    if(action =="HA01_FingeringVoltageIndicator"){
                            rfcMessenger.SetCollideEventRFC("HA01_FingeringVoltageIndicator",1,false);
                            rfcMessenger.SetCollideEventRFC("HA01_FingeringVoltageIndicator",2,true);
                        }
                        if(action =="HA03_FingeringVoltageIndicator"){
                            rfcMessenger.SetCollideEventRFC("HA03_FingeringVoltageIndicator",1 ,false);
                            rfcMessenger.SetCollideEventRFC("HA03_FingeringVoltageIndicator",2 ,true);
                        } 
                }else if( AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                        if(action == "HA01_FingeringVoltageIndicator"){
                            rfcMessenger.SetCollideEventRFC("HA01_FingeringVoltageIndicator",2 ,false);
                            switch25kVCabinateList[0].currentACProcedure = Switch25kVCabinate.ACProcedure.FINGERING_VOLTAGE_INDICATOR;
                        }    
                        if(action =="HA03_FingeringVoltageIndicator"){
                            rfcMessenger.SetCollideEventRFC("HA03_FingeringVoltageIndicator",2 ,false);
                            switch25kVCabinateList[1].currentACProcedure = Switch25kVCabinate.ACProcedure.FINGERING_VOLTAGE_INDICATOR;
                        }
                        if(switch25kVCabinateList[0].currentACProcedure == Switch25kVCabinate.ACProcedure.FINGERING_VOLTAGE_INDICATOR &&
                            switch25kVCabinateList[1].currentACProcedure == Switch25kVCabinate.ACProcedure.FINGERING_VOLTAGE_INDICATOR){
                            rfcMessenger.ReadStepInstructionsRFC(true);
                        }
                    }
                break;
            case(Cabinate25kProcedure.SETUP_BARRIER2):
                if(action == "SafetyBarrier2"){
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier2",1 ,false);
                    rfcMessenger.SetCollideEventRFC("FingeringSafetyBarrier2",2 ,true);
                    rfcMessenger.ReadStepInstructionsRFC(false,"OpenBarrier");
                }                
                if(action == "FingeringSafetyBarrier2"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Cabinate25kProcedure.APPLY_DANGER_NOTICE2_HA_01):
            case(Cabinate25kProcedure.APPLY_DANGER_NOTICE2_HA_03):

                if(action.Contains("HA_01_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }               
                if(action.Contains("HA_03_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Cabinate25kProcedure.AC_SWITCHGEAR_PROCEDURE):
                if(action.StartsWith("HA01")){
                    switch25kVCabinateList[2].ACActionCallResult(action.Replace("HA01_",""),"HA01");
                }                
                if(action.StartsWith("HA03")){
                    switch25kVCabinateList[3].ACActionCallResult(action.Replace("HA03_",""),"HA03");
                }
                if(action == "Phone"){
                    if(
                    //     (switch25kVCabinateList[2].currentACProcedure == Switch25kVCabinate.ACProcedure.CALL_PSC_1 &&
                    // switch25kVCabinateList[3].currentACProcedure == Switch25kVCabinate.ACProcedure.CALL_PSC_1) ||
                        (switch25kVCabinateList[2].currentACProcedure == Switch25kVCabinate.ACProcedure.CALL_PSC_2 &&
                    switch25kVCabinateList[3].currentACProcedure == Switch25kVCabinate.ACProcedure.CALL_PSC_2)
                    ){
                        AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                        rfcMessenger.ReadStepInstructionsRFC(false,"Call_PSC");
                    }   
   
                }
                break;
            case(Cabinate25kProcedure.CALL_PSC3):
                //Teleport first
                if(action =="Phone"){
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Cabinate25kProcedure.CHECK_TESTER):
            if(action =="CheckTester"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Cabinate25kProcedure.APPLY_TO_CIRCUIT):
                if(action.Contains("ApplyCircuit")){
                    if(action.Contains("504") && !switch25kVCabinateList[0].applyCircuitStatus){
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        switch25kVCabinateList[0].applyCircuitStatus = true;
                    }
                    if(action.Contains("503") && !switch25kVCabinateList[1].applyCircuitStatus){
                        AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                        switch25kVCabinateList[1].applyCircuitStatus = true;
                    }
                    if(switch25kVCabinateList[0].applyCircuitStatus &&
                        switch25kVCabinateList[1].applyCircuitStatus){
                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                }
                if(action.Contains("ErrorCircuit")){
                    Debug.Log("error");
                    // IncorrectStepHandleManager.instance.CreateSmokeError(action);

                }
                break;
            case(Cabinate25kProcedure.CHECK_TESTER2):
                if(action =="CheckTester"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Cabinate25kProcedure.CLOSE_EARTH_SWITCH_PROCEDURE):
                if(action.StartsWith("503A")|| action.StartsWith("504A")){
                    switch25KVControlPanel.ControlPanelActionCallResult(action.Substring(5),action.Substring(0,4));
                }
                break;
            case(Cabinate25kProcedure.CALL_PSC4):
                if(action =="Phone"){
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Cabinate25kProcedure.APPLY_PADLOCK2):
                break;
            case(Cabinate25kProcedure.APPLY_SAFETY_DOCUMENT):
                if(action=="SafetyDocument" && AMVRNetworkManager.instance.GetCurrentMTRPlayer().IsPlayerOne()){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;

            default:break;
        }
    }
    public IEnumerator ReadStepInstructions(bool result,string extraInfo = null,bool soloTest = false){
        // if(extraInfo =="Skip"){
        //     Debug.Log("wait././////////////////////////////");
        //     yield return new WaitForSeconds(1f);
        //     Debug.Log("Start loading!!!!!!!!!!!!!");
        // }
        if(result){
            current25kProcedure += 1 ;
            Debug.Log("Enter procedure " + current25kProcedure.ToString());
            switch(current25kProcedure){
                // case(Cabinate25kProcedure.INTRODUCTION):
                //     // StartCoroutine(ReadStepInstructions(true));
                //     break;
                case(Cabinate25kProcedure.START_BRIEFING):
                    TeleportManager.instance.Teleport(player1AreaTransform[8]);
                    waitingRoom.SetActive(false);
                    break;
                case(Cabinate25kProcedure.FINGERING_IDENTIDY_CIRCUIT):
                    if(sceneSection =="Scene1B"){
                        current25kProcedure = Cabinate25kProcedure.CONTROL_PANEL_PROCEDURE;
                        StartCoroutine(ReadStepInstructions(true,"Skip"));
                        rfcMessenger.PlayAnimationTriggerRFC("CB_call_psc","401_Button_CB_prefab");
                        rfcMessenger.PlayAnimationTriggerRFC("CB_call_psc","402_Button_CB_prefab");
                        break;
                    }
                    TeleportManager.instance.Teleport(player1AreaTransform[0]);
                    rfcMessenger.SetCollideEventRFC("HA_09",1 ,true);
                    rfcMessenger.SetCollideEventRFC("HA_11",1 ,true);

                    break;
                case(Cabinate25kProcedure.FINGERING_VOLTAGE_INDICATOR_0911):
                    rfcMessenger.SetCollideEventRFC("HA_09",1 ,false);
                    rfcMessenger.SetCollideEventRFC("HA_09",2 ,false);
                    rfcMessenger.SetCollideEventRFC("HA_11",1 ,false);
                    rfcMessenger.SetCollideEventRFC("HA_11",2 ,false);
                    rfcMessenger.SetCollideEventRFC("HA09_FingeringVoltageIndicator",1 ,true);
                    rfcMessenger.SetCollideEventRFC("HA11_FingeringVoltageIndicator",1 ,true);
                    break;
                case(Cabinate25kProcedure.SETUP_BARRIER):
                    rfcMessenger.SetCollideEventRFC("HA09_FingeringVoltageIndicator",1 ,false);
                    rfcMessenger.SetCollideEventRFC("HA09_FingeringVoltageIndicator",2 ,false);
                    rfcMessenger.SetCollideEventRFC("HA11_FingeringVoltageIndicator",1 ,false);
                    rfcMessenger.SetCollideEventRFC("HA11_FingeringVoltageIndicator",2 ,false);
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier1",1 ,true);
                    break;
                case(Cabinate25kProcedure.APPLY_DANGER_NOTICE1_HA_09):
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier1",1 ,false);
                    rfcMessenger.SetCollideEventRFC("FingeringSafetyBarrier1",2 ,false);
                    rfcMessenger.SetSnapEventRFC("HA_09");
                    rfcMessenger.SetSnapEventRFC("HA_11");
                    switch25kvGearRoom.safetyBarrierList[0].SetActive(true);

                    break;
                case(Cabinate25kProcedure.SWITCH25K_PROCEDURE):
                    switch25kVCabinateList[0].currentPart1Procedure = Switch25kVCabinate.Part1Procedure.FINGERING_SWITCHGEAR_STATUS;
                    switch25kVCabinateList[1].currentPart1Procedure = Switch25kVCabinate.Part1Procedure.FINGERING_SWITCHGEAR_STATUS;

                    rfcMessenger.SetCollideEventRFC("HA09_FingeringStatus",1,true);
                    rfcMessenger.SetCollideEventRFC("HA11_FingeringStatus",1 ,true);
                    
                    break;
                case(Cabinate25kProcedure.FINGERING_CONTROL_PANEL):
                    TeleportManager.instance.Teleport(player1AreaTransform[2]);
                    rfcMessenger.SetCollideEventRFC("Fingering_Isolator",1,true);
                    break;
                case(Cabinate25kProcedure.OPEN_CONTROL_PANEL):
                    rfcMessenger.SetCollideEventRFC("Fingering_Isolator",1 ,false);
                    rfcMessenger.SetCollideEventRFC("Fingering_Isolator",2 ,false);
                    rfcMessenger.SetCollideEventRFC("Open_Isolator",1 ,true);

                    break;
                case(Cabinate25kProcedure.CONTROL_PANEL_PROCEDURE):
                    rfcMessenger.SetCollideEventRFC("Open_Isolator",1 ,false);
                    AnimationManager.instance.RunAnimationBool("Cover_open","cover",true);
                    rfcMessenger.SetCollideEventRFC("503A_VTLight",1 ,true);
                    rfcMessenger.SetCollideEventRFC("504A_VTLight",1 ,true);
                    rfcMessenger.SetCollideEventRFC("503A_SetControlToLocal",1 ,true);
                    rfcMessenger.SetCollideEventRFC("504A_SetControlToLocal",1 ,true);
                    rfcMessenger.SetCollideEventRFC("503A_IsolatorOpen",1 ,true);
                    rfcMessenger.SetCollideEventRFC("504A_IsolatorOpen",1 ,true);
                    
                    break;
                case(Cabinate25kProcedure.FINGERING_AC_SWITCHGEAR):
                    if(sceneSection =="Scene1A"){
                        current25kProcedure = Cabinate25kProcedure.AC_SWITCHGEAR_PROCEDURE;
                        ReadStepInstructions(true);
                        break;
                    }
                    TeleportManager.instance.Teleport(player1AreaTransform[3]);
                    rfcMessenger.SetCollideEventRFC("HA_01",1,true);
                    rfcMessenger.SetCollideEventRFC("HA_03",1 ,true);
                    if(extraInfo =="Skip"){
                        Debug.Log("Skip Part A and direct come to here");
                        Debug.Log("Set bool to tell game to finish at end of this part");
                    }
                    break;
                case(Cabinate25kProcedure.FINGERING_AC_VLOTAGE_INDICATOR_0103):
                    rfcMessenger.SetCollideEventRFC("HA_01",1,false);
                    rfcMessenger.SetCollideEventRFC("HA_03",1 ,false);
                    rfcMessenger.SetCollideEventRFC("HA_01",2,false);
                    rfcMessenger.SetCollideEventRFC("HA_03",2 ,false);
                    rfcMessenger.SetCollideEventRFC("HA01_FingeringVoltageIndicator",1,true);
                    rfcMessenger.SetCollideEventRFC("HA03_FingeringVoltageIndicator",1 ,true);

                    break;
                case(Cabinate25kProcedure.SETUP_BARRIER2):
                    rfcMessenger.SetCollideEventRFC("HA01_FingeringVoltageIndicator",1,false);
                    rfcMessenger.SetCollideEventRFC("HA03_FingeringVoltageIndicator",1 ,false);
                    rfcMessenger.SetCollideEventRFC("HA01_FingeringVoltageIndicator",2,false);
                    rfcMessenger.SetCollideEventRFC("HA03_FingeringVoltageIndicator",2,false);
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier2",1 ,true);
                    break;
                case(Cabinate25kProcedure.APPLY_DANGER_NOTICE2_HA_01):
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier2",1 ,false); 
                    rfcMessenger.SetCollideEventRFC("FingeringSafetyBarrier2",2 ,false);
                    switch25kvGearRoom.safetyBarrierList[1].SetActive(true);
                    rfcMessenger.SetSnapEventRFC("HA_01");
                    rfcMessenger.SetSnapEventRFC("HA_03");

                    break;
                case(Cabinate25kProcedure.AC_SWITCHGEAR_PROCEDURE):
                    switch25kVCabinateList[2].currentACProcedure = Switch25kVCabinate.ACProcedure.FINGERING_CB_STATUS;
                    switch25kVCabinateList[3].currentACProcedure = Switch25kVCabinate.ACProcedure.FINGERING_CB_STATUS;
                    yield return new  WaitForSeconds(2);
                    AnimationManager.instance.RunAnimationBool("light_off","HA01_Signal_Button",true);
                    AnimationManager.instance.RunAnimationBool("light_off","HA03_Signal_Button",true);
                    Debug.Log("Call this???");
                    AnimationManager.instance.RunAnimationTrigger("CB_call_psc","TIS401_Button_CB_prefab");
                    AnimationManager.instance.RunAnimationTrigger("CB_call_psc","TIS402_Button_CB_prefab");
                    rfcMessenger.SetCollideEventRFC("HA01_CBStatus",1 ,true);
                    rfcMessenger.SetCollideEventRFC("HA03_CBStatus",1 ,true);
                    break;
                case(Cabinate25kProcedure.CALL_PSC3):
                    if(sceneSection =="Scene1B"){
                        current25kProcedure = Cabinate25kProcedure.FINISH;
                        Debug.Log("SECTION 1 B FINISH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        break;
                    }
                    TeleportManager.instance.Teleport(player1AreaTransform[4]);
                    TeleportManager.instance.SetCurrentPointerList(new string[] {"Isolator503AFront","Isolator504AFront"});
                    TeleportManager.instance.SetPointer("Isolator503AFront",true);
                    break;
                case(Cabinate25kProcedure.CHECK_TESTER):
                    foreach(EarthingTesterCollider collider in switch25kvGearRoom.earthingTesterColliderList){
                        collider.earthTestStart = true;
                    }
                    AnimationManager.instance.RunAnimationBool("Window_open","504_Window_ani",true);
                    AnimationManager.instance.RunAnimationBool("Window_open","503_Window_ani",true);
                    break;
                case(Cabinate25kProcedure.APPLY_TO_CIRCUIT):

                    break;
                case(Cabinate25kProcedure.CHECK_TESTER2):
                    break;
                case(Cabinate25kProcedure.CLOSE_EARTH_SWITCH_PROCEDURE):
                    switch25KVControlPanel.current503AProcedure =Switch25kVControlPanel.ControlPanelProcedure.CLOSE_EARTH_SWITCH;
                    switch25KVControlPanel.current504AProcedure =Switch25kVControlPanel.ControlPanelProcedure.CLOSE_EARTH_SWITCH;
                    rfcMessenger.SetCollideEventRFC("503A_CloseEarthSwitch",1,true);
                    rfcMessenger.SetCollideEventRFC("504A_CloseEarthSwitch",1,true);
                    TeleportManager.instance.Teleport(player1AreaTransform[2]);
                    break;
                case(Cabinate25kProcedure.CALL_PSC4):

                    break;
                case(Cabinate25kProcedure.APPLY_PADLOCK2):
                    break;
                case(Cabinate25kProcedure.APPLY_SAFETY_DOCUMENT):
                    switch25kvGearRoom.safetyDocumentNPC.SetActive(true);
                    // AnimationManager.instance.RunAnimationBool("Window_open","504_Window_ani",true);
                    // AnimationManager.instance.RunAnimationBool("Window_open","503_Window_ani",true);
                    break;
                case(Cabinate25kProcedure.FINISH):
                    rfcMessenger.PlayAnimationTriggerRFC("Safety_document","NPC_MTR_Player2");
                    switch25kvGearRoom.safetyDocumentNPC.GetComponentInChildren<MultiDCCBDocumentNPC>().safetyDocumentDone.SetActive(true);
                    rfcMessenger.RemoveRightHandObjectRFC();
                    yield return new WaitForSeconds(10);
                    switch25kvGearRoom.safetyDocumentNPC.GetComponentInChildren<MultiDCCBDocumentNPC>().safetyDocumentDone.SetActive(false);
                    switch25kvGearRoom.safetyDocumentOnWall.SetActive(true);
                    break;

                default:
                    break;
            }
        }else{
            switch(current25kProcedure){
                case(Cabinate25kProcedure.SETUP_BARRIER):
                    if(extraInfo == "OpenBarrier")
                        switch25kvGearRoom.safetyBarrierList[0].SetActive(true);
                    break;
                case(Cabinate25kProcedure.SETUP_BARRIER2):
                    if(extraInfo == "OpenBarrier")
                        switch25kvGearRoom.safetyBarrierList[1].SetActive(true);
                    break;
                case(Cabinate25kProcedure.SWITCH25K_PROCEDURE): 
                    Debug.Log("get extra info  " + extraInfo);
                    if(extraInfo =="HA09_True"){
                        StartCoroutine(switch25kVCabinateList[0].CabinateReadStepInstructions(true,soloTest));
                    }
                    if(extraInfo =="HA11_True"){
                        StartCoroutine(switch25kVCabinateList[1].CabinateReadStepInstructions(true,soloTest));
                    }
                    if(extraInfo =="Phone"){
                        // IncorrectStepHandleManager.instance.StoreAction("0911_CallPSC");
                        switch25kVCabinateList[0].part1CallPSCPowerOff = true;
                        switch25kVCabinateList[1].part1CallPSCPowerOff = true;
                        AudioManager.instance.PlayAudioClip("Com_Phone_Call");
                        StartCoroutine(switch25kVCabinateList[0].CabinateReadStepInstructions(false,soloTest));
                        StartCoroutine(switch25kVCabinateList[1].CabinateReadStepInstructions(false,soloTest));
                    }
                    if(extraInfo =="CheckStatus"){
                        if(switch25kVCabinateList[0].part1ProcedureFinished &&
                            switch25kVCabinateList[1].part1ProcedureFinished){
                            Debug.Log("enter next step");
                            // if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() ==2){
                            //     Debug.Log("Test call function!!!!!!!!!!!!!!!!!!!!!!!");
                                rfcMessenger.ReadStepInstructionsRFC(true);
                            // }
                        }
                    }
                    break;
                case(Cabinate25kProcedure.CONTROL_PANEL_PROCEDURE):
                    if(extraInfo =="503A_True"|| extraInfo =="504A_True"){
                        switch25KVControlPanel.ControlPanelReadStepInstructions(true,extraInfo.Substring(0,4),soloTest);
                    }
                    break;
                case(Cabinate25kProcedure.AC_SWITCHGEAR_PROCEDURE):
                    if(extraInfo =="HA01_True"){
                        StartCoroutine(switch25kVCabinateList[2].ACReadStepInstructions(true,soloTest));
                    }
                    if(extraInfo =="HA03_True"){
                        StartCoroutine(switch25kVCabinateList[3].ACReadStepInstructions(true,soloTest));
                    }
                    if(extraInfo =="Call_PSC"){
                        StartCoroutine(switch25kVCabinateList[2].ACReadStepInstructions(true,soloTest));
                        StartCoroutine(switch25kVCabinateList[3].ACReadStepInstructions(true,soloTest));
                    }
                    if(extraInfo =="CheckStatus"){
                        if(switch25kVCabinateList[2].acProcedureFinished &&
                            switch25kVCabinateList[3].acProcedureFinished){
                            Debug.Log("enter next step");
                            if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() ==2){
                                rfcMessenger.ReadStepInstructionsRFC(true);
                            }
                        }
                    }                    
                    break;
                case(Cabinate25kProcedure.CLOSE_EARTH_SWITCH_PROCEDURE):
                    if(extraInfo =="503A_True"|| extraInfo =="504A_True"){
                        switch25KVControlPanel.ControlPanelReadStepInstructions(true,extraInfo.Substring(0,4),soloTest);
                    }
                    break;
                default:break;
            }
        }

        if(soloTest){
            switch(current25kProcedure){
            case(Cabinate25kProcedure.FINGERING_IDENTIDY_CIRCUIT):
                CollideEventObjectManager.instance.SetColliderObjectActive("HA_09",2 ,true);
                CollideEventObjectManager.instance.SetColliderObjectActive("HA_11",2 ,true);
                break;
            case(Cabinate25kProcedure.SETUP_BARRIER):
                rfcMessenger.SetCollideEventRFC("SafetyBarrier",1,true);
                break;

            default: break;
            }
        }

        yield return 0;
    }
    public void SetHandleRotate(float angle,string code,string hole){
        foreach(SwitchHandleRotatorEvent rotator in switch25kvGearRoom.handleRotatorList){
            if(rotator.cabinateCode == code && rotator.holeName == hole){
                rotator.SetAngle(angle);
            }
        }
    }
    public void RemoveHandle(string code,string hole){
        foreach(SwitchHandleRotatorEvent rotator in switch25kvGearRoom.handleRotatorList){
            if(rotator.cabinateCode == code && rotator.holeName == hole){
                rotator.transform.gameObject.SetActive(false);
            }
        }
    }

    public void SkipStep(){
        switch(current25kProcedure){
            case(Cabinate25kProcedure.SWITCH25K_PROCEDURE):
                if(switch25kVCabinateList[0].currentPart1Procedure != Switch25kVCabinate.Part1Procedure.SET_CONTROL_POSITION&&
                    switch25kVCabinateList[1].currentPart1Procedure != Switch25kVCabinate.Part1Procedure.SET_CONTROL_POSITION){
                    rfcMessenger.ReadStepInstructionsRFC(false,"HA09_True");
                    rfcMessenger.ReadStepInstructionsRFC(false,"HA11_True");
                }else{
                    rfcMessenger.NextStepRFC();
                }
                break;
            case(Cabinate25kProcedure.AC_SWITCHGEAR_PROCEDURE):
                if(switch25kVCabinateList[2].currentACProcedure != Switch25kVCabinate.ACProcedure.APPLY_PADLOCK&&
                    switch25kVCabinateList[3].currentACProcedure != Switch25kVCabinate.ACProcedure.APPLY_PADLOCK){
                    rfcMessenger.ReadStepInstructionsRFC(false,"HA01_True");
                    rfcMessenger.ReadStepInstructionsRFC(false,"HA03_True");
                }else{
                    rfcMessenger.NextStepRFC();
                }
                break;
            case(Cabinate25kProcedure.CONTROL_PANEL_PROCEDURE):
            case(Cabinate25kProcedure.CLOSE_EARTH_SWITCH_PROCEDURE):
                rfcMessenger.ReadStepInstructionsRFC(false,"503A_True");
                rfcMessenger.ReadStepInstructionsRFC(false,"504A_True");
                break;
            default:
                rfcMessenger.NextStepRFC();
                break;
        }
    }
}
