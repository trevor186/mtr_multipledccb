﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class EarthingSnapEvent : MonoBehaviour
{
    public VRTK_SnapDropZone snapDropZone;
    public bool isWall;
    void Start()
    {

    }
    
    private void OnTriggerEnter(Collider other)
    {
                // Debug.Log("Show other "+ other.name);

        if(isWall){
            if (other.tag == "EarthingWall")
            {
                Debug.Log("EarthingWall");
                // GameObject clone = Instantiate(other.gameObject);
                // clone.GetComponent<BoxCollider>().enabled = false;
                // snapDropZone.ForceSnap(clone);
                RFCMessenger.instance.NextStepRFC("EarthingConnectionStart");
                // AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();
                // other.gameObject.SetActive(false);
            }
        }else{
            if(other.tag =="EarthingGear"){
                Debug.Log("EarthingGear");
                // GameObject clone = Instantiate(other.gameObject);
                // clone.GetComponent<BoxCollider>().enabled = false;
                // snapDropZone.ForceSnap(clone);
                RFCMessenger.instance.NextStepRFC("EarthingConnectionEnd");
                // other.gameObject.SetActive(false);
            }
        }

    }
}
