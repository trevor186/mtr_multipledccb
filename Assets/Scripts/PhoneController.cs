﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneController : MonoBehaviour
{
    public GameObject basePhoneObject;
    public List<GameObject> phoneCallList = new List<GameObject>();
    
    private void Start() {
        this.gameObject.SetActive(true);
    }

    public void CallPhone(int value){
        basePhoneObject.SetActive(true);
        foreach(GameObject obj in phoneCallList){
            obj.SetActive(false);
        }
        phoneCallList[value].SetActive(true);
        CancelInvoke();
        Invoke("ClosePhone",5f);
    }
    public void ClosePhone(){
        basePhoneObject.SetActive(false);
        foreach(GameObject obj in phoneCallList)
            obj.SetActive(false);
    }
    public bool isPhoneUsing
    {
        get
        {
            return basePhoneObject.activeInHierarchy;
        }
    }
    public void MultiDCCBCallPhone(){
        basePhoneObject.SetActive(true);
        CancelInvoke();
        Invoke("ClosePhone",5f);
    }
}
