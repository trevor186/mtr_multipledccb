﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MTRTeleportArea : MonoBehaviour
{
    [SerializeField] private bool _player1Enter;
    [SerializeField] private bool _player2Enter;
    [SerializeField] private Animator animator;
    private float _timer;
    private bool _checkEnterAudio;
    private bool _checkFinishAudio;
    public int teleportIndex;
    public GameObject animatiorObject;
    public bool FinishWaiting;
    private void OnTriggerStay(Collider other)
    {
        if(GlobalConfig.instance.GetNetworkID() == 1){
            if(other.tag =="TeleportBox"){
                if(other.GetComponentInParent<MTRPlayer>().GetPlayerID() == 1){
                    _player1Enter = true;
                }else if(other.GetComponentInParent<MTRPlayer>().GetPlayerID() == 2){
                    _player2Enter = true;
                }
                if(_player1Enter &&(AMVRNetworkManager.instance.IsSoloPlayMode()? true:_player2Enter)){
                    _timer += Time.deltaTime;
                }
            }
        }   
    }
    void Update(){
        if(AMVRNetworkManager.instance.IsSoloPlayMode()){
            animator.SetBool("Transitions_Start",(_player1Enter));
            // if(_player1Enter &&!_checkEnterAudio){
            //     RFCMessenger.instance.PlayAudioRFC("Transition_In");
            //     _checkEnterAudio = true;
            // }
            // CallAnimationRFC("Transitions_Start",_player1Enter);
        }else{
            // if((_player1Enter&&_player2Enter)&& !_checkEnterAudio){
            //     RFCMessenger.instance.PlayAudioRFC("Transition_In");
            //     _checkEnterAudio = true;
            // }
            animator.SetBool("Transitions_Start",(_player1Enter||_player2Enter ));

            // CallAnimationRFC("Transitions_Start",_player1Enter||_player2Enter);

        }

        // animator.enabled= (_player1Enter &&(ScenarioEventManager.instance.IsSoloPlay()? true:_player2Enter));
        if(_timer >4f){
            animator.SetBool("Transitions_Finish",true);
            if(!_checkFinishAudio){
                RFCMessenger.instance.PlayAudioRFC("Com_Finish");
                _checkFinishAudio = true;
            }
            // CallAnimationRFC("Transitions_Finish",true);
            Invoke("DoTeleport",1f);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag =="TeleportBox"){
            _player1Enter = false;
            _player2Enter = false;
            _checkEnterAudio = false;
            _timer = 0;
        }   
    }
    // public void EnableTeleport(bool value,int index =0){
    //     if(animatiorObject != null) animatiorObject.SetActive(value);
    //     this.gameObject.SetActive(value);
    //     // teleportIndex = index;
    //     TeleportManager.instance.currentTeleportPointIndex = index;
    // }
    public void DoTeleport(){
        // StartCoroutine(ShuntingOperationSceneManager.instance.Teleport(teleportIndex));
        MultiDCCBSceneRFCMessenger.instance.DoTeleportRFC();        
        _timer=0;
        _player1Enter = false;
        _player2Enter = false;
        _checkEnterAudio = false;
    }
    
}
