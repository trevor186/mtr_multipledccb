﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Panel : MonoBehaviour
{
    private void Start() {
        this.gameObject.SetActive(GlobalConfig.instance.GetNetworkID() == 2);
    }
}
