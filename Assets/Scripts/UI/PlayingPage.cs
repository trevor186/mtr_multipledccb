﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class PlayingPage : MonoBehaviour
{
    public GameObject playerRawImage;
    public Button reportButton;
    public GameObject reportPanel;
    public Button enterButton;
    public Image redScreen;
     
    public Color objectColor = Color.white;
    public Color fadeColor = Color.red;
    public float fadeTime = 2;
    public float fadeStart = 0;
    public bool showRed;
    // Start is called before the first frame update
    void Start()
    {
        if(reportPanel != null)
            reportPanel.SetActive(false);
        reportButton.gameObject.SetActive(GlobalConfig.instance.GetNetworkID() == 1);
    }

    // Update is called once per frame
    void Update()
    {
        if(GlobalConfig.instance != null){
            if (GlobalConfig.instance.GetNetworkID() == 1)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    RFCMessenger.instance.SendAll("BackToWaitingRoom");
                }
            }
        }
        
        if (Input.GetKeyDown(KeyCode.F))
        {
            ToggleFullScreen();
        }
        enterButton.onClick.AddListener(onEnterButtonClicked);
            
        // Color newColor = redScreen.color;
        // if(showRed){
        //     newColor.a = Mathf.Min(0.4f,newColor.a+Time.deltaTime);
        //     ShuntingOperationSceneManager.instance.headsetFade.Fade(newColor,0.5f);
        // }else{
        //     newColor.a = 0;
        //     ShuntingOperationSceneManager.instance.headsetFade.Unfade(0);
        // }
        // redScreen.color = newColor;
    }

    public void onEnterButtonClicked(){
        RFCMessenger.instance.NextStepRFC(("Instructor"));
    }
    public void ToggleFullScreen()
    {
        if (playerRawImage.transform.localScale == Vector3.one)
            playerRawImage.transform.localScale = new Vector3(0.4f, 0.4f);
        else
            playerRawImage.transform.localScale = Vector3.one;
    }

    public void ToggleReportPanel()
    {
        reportPanel.SetActive(!reportPanel.activeInHierarchy);
    }

    public void ShowEnterSign(bool value){
        enterButton.gameObject.SetActive(value);
    }
    public void ShowRedSceen(bool value){
        showRed = value;
    }
}
