﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StartPage : MonoBehaviour
{
    // public Toggle startToggle;
    // public List<Toggle> playerToggleList;   // Start is called before the first frame update
    public Toggle manualToggle;
    public Toggle voiceOverToggle;
    public Toggle player2Toggle;

    private void Start() {
        this.gameObject.SetActive(GlobalConfig.instance.IsServer());
    }

    private void Update() {
        player2Toggle.isOn =(AMVRNetworkManager.instance.GetPlayerCount() == 2);
        
        if(Input.GetKeyDown(KeyCode.Return)||Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            GameStart();
        }
    }

    public void QuitApp()
    {
        AMVRNetworkManager.instance.QuitApp();
    }

    public void GameStart()
    {
        ScenarioEventManager.instance.GameStart(manualToggle.isOn, voiceOverToggle.isOn, player2Toggle.isOn);
    }
}
