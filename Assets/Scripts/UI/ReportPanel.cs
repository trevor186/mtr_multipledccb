﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReportPanel : MonoBehaviour
{
    public List<GameObject> ticks = new List<GameObject>();
    public List<GameObject> remarks = new List<GameObject>();

    public List<Sprite> manualReport = new List<Sprite>();
    public List<Sprite> motorReport = new List<Sprite>();
    public List<Image> reportImage = new List<Image>();
    public void Init()
    {
        foreach (GameObject item in ticks)
        {
            item.SetActive(false);            
        }

        foreach (GameObject item in remarks)
        {
            item.SetActive(false);
            item.GetComponent<Text>().text = "";
        }

        this.gameObject.SetActive(false);
    }

    public void UpdateReportStep(int step, bool result)
    {
        if (step > 0 && step <= ticks.Count)
            ticks[step-1].SetActive(result);
    }
    public void ChangeToMotorReport(bool value){
        Debug.Log("init this");
        if(value){
            reportImage[0].sprite = manualReport[0];
            reportImage[1].sprite = manualReport[1];
        }else{
            reportImage[0].sprite = motorReport[0];
            reportImage[1].sprite = motorReport[1];
        }
    }

    public void UpdateRemark(int step, string remarkText)
    {
        if (step > 0 && step <= remarks.Count)
        {
            remarks[step-1].GetComponent<Text>().text = remarkText;
            remarks[step-1].gameObject.SetActive(true);
        }

        // int idx = -1;
        // switch(step)
        // {
        //     case 8: idx = 0; break;
        //     case 19: idx = 1; break;
        //     case 22: idx = 2; break;
        //     case 24: idx = 3; break;
        //     default: idx = -1; break;
        // }

        // if (idx != -1)
        //     remarks[idx].GetComponent<Text>().text = remarkText;
    }

}
