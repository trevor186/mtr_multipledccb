﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiDCCBScene2 : MonoBehaviour
{
    public List<GameObject> TISsafetyBarriers =new List<GameObject>();
    public List<EarthingTesterCollider> earthingTesterCollidersList = new List<EarthingTesterCollider>();
    public GameObject TISRoom;
    public GameObject TISRoomOutside;
    public GameObject KSRRail;
    public GameObject U123;
    public GameObject D123;
    public GameObject U123RedLightHolder;
    public GameObject D123RedLightHolder;
}
