﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using UnityEngine.SceneManagement;
using TNet;

public class MultiDCCBScene2EventManager : MonoBehaviour
{
    public static MultiDCCBScene2EventManager instance;
    void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    private MultiDCCBSceneRFCMessenger rfcMessenger;
    public enum Scene2Procedure{
        WAITING_ROOM,
        INTRODUCTION,
        PART_A_INTRODUCTION,
        PART_A_SCAN_QR_CODE,
        PART_A_WAIT_FOR_CALL_1,
        PART_A_ANSWER_CALL_1,
        // PART_A_PMC_814_FINGERING_NAME,
        // PART_A_PMC_814_FINGERING_STATUS,
        // PART_A_PMC_814_CHANGE_TO_LOCAL,
        // PART_A_PMC_814_APPLY_CAUTION_NOTICE,
        // PART_A_KSR714_FINGERING_NAME,
        // PART_A_KSR714_FINGERING_STATUS,
        // PART_A_KSR711_FINGERING_NAME,
        // PART_A_KSR711_FINGERING_STATUS,
        // PART_A_ANSWER_CALL_1,
        PART_A_CABINATE_PROCEDURES,
        PART_A_CALL_PSC_2,
        PART_A_WAIT_FOR_CALL_2,
        PART_A_ANSWER_CALL_2,
        PART_A_U123_CHECK_TESTER,
        PART_A_U123_APPLY_CIRCUIT,
        PART_A_U123_CHECK_TESTER_2,
        PART_A_U123_ATTACH_LOCK,
        PART_A_U123_FINGERING_LOCK,
        PART_A_U123_PLACE_RED_LAMP,
        PART_A_U123_FINGERING_RED_LAMP,
        PART_A_D123_CHECK_TESTER,
        PART_A_D123_APPLY_CIRCUIT,
        PART_A_D123_CHECK_TESTER_2,
        PART_A_D123_ATTACH_LOCK,
        PART_A_D123_FINGERING_LOCK,
        PART_A_D123_PLACE_RED_LAMP,
        PART_A_D123_FINGERING_RED_LAMP,
        // PART_A_APPLY_CME,
        // PART_A_PLACE_POSSESSION,
        PART_A_CALL_PSC_3,
        PART_A_FINISH,
        PART_B_INTRODUCTION,
        PART_B_CALL_PSC_1,
        PART_B_FINGERING_CABINATES,
        PART_B_FINGERING_VOLTAGE_INDICATORS,
        PART_B_SETUP_SAFETY_BARRIER,
        PART_B_APPLY_DANGER_NOTICE_401,
        PART_B_APPLY_DANGER_NOTICE_402,
        PART_B_APPLY_DANGER_NOTICE_403,
        PART_B_APPLY_DANGER_NOTICE_404,
        PART_B_WAIT_CALL_1,
        PART_B_ANSWER_CALL_1,
        PART_B_FINGERING_CB_PROCEDURE,
        PART_B_CHANGE_CONTROL_PROCEDURE,
        PART_B_APPLY_CAUTION_NOTICE_401,
        PART_B_APPLY_CAUTION_NOTICE_402,
        PART_B_APPLY_CAUTION_NOTICE_403,
        PART_B_APPLY_CAUTION_NOTICE_404,
        PART_B_CALL_PSC_2,
        PART_B_GO_OUTSIDE,
        PART_B_OUTSIDE_CHANGE_CONTROL_PROCEDURE,
        // PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_401,
        // PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_402,
        // PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_403,
        // PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_404,
        PART_B_CALL_PSC_3,
        PART_B_WAIT_CALL_2,
        PART_B_ANSWER_CALL_2,
        PART_B_OUTSIDE_CHECK_TESTER,
        PART_B_OUTSIDE_APPLY_TO_CIRCUIT,
        PART_B_OUTSIDE_CHECK_TESTER_2,
        // PART_B_CALL_PSC_4,
        PART_B_APPLY_EARTHING_PROCEDURE,
        PART_B_CALL_PSC_5,
        PART_B_WAIT_CALL_3,
        PART_B_ANSWER_CALL_3,
        PART_B_FINISH

    }
    public Scene2Procedure currentProcedure;
    public MultiDCCBScene2 scene2;
    public string sceneSection;
    public System.Collections.Generic.List<Scene2TISSwitchCabinate> TISSwitchCabinateList = new System.Collections.Generic.List<Scene2TISSwitchCabinate>();
    public System.Collections.Generic.List<Scene2LTSwitchCabinate> LTSwitchCabinateList = new System.Collections.Generic.List<Scene2LTSwitchCabinate>();
    public System.Collections.Generic.List<Scene2KSRSwitchCabinate> KSRSwitchCabinateList = new System.Collections.Generic.List<Scene2KSRSwitchCabinate>();
    // Start is called before the first frame update
    void Start()
    {
        rfcMessenger = MultiDCCBSceneRFCMessenger.instance;
        TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[0]);
        sceneSection =(string) TNManager.GetPlayerData("SceneParameter").value;
        if(sceneSection == "Scene2A"){
            AudioManager.instance.PlayAudioClip("Scene2A_1");
        }
        if(sceneSection == "Scene2B"){
            AudioManager.instance.PlayAudioClip("Scene2B_1");
        } 
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.S)){
            MultiDCCBSceneHandlingManager.instance.GetMTRPlayer(1).SwapPlayerID();
            CollideEventObjectManager.instance.ChangeColliderList();
            // StartCoroutine(ReadStepInstructions(false,null,true));
        } 
        if(Input.GetKeyDown(KeyCode.Q)){
            SkipStep();
        }
    }
    public void ActionCallResult(string action,string extraInfo =null ){
        Debug.Log("======CHECK               Action           " + action);
        if(action.StartsWith("UI_")){
            MultiDCCBSceneHandlingManager.instance.UIPanelActionCall(action.Replace("UI_",""));
        }
        switch(currentProcedure){
            case(Scene2Procedure.PART_A_SCAN_QR_CODE):
                if(action =="QRCode"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_A_ANSWER_CALL_1):
                if(action =="Phone"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_A_CABINATE_PROCEDURES):
                 Scene2KSRSwitchCabinate KSRcabinate = GetKSRSwitchCabinate(action);
                if(KSRcabinate!= null){
                    Debug.Log("???"+  KSRcabinate.name);
                    KSRcabinate.CabinateActionCallResult(action.Replace(KSRcabinate.cabinateCode+"_",""),KSRcabinate.cabinateCode);
                }
                break;
            case(Scene2Procedure.PART_A_CALL_PSC_2):
            case(Scene2Procedure.PART_A_ANSWER_CALL_2):
                if(action =="Phone"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_A_U123_CHECK_TESTER):
            case(Scene2Procedure.PART_A_U123_CHECK_TESTER_2):
            case(Scene2Procedure.PART_A_D123_CHECK_TESTER):
            case(Scene2Procedure.PART_A_D123_CHECK_TESTER_2):
                if(action =="CheckTester"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_A_U123_APPLY_CIRCUIT):
            case(Scene2Procedure.PART_A_D123_APPLY_CIRCUIT):
                if(action.Contains("ApplyCircuit")){
                    AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                    rfcMessenger.ReadStepInstructionsRFC(true);
                    // AudioManager.in
                }   
                break;
            case(Scene2Procedure.PART_A_U123_ATTACH_LOCK):
            case(Scene2Procedure.PART_A_D123_ATTACH_LOCK):
                if(action.Contains("AttachLock")){
                    Debug.Log("correct?");
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_A_D123_FINGERING_LOCK):
            case(Scene2Procedure.PART_A_U123_FINGERING_LOCK):
                if(action.Contains("Fingering_Earthing")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;            
            case(Scene2Procedure.PART_A_D123_FINGERING_RED_LAMP):
            case(Scene2Procedure.PART_A_U123_FINGERING_RED_LAMP):
                if(action.Contains("Fingering_Red_Lamp")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_A_U123_PLACE_RED_LAMP):
            case(Scene2Procedure.PART_A_D123_PLACE_RED_LAMP):
                if(action =="Red_flash_prefab"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_A_CALL_PSC_3):
                if(action =="Phone"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_CALL_PSC_1):
                if(action =="Phone"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_FINGERING_CABINATES):
                if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    switch(action){
                        case("TIS_401"):
                            rfcMessenger.SetCollideEventRFC("TIS_401",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS_401",2,true);
                            break;
                        case("TIS_402"):
                            rfcMessenger.SetCollideEventRFC("TIS_402",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS_402",2,true);
                            break;
                        case("TIS_403"):
                            rfcMessenger.SetCollideEventRFC("TIS_403",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS_403",2,true);
                            break;
                        case("TIS_404"):
                            rfcMessenger.SetCollideEventRFC("TIS_404",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS_404",2,true);
                            break;
                        default:
                            break;
                    }
                }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                    switch(action){
                        case("TIS_401"):
                            rfcMessenger.SetCollideEventRFC("TIS_401",2,false);
                            TISSwitchCabinateList[0].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING;
                            break;
                        case("TIS_402"):
                            rfcMessenger.SetCollideEventRFC("TIS_402",2,false);
                            TISSwitchCabinateList[1].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING;
                            break;
                        case("TIS_403"):
                            rfcMessenger.SetCollideEventRFC("TIS_403",2,false);
                            TISSwitchCabinateList[2].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING;
                            break;
                        case("TIS_404"):
                            rfcMessenger.SetCollideEventRFC("TIS_404",2,false);
                            TISSwitchCabinateList[3].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING;
                            break;
                        default:
                            break;
                    }
                    if(TISSwitchCabinateList[0].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING &&
                        TISSwitchCabinateList[1].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING &&
                        TISSwitchCabinateList[2].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING &&
                        TISSwitchCabinateList[3].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING                    
                    ){
                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                }
                break;
            case(Scene2Procedure.PART_B_FINGERING_VOLTAGE_INDICATORS):
                if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    switch(action){
                        case("TIS401_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS401_FingeringVoltageIndicator",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS401_FingeringVoltageIndicator",2,true);
                            break;
                        case("TIS402_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS402_FingeringVoltageIndicator",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS402_FingeringVoltageIndicator",2,true);
                            break;
                        case("TIS403_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS403_FingeringVoltageIndicator",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS403_FingeringVoltageIndicator",2,true);
                            break;
                        case("TIS404_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS404_FingeringVoltageIndicator",1,false);
                            rfcMessenger.SetCollideEventRFC("TIS404_FingeringVoltageIndicator",2,true);
                            break;
                        default:
                            break;
                    }
                }else if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 2){
                    switch(action){
                        case("TIS401_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS401_FingeringVoltageIndicator",2,false);
                            TISSwitchCabinateList[0].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR;
                            break;
                        case("TIS402_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS402_FingeringVoltageIndicator",2,false);
                            TISSwitchCabinateList[1].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR;
                            break;
                        case("TIS403_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS403_FingeringVoltageIndicator",2,false);
                            TISSwitchCabinateList[2].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR;
                            break;
                        case("TIS404_FingeringVoltageIndicator"):
                            rfcMessenger.SetCollideEventRFC("TIS404_FingeringVoltageIndicator",2,false);
                            TISSwitchCabinateList[3].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR;
                            break;
                        default:
                            break;
                    }
                    if(TISSwitchCabinateList[0].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR &&
                        TISSwitchCabinateList[1].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR &&
                        TISSwitchCabinateList[2].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR &&
                        TISSwitchCabinateList[3].currentProcedure == Scene2TISSwitchCabinate.CabinateProcedure.FINISHED_FINGERING_VOLTAGE_INDICATOR                    
                    ){
                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                }
                break;
            case(Scene2Procedure.PART_B_SETUP_SAFETY_BARRIER):
                if(action =="SafetyBarrier"){
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier",1,false);
                    rfcMessenger.SetCollideEventRFC("FingeringSafetyBarrier",2,true);
                    rfcMessenger.ReadStepInstructionsRFC(false,"OpenBarrier");
                }
                if(action =="FingeringSafetyBarrier"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_APPLY_DANGER_NOTICE_401):
            case(Scene2Procedure.PART_B_APPLY_DANGER_NOTICE_402):
            case(Scene2Procedure.PART_B_APPLY_DANGER_NOTICE_403):
            case(Scene2Procedure.PART_B_APPLY_DANGER_NOTICE_404):
                if(action.Contains("TIS_401_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }               
                if(action.Contains("TIS_402_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }                
                if(action.Contains("TIS_403_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }               
                if(action.Contains("TIS_404_Danger_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_ANSWER_CALL_1):
                if(action =="Phone"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_FINGERING_CB_PROCEDURE):
                Scene2TISSwitchCabinate TISCabinate = GetTISSwitchCabinate(action);
                if(TISCabinate!= null){
                    TISCabinate.CabinateActionCallResult(action.Replace(TISCabinate.cabinateCode+"_",""),TISCabinate.cabinateCode);
                }
                break;
            case(Scene2Procedure.PART_B_APPLY_CAUTION_NOTICE_401):
            case(Scene2Procedure.PART_B_APPLY_CAUTION_NOTICE_402):
            case(Scene2Procedure.PART_B_APPLY_CAUTION_NOTICE_403):
            case(Scene2Procedure.PART_B_APPLY_CAUTION_NOTICE_404):
                if(action.Contains("TIS_401LocalSwitch_Caution_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }               
                if(action.Contains("TIS_402LocalSwitch_Caution_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }                
                if(action.Contains("TIS_403LocalSwitch_Caution_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }               
                if(action.Contains("TIS_404LocalSwitch_Caution_Notice_prefab(Clone)")){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_CHANGE_CONTROL_PROCEDURE):

                Scene2TISSwitchCabinate TISCabinate2 = GetTISSwitchCabinate(action);
                if(TISCabinate2!= null){
                    Debug.Log("???"+  TISCabinate2.name);
                    TISCabinate2.CabinateActionCallResult(action.Replace(TISCabinate2.cabinateCode+"_",""),TISCabinate2.cabinateCode);
                }
                break;
            case(Scene2Procedure.PART_B_CALL_PSC_2):
            case(Scene2Procedure.PART_B_CALL_PSC_3):
            case(Scene2Procedure.PART_B_ANSWER_CALL_2):
                    if(action =="Phone"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_OUTSIDE_CHANGE_CONTROL_PROCEDURE):
                Scene2LTSwitchCabinate LTCabinate = GetLTSwitchCabinate(action);
                if(LTCabinate!= null){
                    LTCabinate.CabinateActionCallResult(action.Replace(LTCabinate.cabinateCode+"_",""),LTCabinate.cabinateCode);
                }
                break;
            case(Scene2Procedure.PART_B_OUTSIDE_CHECK_TESTER):
            case(Scene2Procedure.PART_B_OUTSIDE_CHECK_TESTER_2):
                if(action =="CheckTester"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            case(Scene2Procedure.PART_B_OUTSIDE_APPLY_TO_CIRCUIT):
                if(action.Contains("ApplyCircuit")){
                    string code = action.Substring(0,5);
                    foreach(Scene2LTSwitchCabinate cab in LTSwitchCabinateList){
                        if(cab.cabinateCode == code && !cab.applyCircuitStatus){
                            AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                            cab.applyCircuitStatus = true;
                            cab.lockCollider.SetActive(false);
                        }
                    }
                    if(LTSwitchCabinateList[0].applyCircuitStatus&&
                        LTSwitchCabinateList[1].applyCircuitStatus&&
                        LTSwitchCabinateList[2].applyCircuitStatus&&
                        LTSwitchCabinateList[3].applyCircuitStatus){
                            rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                }else if(action == "ErrorCircuit"){
                    Debug.Log("Error!!!");
                }
                break;
            // case(Scene2Procedure.PART_B_CALL_PSC_4):
            //     if(action =="Phone"){
            //         rfcMessenger.ReadStepInstructionsRFC(true);
            //     }
            //     break;
            case(Scene2Procedure.PART_B_APPLY_EARTHING_PROCEDURE):
                string circuitCode = action.Substring(0,5);
                foreach(Scene2LTSwitchCabinate cab in LTSwitchCabinateList){
                    if(cab.cabinateCode == circuitCode){
                        cab.CabinateActionCallResult(action.Replace(circuitCode+"_",""),circuitCode);
                    }
                }
                break;
            case(Scene2Procedure.PART_B_CALL_PSC_5):
            case(Scene2Procedure.PART_B_ANSWER_CALL_3):
                if(action =="Phone"){
                    rfcMessenger.ReadStepInstructionsRFC(true);
                }
                break;
            
                  // case(Scene2Procedure.PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_401):
            // case(Scene2Procedure.PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_402):
            // case(Scene2Procedure.PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_403):
            // case(Scene2Procedure.PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_404):
            //     if(action.Contains("Ouside_TIS_401Panel_Danger_Notice_prefab(Clone)")){
            //         rfcMessenger.ReadStepInstructionsRFC(true);
            //     }               
            //     if(action.Contains("Ouside_TIS_402Panel_Danger_Notice_prefab(Clone)")){
            //         rfcMessenger.ReadStepInstructionsRFC(true);
            //     }                
            //     if(action.Contains("Ouside_TIS_403Panel_Danger_Notice_prefab(Clone)")){
            //         rfcMessenger.ReadStepInstructionsRFC(true);
            //     }               
            //     if(action.Contains("Ouside_TIS_404Panel_Danger_Notice_prefab(Clone)")){
            //         rfcMessenger.ReadStepInstructionsRFC(true);
            //     }
            //     break;

            default: 
                break;
        }
    }

    public IEnumerator ReadStepInstructions(bool result,string extraInfo = null,bool soloTest = false){
        Debug.Log("????????????????????????");
        if(result){
            currentProcedure += 1 ;
            Debug.Log("Enter procedure " + currentProcedure.ToString());
            switch(currentProcedure){
                case(Scene2Procedure.WAITING_ROOM):
                    break;
                case(Scene2Procedure.INTRODUCTION):
                    if(sceneSection =="Scene2A"){
                        currentProcedure = Scene2Procedure.PART_A_INTRODUCTION;
                        AudioManager.instance.PlayAudioClip("Scene2A_2");

                    }
                    if(sceneSection =="Scene2B"){
                        currentProcedure = Scene2Procedure.PART_B_INTRODUCTION;
                        TeleportManager.instance.SetCurrentPointerList(new string[] {"TIS_401_402","TIS_403_404"});
                        TeleportManager.instance.SetPointer("TIS_403_404",true);
                        AudioManager.instance.PlayAudioClip("Scene2B_2");
                    }
                    break;
                // case(Scene2Procedure.PART_A_INTRODUCTION):
                //     break;
                case(Scene2Procedure.PART_A_SCAN_QR_CODE):
                    AudioManager.instance.PlayAudioClip("Scene2A_3");
                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().IsPlayerOne()){
                        rfcMessenger.ShowRightHandObjectRFC("QRScanner");
                    }
                    MultiDCCBSceneHandlingManager.instance.rightHandController.pointer.enabled = false;
                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[3]);                
                    rfcMessenger.SetCollideEventRFC("QR_Code",1,true);
                    break;
                case(Scene2Procedure.PART_A_WAIT_FOR_CALL_1):
                    MultiDCCBSceneHandlingManager.instance.rightHandController.pointer.enabled = true;
                    rfcMessenger.RemoveRightHandObjectRFC();
                    AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                    yield return new WaitForSeconds(2);
                    AudioManager.instance.PlayAudioClip("Scene2A_4");
                    yield return new WaitForSeconds(9);
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);
                    rfcMessenger.PlayAnimationBoolRFC("KSR_trackisolator_open","KSR711_Trackside_isolator",true);
                    rfcMessenger.PlayAnimationBoolRFC("KSR_trackisolator_open","KSR714_Trackside_isolator",true);
                    rfcMessenger.PlayAnimationBoolRFC("KSR_trackisolator_open","PMC814_Trackside_isolator",true);
                    
                    yield return new WaitForSeconds(4);
                    AudioManager.instance.PlayAudioClip("Scene2A_6");

                    yield return new WaitForSeconds(6);
                    rfcMessenger.ReadStepInstructionsRFC(true);

                    break;
                case(Scene2Procedure.PART_A_CABINATE_PROCEDURES):
                    AudioManager.instance.PlayAudioClip("Scene2A_7");

                    yield return new WaitForSeconds(2);
                    foreach(Scene2KSRSwitchCabinate ksrCab in KSRSwitchCabinateList){
                        ksrCab.currentProcedure = Scene2KSRSwitchCabinate.CabinateProcedure.FINGERING_PANEL;
                        rfcMessenger.SetCollideEventRFC(ksrCab.cabinateCode+"_FingeringPanel",1,true);
                    }
                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[4]);
                    break;
                case(Scene2Procedure.PART_A_CALL_PSC_2):
                    AudioManager.instance.PlayAudioClip("Scene2A_12");
                    break;
                case(Scene2Procedure.PART_A_WAIT_FOR_CALL_2):
                    yield return new WaitForSeconds(5);
                    AudioManager.instance.PlayAudioClip("Scene2A_14");
                    yield return new WaitForSeconds(8);
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);
                    yield return new WaitForSeconds(4);
                    rfcMessenger.ReadStepInstructionsRFC(true);
                    break;
                case(Scene2Procedure.PART_A_ANSWER_CALL_2):
                    break;
                case(Scene2Procedure.PART_A_U123_CHECK_TESTER):
                    AudioManager.instance.PlayAudioClip("Scene2A_16");
                    yield return new WaitForSeconds(10);
                    AudioManager.instance.PlayAudioClip("Scene2A_17");
                    yield return new WaitForSeconds(3);
                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[7]);                
                    scene2.U123.GetComponent<EarthingTesterCollider>().earthTestStart = true;
                    break;
                case(Scene2Procedure.PART_A_U123_APPLY_CIRCUIT):
                    scene2.U123.GetComponent<BoxCollider>().enabled = true;
                    break;
                case(Scene2Procedure.PART_A_U123_CHECK_TESTER_2):
                    scene2.U123.GetComponent<EarthingTesterCollider>().earthTestStart = false;
                    break;
                case(Scene2Procedure.PART_A_U123_ATTACH_LOCK):
                    AudioManager.instance.PlayAudioClip("Scene2A_18");
                    yield return new WaitForSeconds(3);
                    scene2.U123.GetComponent<EarthingTesterCollider>().earthLockStart = true;
                    break;
                case(Scene2Procedure.PART_A_U123_FINGERING_LOCK):
                    rfcMessenger.RemoveLeftHandObjectRFC();
                    scene2.U123.GetComponent<BoxCollider>().enabled = false;
                    scene2.earthingTesterCollidersList[0].attachedLock.SetActive(true);
                    rfcMessenger.SetCollideEventRFC("U123_Fingering_Earthing",2,true);
                    break;
                case(Scene2Procedure.PART_A_U123_PLACE_RED_LAMP):
                    rfcMessenger.SetCollideEventRFC("U123_Fingering_Earthing",2,false);
                    yield return new WaitForSeconds(2);
                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[9]);
                    scene2.U123RedLightHolder.SetActive(true);
                    break;
                case(Scene2Procedure.PART_A_U123_FINGERING_RED_LAMP):
                    rfcMessenger.RemoveRightHandObjectRFC();
                    rfcMessenger.SetCollideEventRFC("U123_Fingering_Red_Lamp",2,true);
                    break;
                case(Scene2Procedure.PART_A_D123_CHECK_TESTER):
                    AudioManager.instance.PlayAudioClip("Scene2A_19");

                    rfcMessenger.SetCollideEventRFC("U123_Fingering_Red_Lamp",2,false);
                    yield return new WaitForSeconds(4);
                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[8]); 
                    scene2.D123.GetComponent<EarthingTesterCollider>().earthTestStart = true;
                    break;
                case(Scene2Procedure.PART_A_D123_APPLY_CIRCUIT):
                    scene2.D123.GetComponent<BoxCollider>().enabled = true;
                    break;
                case(Scene2Procedure.PART_A_D123_CHECK_TESTER_2):
                    scene2.U123.GetComponent<EarthingTesterCollider>().earthTestStart = false;
                    break;
                case(Scene2Procedure.PART_A_D123_ATTACH_LOCK):
                    AudioManager.instance.PlayAudioClip("Scene2A_19");
                    yield return new WaitForSeconds(3);

                    scene2.D123.GetComponent<EarthingTesterCollider>().earthLockStart = true;
                    break;
                case(Scene2Procedure.PART_A_D123_FINGERING_LOCK):
                    scene2.D123.GetComponent<BoxCollider>().enabled = false;

                    scene2.earthingTesterCollidersList[1].attachedLock.SetActive(true);
                    rfcMessenger.SetCollideEventRFC("D123_Fingering_Earthing",2,true);
                    break;
                case(Scene2Procedure.PART_A_D123_PLACE_RED_LAMP):
                    rfcMessenger.SetCollideEventRFC("D123_Fingering_Earthing",2,false);
                    yield return new WaitForSeconds(2);
                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[10]); 
                    scene2.D123RedLightHolder.SetActive(true);
                    break;
                case(Scene2Procedure.PART_A_D123_FINGERING_RED_LAMP):
                    rfcMessenger.RemoveRightHandObjectRFC();
                    rfcMessenger.SetCollideEventRFC("D123_Fingering_Red_Lamp",2,true);
                    break;
                case(Scene2Procedure.PART_A_CALL_PSC_3):
                    rfcMessenger.SetCollideEventRFC("D123_Fingering_Red_Lamp",2,false);
                    AudioManager.instance.PlayAudioClip("Scene2A_21");

                    break;
                case(Scene2Procedure.PART_A_FINISH):
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);
                    yield return new WaitForSeconds(5);
                    AudioManager.instance.PlayAudioClip("Scene2A_22");
                    // AudioManager.instance.PlayAudioClip("Com_Fingering procedures");

                    break;
                case(Scene2Procedure.PART_B_CALL_PSC_1):
                    //Start Scene 2B and teleport to HP.
                    AudioManager.instance.PlayAudioClip("Scene2B_3");

                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[1]);
                    break;
                case(Scene2Procedure.PART_B_FINGERING_CABINATES):
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);

                    yield return new WaitForSeconds(3); // 17.5
                    AudioManager.instance.PlayAudioClip("Scene2B_4");

                    rfcMessenger.SetCollideEventRFC("TIS_401",1,true);                        
                    rfcMessenger.SetCollideEventRFC("TIS_402",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS_403",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS_404",1,true);
                    break;
                    case(Scene2Procedure.PART_B_FINGERING_VOLTAGE_INDICATORS):
                    rfcMessenger.SetCollideEventRFC("TIS_401",1,false);                        
                    rfcMessenger.SetCollideEventRFC("TIS_402",1,false);
                    rfcMessenger.SetCollideEventRFC("TIS_403",1,false);
                    rfcMessenger.SetCollideEventRFC("TIS_404",1,false);
                    rfcMessenger.SetCollideEventRFC("TIS_401",2,false);                        
                    rfcMessenger.SetCollideEventRFC("TIS_402",2,false);
                    rfcMessenger.SetCollideEventRFC("TIS_403",2,false);
                    rfcMessenger.SetCollideEventRFC("TIS_404",2,false);
                    rfcMessenger.SetCollideEventRFC("TIS401_FingeringVoltageIndicator",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS402_FingeringVoltageIndicator",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS403_FingeringVoltageIndicator",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS404_FingeringVoltageIndicator",1,true);
                    break;
                case(Scene2Procedure.PART_B_SETUP_SAFETY_BARRIER):
                    rfcMessenger.SetCollideEventRFC("TIS401_FingeringVoltageIndicator",1,false);
                    rfcMessenger.SetCollideEventRFC("TIS402_FingeringVoltageIndicator",1,false);
                    rfcMessenger.SetCollideEventRFC("TIS403_FingeringVoltageIndicator",1,false);
                    rfcMessenger.SetCollideEventRFC("TIS404_FingeringVoltageIndicator",1,false);
                    rfcMessenger.SetCollideEventRFC("TIS401_FingeringVoltageIndicator",2,false);
                    rfcMessenger.SetCollideEventRFC("TIS402_FingeringVoltageIndicator",2,false);
                    rfcMessenger.SetCollideEventRFC("TIS403_FingeringVoltageIndicator",2,false);
                    rfcMessenger.SetCollideEventRFC("TIS404_FingeringVoltageIndicator",2,false);
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier",1,true);
                    break;
                case(Scene2Procedure.PART_B_APPLY_DANGER_NOTICE_401):
                    rfcMessenger.SetCollideEventRFC("SafetyBarrier",1,false);
                    rfcMessenger.SetCollideEventRFC("FingeringSafetyBarrier",2,false);
                    rfcMessenger.SetSnapEventRFC("TIS_401");
                    rfcMessenger.SetSnapEventRFC("TIS_402");
                    rfcMessenger.SetSnapEventRFC("TIS_403");
                    rfcMessenger.SetSnapEventRFC("TIS_404");
                    break;
                case(Scene2Procedure.PART_B_WAIT_CALL_1):
                    yield return new WaitForSeconds(5);
                    //Open?????
                    AudioManager.instance.PlayAudioClip("Scene2B_8");

                    rfcMessenger.PlayAnimationTriggerRFC("CB_call_psc","TIS401_Button_CB_prefab");
                    rfcMessenger.PlayAnimationTriggerRFC("CB_call_psc","TIS402_Button_CB_prefab");
                    rfcMessenger.PlayAnimationTriggerRFC("CB_call_psc","TIS403_Button_CB_prefab");
                    rfcMessenger.PlayAnimationTriggerRFC("CB_call_psc","TIS404_Button_CB_prefab");
                    rfcMessenger.PlayAnimationTriggerRFC("light_off","TIS401_Signal_Button");
                    rfcMessenger.PlayAnimationTriggerRFC("light_off","TIS402_Signal_Button");
                    rfcMessenger.PlayAnimationTriggerRFC("light_off","TIS403_Signal_Button");
                    rfcMessenger.PlayAnimationTriggerRFC("light_off","TIS404_Signal_Button");
                    yield return new WaitForSeconds(10); //11
                    
                    //Ring Phone???                        
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);
                    yield return new WaitForSeconds(4); //11

                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().IsPlayerOne())
                        StartCoroutine(ReadStepInstructions(true));
                    break;
                case(Scene2Procedure.PART_B_ANSWER_CALL_1):
                    // wait for user to open Phone and answer ;
                    break;
                case(Scene2Procedure.PART_B_FINGERING_CB_PROCEDURE):
                    AudioManager.instance.PlayAudioClip("Scene2B_10");
                    yield return new WaitForSeconds(3); //16
                    AudioManager.instance.PlayAudioClip("Scene2B_11");
                    yield return new WaitForSeconds(3); //16

                    TISSwitchCabinateList[0].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINGERING_CB;
                    TISSwitchCabinateList[1].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINGERING_CB;
                    TISSwitchCabinateList[2].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINGERING_CB;
                    TISSwitchCabinateList[3].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.FINGERING_CB;
                    rfcMessenger.SetCollideEventRFC("TIS401_FingeringCB",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS402_FingeringCB",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS403_FingeringCB",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS404_FingeringCB",1,true);
                    break;
                case(Scene2Procedure.PART_B_CHANGE_CONTROL_PROCEDURE):
                    TISSwitchCabinateList[0].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.TURN_CONTROL_TO_LOCAL;
                    TISSwitchCabinateList[1].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.TURN_CONTROL_TO_LOCAL;
                    TISSwitchCabinateList[2].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.TURN_CONTROL_TO_LOCAL;
                    TISSwitchCabinateList[3].currentProcedure = Scene2TISSwitchCabinate.CabinateProcedure.TURN_CONTROL_TO_LOCAL;
                    rfcMessenger.SetCollideEventRFC("TIS401_SetToLocal",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS402_SetToLocal",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS403_SetToLocal",1,true);
                    rfcMessenger.SetCollideEventRFC("TIS404_SetToLocal",1,true);
                    break;
                case(Scene2Procedure.PART_B_APPLY_CAUTION_NOTICE_401):
                    rfcMessenger.SetSnapEventRFC("TIS_401LocalSwitch");
                    rfcMessenger.SetSnapEventRFC("TIS_402LocalSwitch");
                    rfcMessenger.SetSnapEventRFC("TIS_403LocalSwitch");
                    rfcMessenger.SetSnapEventRFC("TIS_404LocalSwitch");
                    break;
                case(Scene2Procedure.PART_B_CALL_PSC_2):
                    AudioManager.instance.PlayAudioClip("Scene2B_15a");
                    yield return new WaitForSeconds(3);
                    AudioManager.instance.PlayAudioClip("Scene2B_15b");
                    yield return new WaitForSeconds(3);
                    break;
                case(Scene2Procedure.PART_B_GO_OUTSIDE):
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);
                    
                    yield return new WaitForSeconds(4);
                    TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[2]);
                    
                    StartCoroutine(ReadStepInstructions(true));

                    break;
                case(Scene2Procedure.PART_B_OUTSIDE_CHANGE_CONTROL_PROCEDURE):
                    AudioManager.instance.PlayAudioClip("Scene2B_17");
                    // yield return new WaitForSeconds(12);

                    TeleportManager.instance.SetCurrentPointerList(new string[] {"TP_LT401","TP_LT402","TP_LT403","TP_LT404"});
                    TeleportManager.instance.SetPointer("TP_LT402",true);
                    TeleportManager.instance.SetPointer("TP_LT403",true);
                    TeleportManager.instance.SetPointer("TP_LT404",true);

                    currentProcedure = Scene2Procedure.PART_B_OUTSIDE_CHANGE_CONTROL_PROCEDURE;
                    foreach(Scene2LTSwitchCabinate cab in LTSwitchCabinateList){
                        cab.currentProcedure = Scene2LTSwitchCabinate.CabinateProcedure.FINGERING_PANEL;
                    }
                    rfcMessenger.SetCollideEventRFC("LT401_FingeringPanel",1,true);
                    rfcMessenger.SetCollideEventRFC("LT402_FingeringPanel",1,true);
                    rfcMessenger.SetCollideEventRFC("LT403_FingeringPanel",1,true);
                    rfcMessenger.SetCollideEventRFC("LT404_FingeringPanel",1,true);
                    break;
                case(Scene2Procedure.PART_B_CALL_PSC_3):
                    AudioManager.instance.PlayAudioClip("Scene2B_21");
                    
                    // TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[1]);
                    break;
                case(Scene2Procedure.PART_B_WAIT_CALL_2):
                    // AudioManager.instance.PlayAudioClip("Com_Phone_Call"); 
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);                   
                    yield return new WaitForSeconds(6);
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);
                    yield return new WaitForSeconds(4);
                    AudioManager.instance.PlayAudioClip("Scene2B_23");
                    rfcMessenger.ReadStepInstructionsRFC(true);
                    break;
                case(Scene2Procedure.PART_B_OUTSIDE_CHECK_TESTER):
                    AudioManager.instance.PlayAudioClip("Scene2B_25");
                    yield return new WaitForSeconds(5);
                    AudioManager.instance.PlayAudioClip("Scene2B_26");

                    // TeleportManager.instance.SetCurrentPointerList(new string[] {"TP_LT401","TP_LT402","TP_LT403","TP_LT404"});
                    // TeleportManager.instance.SetPointer("TP_LT402",true);
                    // TeleportManager.instance.SetPointer("TP_LT403",true);
                    // TeleportManager.instance.SetPointer("TP_LT404",true);
                    // TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[2]);
                    break;
                case(Scene2Procedure.PART_B_OUTSIDE_APPLY_TO_CIRCUIT):
                    foreach(EarthingTesterCollider collider in scene2.earthingTesterCollidersList){
                        collider.earthTestStart = true;
                        collider.gameObject.SetActive(true);
                    }
                    break;
                // case(Scene2Procedure.PART_B_CALL_PSC_4):
                //     TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[1]);
                    
                    //for testing 

                case(Scene2Procedure.PART_B_APPLY_EARTHING_PROCEDURE):

                    // TeleportManager.instance.SetCurrentPointerList(new string[] {"TP_LT401","TP_LT402","TP_LT403","TP_LT404"});
                    // TeleportManager.instance.SetPointer("TP_LT402",true);
                    // TeleportManager.instance.SetPointer("TP_LT403",true);
                    // TeleportManager.instance.SetPointer("TP_LT404",true);

                    // TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[2]);
                    rfcMessenger.SetCollideEventRFC("LT401_FingeringCircuit",1,true);
                    rfcMessenger.SetCollideEventRFC("LT402_FingeringCircuit",1,true);
                    rfcMessenger.SetCollideEventRFC("LT403_FingeringCircuit",1,true);
                    rfcMessenger.SetCollideEventRFC("LT404_FingeringCircuit",1,true);
                    foreach(Scene2LTSwitchCabinate cab in LTSwitchCabinateList){
                        cab.currentProcedure = Scene2LTSwitchCabinate.CabinateProcedure.FINGERING_EARTHING_TOP;
                    }
                    foreach(EarthingTesterCollider collider in scene2.earthingTesterCollidersList){
                        collider.earthLockStart = true;
                        collider.gameObject.SetActive(false);
                    }
                    break;
                case(Scene2Procedure.PART_B_CALL_PSC_5):
                    AudioManager.instance.PlayAudioClip("Scene2B_35");
                    break;
                case(Scene2Procedure.PART_B_WAIT_CALL_3):
                    yield return new WaitForSeconds(1);
                    AudioManager.instance.PlayAudioClip("Scene2B_37");
                    yield return new WaitForSeconds(5);
                    AudioManager.instance.PlayAudioClip("Com_Phone_Call",false);
                    yield return new WaitForSeconds(4);
                    AudioManager.instance.PlayAudioClip("Scene2B_39");

                    if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().IsPlayerOne())
                        StartCoroutine(ReadStepInstructions(true));
                    break;
                case(Scene2Procedure.PART_B_FINISH):
                    AudioManager.instance.PlayAudioClip("Scene2B_42");
                    break;
                    // currentProcedure = Scene2Procedure.PART_B_OUTSIDE_APPLY_CAUTION_NOTICE_401;
                    // rfcMessenger.SetSnapEventRFC("LT401_Panel");
                    // rfcMessenger.SetSnapEventRFC("LT402_Panel");
                    // rfcMessenger.SetSnapEventRFC("LT403_Panel");
                    // rfcMessenger.SetSnapEventRFC("LT404_Panel");
                    // break;

                default:
                    break;
            }
        }else{
            switch(currentProcedure){
                case(Scene2Procedure.PART_A_CABINATE_PROCEDURES):
                    if(extraInfo == "KSR711_True"){
                        StartCoroutine(KSRSwitchCabinateList[0].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "KSR714_True"){
                        StartCoroutine(KSRSwitchCabinateList[1].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "PMC814_True"){
                        StartCoroutine(KSRSwitchCabinateList[2].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo =="KSR711_Done"){
                        yield return new WaitForSeconds(2);
                        AudioManager.instance.PlayAudioClip("Scene2A_8");
                        TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[5]);
                    }
                    if(extraInfo =="KSR714_Done"){
                        yield return new WaitForSeconds(2);
                        AudioManager.instance.PlayAudioClip("Scene2A_10");
                        TeleportManager.instance.Teleport(TeleportManager.instance.playerTeleportPositionList[6]);
                    }
                    if(extraInfo =="PMC814_Done"){
                        AudioManager.instance.PlayAudioClip("Scene2A_12");
                        rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                    break;
                case(Scene2Procedure.PART_B_INTRODUCTION):
                    Debug.Log("Set up part B introduction");
                    break;
                case(Scene2Procedure.PART_B_SETUP_SAFETY_BARRIER):
                    if(extraInfo =="OpenBarrier"){
                        scene2.TISsafetyBarriers[0].SetActive(true);
                    }
                    break;
                case(Scene2Procedure.PART_B_FINGERING_CB_PROCEDURE):
                    if(extraInfo == "TIS401_True"){
                        StartCoroutine(TISSwitchCabinateList[0].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "TIS402_True"){
                        StartCoroutine(TISSwitchCabinateList[1].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "TIS403_True"){
                        StartCoroutine(TISSwitchCabinateList[2].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "TIS404_True"){
                        StartCoroutine(TISSwitchCabinateList[3].CabinateReadStepInstructions(true));
                    }
                    if(extraInfo =="CheckProcedure"){
                        bool checkAllCabDone =true;
                        foreach(Scene2TISSwitchCabinate cab in TISSwitchCabinateList){
                            if(!cab.finishFingeringCB){
                                checkAllCabDone = false;
                                break;
                            }
                        }
                        if(checkAllCabDone)
                            rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                    break;
                case(Scene2Procedure.PART_B_CHANGE_CONTROL_PROCEDURE):
                    if(extraInfo == "TIS401_True"){
                        StartCoroutine(TISSwitchCabinateList[0].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "TIS402_True"){
                        StartCoroutine(TISSwitchCabinateList[1].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "TIS403_True"){
                        StartCoroutine(TISSwitchCabinateList[2].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "TIS404_True"){
                        StartCoroutine(TISSwitchCabinateList[3].CabinateReadStepInstructions(true));
                    }
                    if(extraInfo =="CheckProcedure"){
                        bool checkAllCabDone =true;
                        foreach(Scene2TISSwitchCabinate cab in TISSwitchCabinateList){
                            if(!cab.controlToLocalProcedureBool){
                                checkAllCabDone = false;
                                break;
                            }
                        }
                        if(checkAllCabDone)
                            rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                    break;
                case(Scene2Procedure.PART_B_OUTSIDE_CHANGE_CONTROL_PROCEDURE):
                    if(extraInfo == "LT401_True"){
                        StartCoroutine(LTSwitchCabinateList[0].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "LT402_True"){
                        StartCoroutine(LTSwitchCabinateList[1].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "LT403_True"){
                        StartCoroutine(LTSwitchCabinateList[2].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "LT404_True"){
                        StartCoroutine(LTSwitchCabinateList[3].CabinateReadStepInstructions(true));
                    }
                    if(extraInfo =="CheckProcedure"){
                        bool checkAllCabDone =true;
                        foreach(Scene2LTSwitchCabinate cab in LTSwitchCabinateList){
                            if(!cab.controlToOffProcedureBool){
                                checkAllCabDone = false;
                                break;
                            }
                        }
                        if(checkAllCabDone)
                            rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                    break;
                case(Scene2Procedure.PART_B_APPLY_EARTHING_PROCEDURE):
                    if(extraInfo == "LT401_True"){
                        StartCoroutine(LTSwitchCabinateList[0].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "LT402_True"){
                        StartCoroutine(LTSwitchCabinateList[1].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "LT403_True"){
                        StartCoroutine(LTSwitchCabinateList[2].CabinateReadStepInstructions(true));
                    }                 
                    if(extraInfo == "LT404_True"){
                        StartCoroutine(LTSwitchCabinateList[3].CabinateReadStepInstructions(true));
                    }
                    if(extraInfo =="CheckProcedure"){
                        bool checkAllCabDone =true;
                        foreach(Scene2LTSwitchCabinate cab in LTSwitchCabinateList){
                            if(!cab.applyLockStatus){
                                checkAllCabDone = false;
                                break;
                            }
                        }
                        if(checkAllCabDone)
                            rfcMessenger.ReadStepInstructionsRFC(true);
                    }
                    break;
                case(Scene2Procedure.PART_B_FINISH):
                    AudioManager.instance.PlayAudioClip("Com_Fingering procedures");
                    break;
                default:
                    break;
            }
        }
        yield return 0;
    }
    public void SkipStep(){
        switch(currentProcedure){
            // case(Scene2Procedure.):
            //     if(switch25kVCabinateList[0].currentPart1Procedure != Switch25kVCabinate.Part1Procedure.SET_CONTROL_POSITION&&
            //         switch25kVCabinateList[1].currentPart1Procedure != Switch25kVCabinate.Part1Procedure.SET_CONTROL_POSITION){
            //         rfcMessenger.ReadStepInstructionsRFC(false,"HA09_True");
            //         rfcMessenger.ReadStepInstructionsRFC(false,"HA11_True");
            //     }else{
            //         rfcMessenger.TestNextStepRFC();
            //     }
            //     break;
            // case(Scene2Procedure.AC_SWITCHGEAR_PROCEDURE):
            //     if(switch25kVCabinateList[2].currentACProcedure != Switch25kVCabinate.ACProcedure.APPLY_PADLOCK&&
            //         switch25kVCabinateList[3].currentACProcedure != Switch25kVCabinate.ACProcedure.APPLY_PADLOCK){
            //         rfcMessenger.ReadStepInstructionsRFC(false,"HA01_True");
            //         rfcMessenger.ReadStepInstructionsRFC(false,"HA03_True");
            //     }else{
            //         rfcMessenger.TestNextStepRFC();
            //     }
            //     break;
            // case(Scene2Procedure.CONTROL_PANEL_PROCEDURE):
            // case(Scene2Procedure.CLOSE_EARTH_SWITCH_PROCEDURE):
            //     rfcMessenger.ReadStepInstructionsRFC(false,"503A_True");
            //     rfcMessenger.ReadStepInstructionsRFC(false,"504A_True");
            //     break;
            default:
                Debug.Log("Skip steppp before RFCCCCC");
                rfcMessenger.NextStepRFC();
                break;
        }
    }
    public Scene2TISSwitchCabinate GetTISSwitchCabinate(string action){
        foreach(Scene2TISSwitchCabinate cab in TISSwitchCabinateList){
            if(action.Contains(cab.cabinateCode)){
                return cab;
            }
        }
        return null;
    }
    public Scene2LTSwitchCabinate GetLTSwitchCabinate(string action){
        Debug.Log("Check action" + action);
        foreach(Scene2LTSwitchCabinate cab in LTSwitchCabinateList){
            
            if(action.Contains(cab.cabinateCode)){
                Debug.Log("get cab  "  + cab.name );
                return cab;
            }
        }
        return null;
    }
    public Scene2KSRSwitchCabinate GetKSRSwitchCabinate(string action){
        foreach(Scene2KSRSwitchCabinate cab in KSRSwitchCabinateList){
            if(action.Contains(cab.cabinateCode)){
                Debug.Log("get cab  "  + cab.name );
                return cab;
            }
        }
        return null;
    }
}
