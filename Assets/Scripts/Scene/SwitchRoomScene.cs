﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchRoomScene : MonoBehaviour
{
    public List<SwitchGear> switchCabinateList = new List<SwitchGear>();
    public GameObject trackSideSign;
    public BoxCollider roomEarthingStartCollider;
    public GameObject earthingToWall;
    public GameObject MeggerTesterTool;
    public GameObject isoCameraPart1;
    public GameObject isoCameraPart3;
    public GameObject singlePlayNPC;
    public GameObject folderSafetyDoc;
    public GameObject folderSafetyDocBlank;
    
    [HideInInspector]
    public GameObject safetyDocumentClone;


    // Start is called before the first frame update
    void Start()
    {
        trackSideSign.SetActive(false);
        folderSafetyDocBlank.SetActive(true);
        folderSafetyDoc.SetActive(false);
    }

    public void ChangeIsoCameraToPart3()
    {
        isoCameraPart1.SetActive(false);
        isoCameraPart3.SetActive(true);
    }
    public bool CheckExplosion(){
        foreach(SwitchGear gear in switchCabinateList){
            if(gear.GetExplosionBool()){
                return true;
            }
        }
        return false;
    }

}
