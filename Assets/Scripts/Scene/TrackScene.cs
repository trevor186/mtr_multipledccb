﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackScene : MonoBehaviour
{
    public GameObject manualIsolatorID;
    public List<ManualIsolator> manualIsolatorList = new List<ManualIsolator>();
    public List<MotorIsolator> motorIsolatorList = new List<MotorIsolator>();

    public GameObject switchGearSign;


    // Start is called before the first frame update
    void Start()
    {
        switchGearSign.SetActive(false);        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
