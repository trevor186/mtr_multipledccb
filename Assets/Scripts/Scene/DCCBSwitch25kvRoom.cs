﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DCCBSwitch25kvRoom : MonoBehaviour
{
    public List<GameObject> safetyBarrierList = new List<GameObject>();
    public List<Switch25kVCabinate> cabinates25k = new List<Switch25kVCabinate>();
    public List<SwitchHandleRotatorEvent> handleRotatorList = new List<SwitchHandleRotatorEvent>();

    public List<EarthingTesterCollider> earthingTesterColliderList = new List<EarthingTesterCollider>();
    public GameObject safetyDocumentOnWall;
    public GameObject safetyDocumentNPC;
}
