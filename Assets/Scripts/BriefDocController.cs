﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BriefDocController : MonoBehaviour
{

    public List<GameObject> documentList = new List<GameObject>();
    public List<Sprite> workBriefDocSprite = new List<Sprite>();
    public List<GameObject> documentListBackup = new List<GameObject>();
    public List<GameObject> workBriefDocumentListFinish = new List<GameObject>();
    int currentPage;

    public void Init(bool isManual, int cabinateNumber)
    {
        documentList.Clear();
        workBriefDocumentListFinish.Clear();
        documentList = documentListBackup.ToList();

        Debug.Log("Doc init - isManual:" + isManual + ", cabinateNumber:" + cabinateNumber);
        for (int i=0;i<5;i++)
        {
            documentList[i+2].GetComponent<Image>().sprite = workBriefDocSprite[i + (isManual?0:15) + 5*(cabinateNumber)];
            workBriefDocumentListFinish.Add(documentList[i+2]);
        }


        currentPage = 0;
    }

    public void SetFinishList()
    {
        documentList.Clear();
        documentList = workBriefDocumentListFinish.ToList();;
        currentPage = 0;
    }

    public void OpenDoc(){
        this.gameObject.SetActive(true);
        currentPage = 0;
        SwitchPage(0);
    }
    public void CloseDoc(){
        Debug.Log("Close doc");
        this.gameObject.SetActive(false);
        foreach(GameObject doc in documentList){
            doc.SetActive(false);
        }
    }

    public void SwitchPage(int value){
        foreach(GameObject doc in documentList){
            doc.SetActive(false);
        }
        currentPage = Mathf.Clamp(currentPage + value,0,2);
        documentList[currentPage].SetActive(true);
    }    

    public void NextPage()
    {
        foreach(GameObject doc in documentList){
            doc.SetActive(false);
        }
        currentPage = Mathf.Clamp((currentPage + 1) % documentList.Count, 0, documentList.Count);
        documentList[currentPage].SetActive(true);
    }
    public void OpenManual(string value){
        foreach(GameObject doc in documentList){
            if(doc.gameObject.name == value){
                doc.SetActive(true);
            }else{
                doc.SetActive(false);
            }
        }
    }

}
