﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using VRTK;

public class AbstructNoticeSnapEvent : TNBehaviour
{
    public Transform target;
    public VRTK_SnapDropZone snapDropZone;
    public System.Collections.Generic.List<GameObject> noticeList = new System.Collections.Generic.List<GameObject>();
    public string noticeName;
    public GameObject snappedObject;
    public bool cautionNotice;
    public bool dangerNotice;
    public GameObject backupSnappedObject;

    // Start is called before the first frame update
    public virtual void Start()
    {
        target = transform.Find("HighlightContainer/HighlightObject");
        snapDropZone.ObjectSnappedToDropZone += new SnapDropZoneEventHandler(delegate{
            Debug.Log("NoticeSnapEvent Snapped");
            this.GetComponent<BoxCollider>().enabled =false;
            RFCMessenger.instance.NextStepRFC("PutUpNotice",this.name +"_"+snapDropZone.GetCurrentSnappedObject().name);
            AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();
        });
    }
    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Notice")
        {

        }
    }
    public virtual void SnapToHolder(string name ){
        foreach(GameObject notice in noticeList){
            if(name == notice.name){
                GameObject clone;
                clone = GameObject.Instantiate(notice);
                clone.GetComponent<BoxCollider>().enabled = false;
                clone.SetActive(true);
                // if(this.name =="Collider_NoticeHanger2"){
                //     Debug.Log("Change this ");
                //     clone.transform.localPosition = new Vector3(0,0,1);
                //     clone.transform.localRotation = Quaternion.Euler(0,3,0);
                // }
                snapDropZone.ForceSnap(clone);
                snappedObject = clone;
            }
        }
    }
    public virtual void DestroySnappedNotice(){
        if(snappedObject != null){
            Destroy(snappedObject);
            this.GetComponent<BoxCollider>().enabled =true;
        }
    }
    public void CheckSnapObject(){
        Debug.Log("Enter");
        if(snappedObject == null){
            Debug.Log("Please snap");
            backupSnappedObject.SetActive(true);
            snapDropZone.enabled = false;
        }
    }
}