﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using VRTK;
using VRTK.Controllables.ArtificialBased;

public class SwitchHandleSnapEvent : TNBehaviour
{
    Transform target;
    public VRTK_SnapDropZone snapDropZone;
    public string cabinateCode;
    public string holeName;

    // Start is called before the first frame update
    void Start()
    {
        target = transform.Find("HighlightContainer/HighlightObject");

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SwitchHandle")
        {
            this.GetComponent<BoxCollider>().enabled =false;
            string path = "Prefabs/" + other.name;
            Debug.Log("?path?  " + path + " ???  "+ AMVRNetworkManager.instance.channelID);
            if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() ==1){
                TNManager.Instantiate(AMVRNetworkManager.instance.channelID, "SwitchHandleSnapCreate", path, false, target.position, target.rotation,cabinateCode,holeName);
                MultiDCCBSceneRFCMessenger.instance.CloseHandleSnapRFC(cabinateCode,holeName);
            }
            // AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();
            // RFCMessenger.instance.NextStepRFC("Handle",other.name);
        }

    }

    [RCC]
	static GameObject SwitchHandleSnapCreate(GameObject prefab, Vector3 pos, Quaternion rot,string code,string hole)
	{
		Debug.Log("SwitchHandleSnapCreate");
        GameObject clone = GameObject.Instantiate(prefab, pos, rot);
        clone.GetComponent<BoxCollider>().enabled = false;
        clone.GetComponentInChildren<VRTK_ArtificialRotator>().enabled = true;
        // ScenarioEventManager.instance.SetRotator(clone);
        clone.GetComponentInChildren<SwitchHandleRotatorEvent>().cabinateCode = code;
        clone.GetComponentInChildren<SwitchHandleRotatorEvent>().holeName = hole;
        MultiDCCBSceneEventManager.instance.switch25kvGearRoom.handleRotatorList.Add(clone.GetComponentInChildren<SwitchHandleRotatorEvent>());
        MultiDCCBSceneRFCMessenger.instance.RemoveRightHandObjectRFC();
        
        return clone;
    }
}
