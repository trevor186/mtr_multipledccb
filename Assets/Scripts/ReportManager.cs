﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReportManager : MonoBehaviour
{
    public static ReportManager instance = null;
    public Dictionary<string,bool> checkList = new Dictionary<string,bool>();
    public Dictionary<int,bool> reportList = new Dictionary<int,bool>();

    // public List<GameObject> ticks = new List<GameObject>();
    // public List<GameObject> remarks = new List<GameObject>();

    List<bool> ticks = new List<bool>();

    public ReportPanel [] reportPanels;


    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start() {
        Init();
    }

    public void Init()
    {
        ticks.Clear();
        for (int i=0;i<reportPanels[0].ticks.Count;i++)
            ticks.Add(false);

        foreach (ReportPanel item in reportPanels)
        {
            if (item != null)
                item.Init();            
        }
    }

    // public void UpdateReport(string param,bool result){
    //     checkList.Add(param,result);
    // }

    public void InitReportType(){   
        foreach (ReportPanel item in reportPanels)
        {
            if (item != null)
                item.ChangeToMotorReport(ScenarioEventManager.instance.IsManualGameMode());
        }
    }
    public void UpdateReportStep(int step, bool result)
    {

        if (step > 0 && step <= ticks.Count)
            ticks[step-1] = result;

        foreach (ReportPanel item in reportPanels)
        {
            item.UpdateReportStep(step, result);
        }            
    }

    public void UpdateRemark(int step, string remarkText)
    {
        HapticsManager.instance.PlayVibration();
        AudioManager.instance.PlayAudioClip("COM_False");

        foreach (ReportPanel item in reportPanels)
        {
            item.UpdateRemark(step, remarkText);
        }

        // int idx = -1;
        // switch(step)
        // {
        //     case 8: idx = 0; break;
        //     case 19: idx = 1; break;
        //     case 22: idx = 2; break;
        //     case 24: idx = 3; break;
        //     default: idx = -1; break;
        // }

        // if (idx != -1)
        //     remarks[idx].GetComponent<Text>().text = remarkText;
    }

    public bool CheckStep(int step)
    {
        // return (ticks[step-1].activeSelf);
        return ticks[step-1];
    }

    public bool CheckRemark(int step)
    {
        return reportPanels[0].remarks[step-1].activeSelf;
    }

    // public void ShowReportInHand(Transform parent = null)
    // {
    //     if (reportPanelInHand == null)
    //     {
    //         if (parent != null)
    //             reportPanelInHand = GameObject.Instantiate(reportPanel, Vector3.zero, Quaternion.identity, parent);
    //     }
    //     else
    //     {
    //         reportPanelInHand.SetActive(true);

    //     }
    // }

    // public void CloneReportToHand()
    // {
    //     if (reportPanelInHand != null)
    //     {
    //         Transform parent = reportPanelInHand.transform.parent;
    //         Destroy(reportPanelInHand.gameObject);
    //         ShowReportInHand(parent);
    //     }        
    // }

    public void AddReportPanel(ReportPanel panel)
    {
        reportPanels[1] = panel;
    }

    public void ShowAllReports()
    {
        foreach (ReportPanel item in reportPanels)     
        {
            if (item != null)
                item.gameObject.SetActive(true);
        }
    }
}