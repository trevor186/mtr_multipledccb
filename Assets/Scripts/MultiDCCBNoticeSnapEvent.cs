﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using VRTK;


public class MultiDCCBNoticeSnapEvent : AbstructNoticeSnapEvent
{
    // Start is called before the first frame update
    public override void Start()
    {
        target = transform.Find("HighlightContainer/HighlightObject");
        snapDropZone.highlightAlwaysActive = false;
        snapDropZone.ObjectSnappedToDropZone += new SnapDropZoneEventHandler(delegate{
            Debug.Log("NoticeSnapEvent Snapped");
            this.GetComponent<BoxCollider>().enabled =false;
            if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1)
            {
               MultiDCCBSceneRFCMessenger.instance.RemoveRightHandObjectRFC();
               MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(noticeName+"_" +snapDropZone.GetCurrentSnappedObject().name);
            }
        });
    }

    // Update is called once per frame
    public override void OnTriggerEnter(Collider other)
    {
        if(other.tag =="Notice"){
            if( (cautionNotice && other.name.Contains("Caution")) ||
                (dangerNotice &&other.name.Contains("Danger"))){
                // SnapToHolder(other.name);
                GameObject clone = Instantiate(other.gameObject,this.transform);
                other.gameObject.SetActive(false);
                this.GetComponent<BoxCollider>().enabled =false;
                clone.GetComponent<BoxCollider>().enabled = false;
                clone.SetActive(true);
                snapDropZone.ForceSnap(clone);
                snappedObject =clone;
                Debug.Log("Snapped!!!");
                if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1){
                    Debug.Log("Test  Check snap ");
                        MultiDCCBSceneRFCMessenger.instance.CheckSnapObjectRFC(this.gameObject.name);
                    }
                }
        }
    }
    public override void SnapToHolder(string name ){
        base.SnapToHolder(name);
    }
    public override void DestroySnappedNotice(){
        base.DestroySnappedNotice();
    }
}
