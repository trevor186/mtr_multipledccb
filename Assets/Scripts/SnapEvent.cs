﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class SnapEvent : MonoBehaviour
{
    public string eventName;
    Transform target;
    public VRTK_SnapDropZone snapDropZone;
        public GameObject snappedObject;

    // Start is called before the first frame update
    void Start()
    {
        target = transform.Find("HighlightContainer/HighlightObject");
        snapDropZone.ObjectSnappedToDropZone += new SnapDropZoneEventHandler(delegate{
            this.GetComponent<BoxCollider>().enabled =false;
            // RFCMessenger.instance.NextStepRFC("PutUpNotice",this.name +"_"+snapDropZone.GetCurrentSnappedObject().name);
            // AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();
        });
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("on trigger entereeee!!!  "  + other.name  );
        if (other.tag == "SnapObject")
        {
            GameObject clone = Instantiate(other.gameObject,this.transform);
            other.gameObject.SetActive(false);
            this.GetComponent<BoxCollider>().enabled =false;
            clone.GetComponent<BoxCollider>().enabled = false;
            clone.SetActive(true);
            snapDropZone.ForceSnap(clone);
            SnapEventObjectManager.instance.ActionCall(eventName);
        }
    }
    // Update is called once per frame
    public void DestroySnappedNotice(){
        if(snappedObject != null){
            Destroy(snappedObject);
            this.GetComponent<BoxCollider>().enabled =true;
        }
    }
}
