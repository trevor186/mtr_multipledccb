using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideEvent : MonoBehaviour // this is to return value 
{
    public string actionValue;
    public int objectPlayerID; 
    public bool isFingering;
    public void setActionValue(string value){
        actionValue = value;
    }
    public string getActionValue(){
        if(isFingering &&  this.transform.Find("fingering") != null){
            this.transform.Find("fingering").gameObject.SetActive(true);
            AudioManager.instance.PlayAudioClip("Com_Fingering_procedures",false);
            Invoke("CloseFingering",3);
        }
        return actionValue;
    }
    
    public void CloseFingering(){
        this.transform.Find("fingering").gameObject.SetActive(false);
    }

    public void RFCFingering(){
        if(isFingering &&  this.transform.Find("fingering") != null){
            this.transform.Find("fingering").gameObject.SetActive(true);
            AudioManager.instance.PlayAudioClip("Com_Fingering_procedures");
            Invoke("CloseFingering",3);
        }
    }
}
