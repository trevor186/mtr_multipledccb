﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;
using VRTK.Controllables.ArtificialBased;

public class HandleRotatorEvent : MonoBehaviour
{
    VRTK_ArtificialRotator controllable;
    private int _rotateCounter;
    private bool _playSound;
    // public Text displayText;

    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_ArtificialRotator>() : controllable);
        controllable.ValueChanged += ValueChanged;

        // controllable.MinLimitReached += MinLimitReached;
        controllable.MaxLimitReached += MaxLimitReached;
    }
    protected virtual void ValueChanged(object sender, ControllableEventArgs e)
    {
        // Debug.Log(e.value.ToString("F3"));
        Debug.Log("Rotator angle" + e.value.ToString("F3"));
        if(_rotateCounter<(int)Mathf.Floor(e.value/360)){
            _rotateCounter = (int)Mathf.Floor(e.value/360);
            _playSound = true;
        }
        Debug.Log("ROUNTER?" + _rotateCounter);
        if (AMVRNetworkManager.instance.GetCurrentPlayer().IsPlayerOne())
            RFCMessenger.instance.SendOthers("RotatorAngle", e.value);
        if(!AudioManager.instance.audioSource.isPlaying && _playSound){
            AudioManager.instance.PlayAudioClip("P1_Manoeuvring_handle");
            _playSound = false;
        }
    }

    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        AudioManager.instance.audioSource.Stop();
        // controllable.gameObject.SetActive(false);   //Remove the rotator now

        if (GlobalConfig.instance.GetNetworkID() == 1)
            RFCMessenger.instance.NextStepRFC("HandleRotateFinish");
        // RFCMessenger.instance.SendOthers("Step3Finish");
    }
}

