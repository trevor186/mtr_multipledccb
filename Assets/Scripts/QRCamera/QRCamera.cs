﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QRCamera : MonoBehaviour
{
    public Transform cam;
    public bool scannedObject;
    public float scannedTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ScanQRCode(cam);
        if(scannedObject){
            scannedTime += Time.deltaTime;
        }
        if(scannedTime >2){
            scannedTime =0;
            scannedObject = false;
            MultiDCCBSceneHandlingManager.instance.SceneActionCallResult("QRCode");
        }
    }

    public void ScanQRCode(Transform camTransform){
        RaycastHit hit;
            var ignoreMask = (1 <<10);
        
        int layerMask =0;
        layerMask = ~layerMask;
        ignoreMask = ~ignoreMask;
        
        Ray ray = new Ray(camTransform.position,
                            camTransform.forward);
    
        // Debug.Log("Hit?" + hit.transform.gameObject);
        if(Physics.Raycast(ray,out hit, 4f,ignoreMask)){
            // item = SafetySceneManager.instance.GetObjectName(hit);
            // colliderText.text = item;
           if(hit.transform.name=="QRcode_collider"){
               Debug.Log("Get  tag "  + hit.transform.name );
               scannedObject = true;
           }
        }else{
            scannedObject = false;
            scannedTime = 0;
        }
    }
}
