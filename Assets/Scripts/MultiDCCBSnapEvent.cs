﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using VRTK;

public class MultiDCCBSnapEvent : AbstructNoticeSnapEvent
{
    public string targetItem;
    public override void Start()
    {
        target = transform.Find("HighlightContainer/HighlightObject");
        
        snapDropZone.ObjectSnappedToDropZone += new SnapDropZoneEventHandler(delegate{
            Debug.Log("SanpEvent Snapped");
            this.GetComponent<BoxCollider>().enabled =false;
            if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1)
            {
                Debug.Log("Send this");
               MultiDCCBSceneRFCMessenger.instance.RemoveRightHandObjectRFC();
               MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(targetItem);
            }
        });
    }

    // Update is called once per frame
    public override void OnTriggerEnter(Collider other)
    {
        if(other.name.Contains(targetItem)){
                // SnapToHolder(other.name);
                GameObject clone = Instantiate(other.gameObject,this.transform);
                other.gameObject.SetActive(false);
                this.GetComponent<BoxCollider>().enabled =false;
                clone.GetComponent<BoxCollider>().enabled = false;
                clone.SetActive(true);
                snapDropZone.ForceSnap(clone);
                snappedObject =clone;
                clone.transform.localPosition= Vector3.zero;
                Debug.Log("Snapped!!!");
            if(AMVRNetworkManager.instance.GetCurrentMTRPlayer().GetPlayerID() == 1)
            {
                MultiDCCBSceneRFCMessenger.instance.CheckSnapObjectRFC(this.name);
                Debug.Log("Send this");
                MultiDCCBSceneRFCMessenger.instance.RemoveRightHandObjectRFC();
                MultiDCCBSceneHandlingManager.instance.SceneActionCallResult(targetItem);
                snapDropZone.enabled = false;
            }
        }
    }
    public override void SnapToHolder(string name ){
        base.SnapToHolder(name);
    }
    public override void DestroySnappedNotice(){
        base.DestroySnappedNotice();
    }

}
