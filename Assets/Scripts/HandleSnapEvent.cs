﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
using VRTK;
using VRTK.Controllables.ArtificialBased;

public class HandleSnapEvent : TNBehaviour
{
    Transform target;
    public VRTK_SnapDropZone snapDropZone;

    
    void Start()
    {
        target = transform.Find("HighlightContainer/HighlightObject");

    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ManoeuvringHandle")
        {
            this.GetComponent<BoxCollider>().enabled =false;
            string path = "Prefabs/" + other.name;
            TNManager.Instantiate(AMVRNetworkManager.instance.channelID, "HandleSnapCreate", path, false, target.position, target.rotation);
            AMVRNetworkManager.instance.GetCurrentPlayer().RemoveRightHandObjectRFC();
            RFCMessenger.instance.NextStepRFC("Handle",other.name);
        }

    }

    [RCC]
	static GameObject HandleSnapCreate(GameObject prefab, Vector3 pos, Quaternion rot)
	{
		Debug.Log("HandleSnapCreate");

        GameObject clone = GameObject.Instantiate(prefab, pos, rot);
        clone.GetComponent<BoxCollider>().enabled = false;
        clone.GetComponentInChildren<VRTK_ArtificialRotator>().enabled = true;
        ScenarioEventManager.instance.SetRotator(clone);

        return clone;
    }
}
