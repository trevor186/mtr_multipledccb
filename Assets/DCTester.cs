﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DCTester : MonoBehaviour
{
    public GameObject blackCableStart;
    public GameObject redCableStart;
    public Animator DCAnimator;

    public void CollideAction(){
        DCAnimator.SetBool("Megger_1V",true);
        if(IsInvoking())
            CancelInvoke();
        Invoke("ResetAnimation",3f);
    }
    public void ResetAnimation(){
        DCAnimator.SetBool("Megger_1V",false);
    }

}
