﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;

public class ObjectHolderManager : TNBehaviour
{
    public static ObjectHolderManager instance;
    public System.Collections.Generic.List<string> prefabPath = new System.Collections.Generic.List<string>();
    public System.Collections.Generic.List<GameObject> holderList = new System.Collections.Generic.List<GameObject>();
    public GameObject DCTester;
    public GameObject Megger;
    public GameObject toolsetMeggerTester;
    // public Dictionary <string,string> prefabDictionary = new Dictionary<string, string>();
    // private string CautionNoticePath = ""
    protected override void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    
    public void InstantiateObjectRCC(int prefabIndex, int playerId){
        // return this.gameObject;
        if(AMVRNetworkManager.instance.GetCurrentPlayer().GetPlayerID() == 1 )
            TNManager.Instantiate(AMVRNetworkManager.instance.channelID, "InstantiateObject",prefabPath[prefabIndex],false,prefabIndex,playerId);

    }

    [RCC]
    static GameObject InstantiateObject(GameObject prefab,int prefabIndex, int playerId)
    {
    	GameObject obj = GameObject.Instantiate(prefab,ObjectHolderManager.instance.holderList[prefabIndex].transform);

        obj.AddComponent<SyncObject>();
    	obj.SetActive(true);

        // if (GlobalConfig.instance.GetNetworkID() == playerId)
		// 	AMVRNetworkManager.instance.SetCurrentPlayer(obj.GetComponent<TNetPlayer>());
        return obj;
    }
}
