﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using Bhaptics.Tact.Unity;
public class HapticsManager : MonoBehaviour
{
    public ControllerManager leftController;
    public ControllerManager rightController;
    public List<GameObject> controllerList = new List<GameObject>();
    // public List<ShuntingOperationController> controllerForShunting = new List<ShuntingOperationController>(); 
    public TactSource[] tactSourceList;

    public static HapticsManager instance;
    protected void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void PlayVibration(){
        Debug.Log("Vibrate");
        foreach(TactSource tact in tactSourceList){
            tact.Play();
        }
        if(leftController != null){
            leftController.Vibrate();
            rightController.Vibrate();
        }
        // if(controllerForShunting.Count >0){
        //     foreach(ShuntingOperationController controller in controllerForShunting){
        //         controller.Vibrate();
        //     }
        // }
    }
}
