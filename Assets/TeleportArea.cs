﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TNet;
public class TeleportArea : TNBehaviour
{

    [SerializeField] private bool _player1Enter;
    [SerializeField] private bool _player2Enter;
    [SerializeField] private Animator animator;
    private float _timer;
    private bool _checkEnterAudio;
    private bool _checkFinishAudio;
    private void OnTriggerStay(Collider other)
    {
        if(GlobalConfig.instance.GetNetworkID() == 1){
            if(other.tag =="SpotLight"){
                if(other.GetComponentInParent<TNetPlayer>().GetPlayerID() == 1){
                    // Debug.Log("player 1 enter");
                    _player1Enter = true;
                }else if(other.GetComponentInParent<TNetPlayer>().GetPlayerID() == 2){
                    // Debug.Log("player 2 enter");
                    _player2Enter = true;
                }

                if(_player1Enter &&(ScenarioEventManager.instance.IsSoloPlay()? true:_player2Enter)){
                    _timer += Time.deltaTime;
                    // animator.SetBool("Transitions_Start",true);
                }
            }
        }   
    }
    void Update(){
        if(ScenarioEventManager.instance.IsSoloPlay()){
            animator.SetBool("Transitions_Start",(_player1Enter));
            // if(_player1Enter &&!_checkEnterAudio){
            //     RFCMessenger.instance.PlayAudioRFC("Transition_In");
            //     _checkEnterAudio = true;
            // }
            // CallAnimationRFC("Transitions_Start",_player1Enter);
        }else{
            // if((_player1Enter&&_player2Enter)&& !_checkEnterAudio){
            //     RFCMessenger.instance.PlayAudioRFC("Transition_In");
            //     _checkEnterAudio = true;
            // }
            animator.SetBool("Transitions_Start",(_player1Enter||_player2Enter ));

            // CallAnimationRFC("Transitions_Start",_player1Enter||_player2Enter);

        }

        // animator.enabled= (_player1Enter &&(ScenarioEventManager.instance.IsSoloPlay()? true:_player2Enter));
        if(_timer >4f){
            animator.SetBool("Transitions_Finish",true);
            if(!_checkFinishAudio){
                RFCMessenger.instance.PlayAudioRFC("Com_Finish");
                _checkFinishAudio = true;
            }
            // CallAnimationRFC("Transitions_Finish",true);
            Invoke("DoTeleport",1f);
        }
   
    }
    // public void TestRFC(){
    //     tno.Send("TestCalling",Target.All);
    //     Debug.Log("Call rfc????");
    // }

    // [RFC] void TestCalling(){
    //     Debug.Log("Call resutl");
    // }
    // public void CallAnimationRFC(string animationName,bool value){
    //     tno.Send("CallAnimation",Target.All,animationName,value);
    // }
    // [RFC] void CallAnimation(string animationName,bool value){
    //     animator.SetBool(animationName,value);
    // }

    private void OnTriggerExit(Collider other)
    {
        if(GlobalConfig.instance.GetNetworkID() == 1){
            if(other.tag =="SpotLight"){
                if(other.GetComponentInParent<TNetPlayer>().GetPlayerID() == 1){
                    Debug.Log("Exit Teleport 1");
                    _player1Enter = false;
                     _checkEnterAudio = false;

                }else if(other.GetComponentInParent<TNetPlayer>().GetPlayerID() == 2){
                    Debug.Log("Exit Teleport 2 ");
                    _player2Enter = false;
                    _checkEnterAudio = false;

                }
                _timer = 0;

            }   
        }
    }
    private void DoTeleport(){
        RFCMessenger.instance.NextStepRFC("Teleport");
        // this.gameObject.SetActive(false);
        _timer=0;
    }
}
